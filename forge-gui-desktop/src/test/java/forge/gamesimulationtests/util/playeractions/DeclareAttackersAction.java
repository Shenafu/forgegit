package forge.gamesimulationtests.util.playeractions;

import forge.gamesimulationtests.util.card.CardSpecification;
import forge.gamesimulationtests.util.player.PlayerSpecification;

import java.util.HashMap;
import java.util.Map;

public class DeclareAttackersAction extends BasePlayerAction {
	private final Map<CardSpecification, PlayerSpecification> playerAttackAssignments;
	private final Map<CardSpecification, CardSpecification> permanentAttackAssignments;
	
	public DeclareAttackersAction( PlayerSpecification player ) {
		super( player );
		playerAttackAssignments = new HashMap<CardSpecification, PlayerSpecification>();
		permanentAttackAssignments = new HashMap<CardSpecification, CardSpecification>();
	}
	
	/**
	 * Attack the only opponent
	 */
	public DeclareAttackersAction attack( CardSpecification attacker ) {
		playerAttackAssignments.put( attacker, null );
		return this;
	}
	
	public DeclareAttackersAction attack( CardSpecification attacker, PlayerSpecification player ) {
		playerAttackAssignments.put( attacker, player );
		return this;
	}
	
	public DeclareAttackersAction attack( CardSpecification attacker, CardSpecification permanent ) {
		permanentAttackAssignments.put( attacker, permanent );
		return this;
	}

	public Map<CardSpecification, PlayerSpecification> getPlayerAttackAssignments() {
		return playerAttackAssignments;
	}

	public Map<CardSpecification, CardSpecification> getPermanentAttackAssignments() {
		return permanentAttackAssignments;
	}
}
