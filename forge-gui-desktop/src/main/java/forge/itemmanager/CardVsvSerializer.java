package forge.itemmanager;

import forge.card.ICardFace;
import forge.item.PaperCard;
import forge.util.ItemPool;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class CardVsvSerializer {
	public static void writeCardVsv(final ItemPool<PaperCard> pool, final File f) {
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(f));
			writeCardVsv(pool, writer);
			writer.close();
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * <p>
	 * writeCardVsv.
	 * </p>
	 * @param <T>
	 *
	 * @param d
	 *            a {@link forge.deck.Deck} object.
	 * @param out
	 *            a {@link java.io.BufferedWriter} object.
	 * @throws IOException 
	 */
	private static void writeCardVsv(final ItemPool<PaperCard> pool, final BufferedWriter out) throws IOException {
		out.append("[[Meta]]\n");
		out.append("`Set`").append("\n");
		out.append("\n");

		out.append("[[Cards]]\n");
		
		List<PaperCard> cards = pool.toFlatList();
		
		// Scenes go last, so we can set `genre`earth w/o affecting other cards
		cards.sort(new Comparator<PaperCard>() {
	        @Override
	        public int compare(final PaperCard a, final PaperCard b) {
	      	  boolean l = a.getRules().getType().isScene();
	      	  boolean r = b.getRules().getType().isScene();
	      	  if (l && !r) {
	      		  return 1;
	      	  }
	      	  if (r && !l) {
	      		  return -1;
	      	  }
	      	  return 0;
	        }
		});
		
		for (final PaperCard card : cards) {
			
			boolean isSplit = card.getRules().getSplitType().toString().contentEquals("Split");
			boolean isFlip = card.getRules().getSplitType().toString().contentEquals("Flip");
			boolean isTransform = card.getRules().getSplitType().toString().contentEquals("Transform");
			
			if (isSplit || isFlip) {
				out.append(outputSplitCard(card));
			}
			else {
				out.append(outputCard(card.getRules().getMainPart(), card));

				if (isTransform) {
					out.append(outputCard(card.getRules().getOtherPart(), card));
				}
			}

		}

		out.flush();
	}
	
	private static String outputCard(ICardFace card, PaperCard papercard) {
		StringBuilder out = new StringBuilder();
		String t;

		out.append("`CardName`").append(getField(card, "cardname")).append("\n");
		out.append("`ManaCost`").append(getField(card, "manacost")).append("\n");
		out.append("`Color`").append(getField(card, "color")).append("\n");

		// genre goes before types, in case of Scenes, which need `genre`earth
		if (card.getType().isScene()) {
			out.append("`Genre`").append("earth").append("\n");				
		}
		
		t = getField(card, "supertype");
		if (!t.contentEquals("")) {
			out.append("`SuperType`").append(t).append("\n");
		}
		t = getField(card, "cardtype");
		if (!t.contentEquals("")) {
			out.append("`CardType`").append(t).append("\n");
		}
		t = getField(card, "subtype");
		if (!t.contentEquals("")) {
			out.append("`SubType`").append(t).append("\n");
		}

		if (card.getPower() != null || card.getType().isPlaneswalker() || card.getType().isScene()) {
			out.append("`PowerToughness`").append(getField(card, "powertoughness")).append("\n");
		}

		out.append("`Rarity`").append(papercard.getRarity().toString()).append("\n");
		out.append("`RulesText`").append(getField(card, "rulestext")).append("\n");

		if (card.getType().isLand()) {
			out.append("`Border`").append("L").append("\n");
		}
		out.append("\n");
		
		return out.toString();
	}

	private static String outputSplitCard(PaperCard card) {
		StringBuilder out = new StringBuilder();
		String t;
		boolean isSplit = card.getRules().getSplitType().toString().contentEquals("Split");
		boolean isFlip = card.getRules().getSplitType().toString().contentEquals("Flip");
		
		ICardFace main = card.getRules().getMainPart();
		ICardFace alt = card.getRules().getOtherPart();

		out.append("`CardName`").append(getField(main, "cardname")).append(";;").append(getField(alt, "cardname")).append("\n");
		out.append("`ManaCost`").append(getField(main, "manacost"));
		if (isSplit) {
			out.append(";;").append(getField(alt, "manacost"));
		}
		out.append("\n");
		out.append("`Color`");
		if (isFlip) {
			out.append(getField(main, "color"));
		}
		else if (isSplit) {
			if (main.getColor() == alt.getColor()) {
				out.append(getField(main, "color"));
			}
			else {
				out.append("m");
			}
		}
		out.append("\n");

		t = getField(main, "supertype");
		if (!t.contentEquals("")) {
			out.append("`SuperType`").append(t).append("\n");
		}
		t = getField(main, "cardtype");
		out.append("`CardType`").append(t).append("\n");
		
		
		out.append("`SubType`");
		t = getField(main, "subtype");
		if (!t.contentEquals("")) {
			out.append(t);
		}
		out.append(";;");

		t = getField(alt, "supertype");
		if (!t.contentEquals("")) {
			out.append(t).append(" ");
		}
		t = getField(alt, "cardtype");
		if (!t.contentEquals("")) {
			out.append(t);
		}
		t = getField(alt, "subtype");
		if (!t.contentEquals("")) {
			out.append(" -- ").append(t);
		}
		out.append("\n");
		
		if (card.getRules().getPower() != null || card.getRules().getType().isPlaneswalker() || card.getRules().getType().isScene()) {
			out.append("`PowerToughness`");
			if (main.getPower() != null || main.getType().isPlaneswalker() || main.getType().isScene()) {
				out.append(getField(main, "powertoughness"));
			}
			if (alt.getPower() != null || alt.getType().isPlaneswalker() || alt.getType().isScene()) {
				out.append(";;").append(getField(alt, "powertoughness"));
			}
			out.append("\n");
		}				
		
		out.append("`Rarity`").append(card.getRarity().toString()).append("\n");

		out.append("`RulesText`").append(getField(main, "rulestext")).append(";;").append(getField(alt, "rulestext")).append("\n");
		
		if (isSplit && card.getRules().hasKeyword("Fuse")) {
			out.append("`Extra`").append("Fuse (you may cast both halves of this card from your hand).").append("\n");
		}
		
		// set frame
		out.append("`Frame`");
		if (card.getRules().getSplitType().toString().contentEquals("Split")) {
			out.append("vogonsplit");
		}
		else if (card.getRules().getSplitType().toString().contentEquals("Flip")) {
			out.append("flip");
		}
		out.append("\n");
		
		out.append("\n");
		return out.toString();
	}
	
	private static String getField(ICardFace card, String field) {
		StringBuilder data = new StringBuilder();
		field = field.toLowerCase();
		switch (field) {
			case "cardname":
				data.append(card.getName().toString());
				break;
			case "manacost":
				data.append(card.getManaCost().getBaseString());
				break;
			case "color":
				data.append(card.getColor().toShortString());
				break;
			case "powertoughness":
				if (card.getPower() != null) {
					data.append(card.getPower()+"/"+card.getToughness());
				}
				else if (card.getType().isPlaneswalker()) {
					data.append(Integer.toString(card.getInitialLoyalty()));
				}
				else if (card.getType().isScene()) {
					data.append(Integer.toString(card.getInitialFate()));
				}
				break;
			case "supertype":
				data.append(getTypeString(card.getType().getSupertypes()));
				break;
			case "cardtype":
				data.append(getTypeString(card.getType().getCoreTypes()));
				break;
			case "subtype":
				data.append(getTypeString(card.getType().getSubtypes()));
				break;
			case "rulestext":
				data.append(card.getOracleText().replaceAll("\\\\n", "^"));
				break;
			default:
				break;
		}
		
		return data.toString();
	}
	
	private static <T> String getTypeString(Iterable<T> iterable) {
		return iterable.toString().replaceAll("^.(.*).$", "$1").replace(",",  "");
	}
}