package forge.itemmanager.filters;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import forge.card.CardRarity;
import forge.card.CardRules;
import forge.item.PaperCard;
import forge.itemmanager.ItemManager;
import forge.itemmanager.SFilterUtil;
import forge.itemmanager.SpellShopManager;
import forge.itemmanager.SItemManagerUtil.StatTypes;
import forge.model.FModel;

import javax.swing.*;

import java.util.ArrayList;
import java.util.List;

/** 
 * TODO: Write javadoc for this type.
 *
 */
public class CardRarityFilter extends StatTypeFilter<PaperCard> {
    public CardRarityFilter(ItemManager<? super PaperCard> itemManager0) {
        super(itemManager0);
    }

    @Override
    public ItemFilter<PaperCard> createCopy() {
        return new CardRarityFilter(itemManager);
    }

    @Override
    protected void buildWidget(JPanel widget) {
        if (itemManager instanceof SpellShopManager) {
            addToggleButton(widget, StatTypes.PACK_OR_DECK);
        }
        addToggleButton(widget, StatTypes.RARITY_BASIC);
        addToggleButton(widget, StatTypes.RARITY_COMMON);
        addToggleButton(widget, StatTypes.RARITY_UNCOMMON);
        addToggleButton(widget, StatTypes.RARITY_RARE);
        addToggleButton(widget, StatTypes.RARITY_MYTHICRARE);
        addToggleButton(widget, StatTypes.RARITY_SPECIAL);
        addToggleButton(widget, StatTypes.RARITY_NONE);
        addToggleButton(widget, StatTypes.RARITY_UNKNOWN);
    }

	@Override
    protected final Predicate<PaperCard> buildPredicate() {
       return SFilterUtil.buildRarityFilter(buttonMap);
    }
}
