package forge.screens.deckeditor.controllers;

import forge.deck.Deck;
import forge.item.PaperCard;
import forge.properties.ForgeConstants;
import forge.util.ImageUtil;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

public class DeckVsvSerializer {
    public static void writeDeckVsv(final Deck d, final File f) {
        try {
            final BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            writeDeckVsv(d, writer);
            writer.close();
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * <p>
     * writeDeck.
     * </p>
     *
     * @param d
     *            a {@link forge.deck.Deck} object.
     * @param out
     *            a {@link java.io.BufferedWriter} object.
     */
    private static void writeDeckVsv(final Deck d, final BufferedWriter out) {
        Template temp;

        /* Create and adjust the configuration */
        final Configuration cfg = new Configuration();
        try {
            cfg.setClassForTemplateLoading(DeckVsvSerializer.class, "/");
            cfg.setObjectWrapper(new DefaultObjectWrapper());

            /*
             * ------------------------------------------------------------------
             * -
             */
            /*
             * You usually do these for many times in the application
             * life-cycle:
             */

            /* Get or create a template */
            temp = cfg.getTemplate("vsv-deck-template.ftl");

            /* Create a data-model */
            final Map<String, Object> root = new HashMap<>();

            int totalCards = 0;
            final Map<String, Integer> map = new TreeMap<>();
            for (final Entry<PaperCard, Integer> entry : d.getMain()) {
                map.put(entry.getKey().getName(), entry.getValue());
                totalCards += entry.getValue();
                // System.out.println(entry.getValue() + " " +
                // entry.getKey().getName());
            }

            root.put("title", d.getName());
            root.put("cardList", map);
            root.put("totalCards", totalCards);

            /* Merge data-model with template */
            temp.process(root, out);
            out.flush();
        } catch (final IOException | TemplateException e) {
            System.out.println(e.toString());
        }
    }
}