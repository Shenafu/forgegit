/*
 * Forge: Play Magic: the Gathering.
 * Copyright (C) 2011  Forge Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package forge.screens.deckeditor;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.text.NumberFormat;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableList;

import com.google.common.collect.Iterables;

import forge.ImageCache;
import forge.StaticData;
import forge.UiCommand;
import forge.assets.FSkinProp;
import forge.card.CardEdition;
import forge.card.CardRules;
import forge.card.mana.ManaCostShard;
import forge.deck.CardPool;
import forge.deck.Deck;
import forge.deck.DeckProxy;
import forge.deck.DeckgenUtil;
import forge.item.PaperCard;
import forge.model.FModel;
import forge.toolbox.FComboBox;
import forge.toolbox.FComboBoxPanel;
import forge.toolbox.FHtmlViewer;
import forge.toolbox.FLabel;
import forge.toolbox.FMouseAdapter;
import forge.toolbox.FOptionPane;
import forge.toolbox.FSkin;
import forge.toolbox.FTextField;
import forge.toolbox.FTextField.ChangeListener;
import forge.toolbox.FSkin.SkinnedPanel;
import forge.view.arcane.CardPanel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

@SuppressWarnings("serial")
public class AddBasicLandsDialog {
    private static final int WIDTH = 1600;
    private static final int HEIGHT = 740;
    private static final int ADD_BTN_SIZE = 64;
    private static final int LAND_PANEL_PADDING = 6;

    private final FComboBoxPanel<CardEdition> cbLandSet = new FComboBoxPanel<CardEdition>("Land Set:", FlowLayout.CENTER, Iterables.filter(StaticData.instance().getEditions(), CardEdition.Predicates.hasBasicLands));

    private final MainPanel panel = new MainPanel();
    private final LandPanel pnlPlains = new LandPanel("Plains");
    private final LandPanel pnlIsland = new LandPanel("Island");
    private final LandPanel pnlSwamp = new LandPanel("Swamp");
    private final LandPanel pnlMountain = new LandPanel("Mountain");
    private final LandPanel pnlForest = new LandPanel("Forest");

    private final FHtmlViewer lblDeckInfo = new FHtmlViewer();

    private final Deck deck;

    private FOptionPane optionPane;
    private int nonLandCount, oldLandCount;
    private CardEdition landSet;

    public AddBasicLandsDialog(Deck deck0) {
        this(deck0, null);
    }
    public AddBasicLandsDialog(Deck deck0, CardEdition defaultLandSet) {
        deck = deck0;

        if (defaultLandSet == null) {
            defaultLandSet = DeckProxy.getDefaultLandSet(deck);
        }

        panel.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        panel.add(cbLandSet);
        panel.add(pnlPlains);
        panel.add(pnlIsland);
        panel.add(pnlSwamp);
        panel.add(pnlMountain);
        panel.add(pnlForest);
        panel.add(lblDeckInfo);

        lblDeckInfo.setFont(FSkin.getRelativeFont(14));
        lblDeckInfo.addMouseListener(new FMouseAdapter() {
            @Override
            public void onLeftDoubleClick(MouseEvent e) {
                Map<ManaCostShard, Integer> suggestionMap = DeckgenUtil.suggestBasicLandCount(deck);
                pnlPlains.count = suggestionMap.get(ManaCostShard.WHITE);
                pnlIsland.count = suggestionMap.get(ManaCostShard.BLUE);
                pnlSwamp.count = suggestionMap.get(ManaCostShard.BLACK);
                pnlMountain.count = suggestionMap.get(ManaCostShard.RED);
                pnlForest.count = suggestionMap.get(ManaCostShard.GREEN);

                pnlPlains.txtCount.setText(String.valueOf(pnlPlains.count));
                pnlIsland.txtCount.setText(String.valueOf(pnlIsland.count));
                pnlSwamp.txtCount.setText(String.valueOf(pnlSwamp.count));
                pnlMountain.txtCount.setText(String.valueOf(pnlMountain.count));
                pnlForest.txtCount.setText(String.valueOf(pnlForest.count));
                
                updateDeckInfoLabel();
            }
        });
        lblDeckInfo.setToolTipText("Deck statistics. Double click to auto-suggest basic lands.");

        cbLandSet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                landSet = cbLandSet.getSelectedItem();
                pnlPlains.refreshArtChoices();
                pnlIsland.refreshArtChoices();
                pnlSwamp.refreshArtChoices();
                pnlMountain.refreshArtChoices();
                pnlForest.refreshArtChoices();
            }
        });
        cbLandSet.setSelectedItem(defaultLandSet);

        //initialize land counts based on current deck contents
        int halfCountW = 0; //track half shard count for each color to add to symbol count only if a full symbol is also found
        int halfCountU = 0;
        int halfCountB = 0;
        int halfCountR = 0;
        int halfCountG = 0;
        for (Entry<PaperCard, Integer> entry : deck.getMain()) {
            CardRules cardRules = entry.getKey().getRules();
            int count = entry.getValue();
            if (cardRules.getType().isLand()) {
                oldLandCount += count;
            }
            else {
                nonLandCount += count;

                for (ManaCostShard shard : cardRules.getManaCost()) {
                    boolean isMonoColor = shard.isMonoColor();
                    if (shard.isWhite()) {
                        if (isMonoColor) {
                            pnlPlains.symbolCount += count;
                            continue;
                        }
                        halfCountW += count;
                    }
                    if (shard.isBlue()) {
                        if (isMonoColor) {
                            pnlIsland.symbolCount += count;
                            continue;
                        }
                        halfCountU += count;
                    }
                    if (shard.isBlack()) {
                        if (isMonoColor) {
                            pnlSwamp.symbolCount += count;
                            continue;
                        }
                        halfCountB += count;
                    }
                    if (shard.isRed()) {
                        if (isMonoColor) {
                            pnlMountain.symbolCount += count;
                            continue;
                        }
                        halfCountR += count;
                    }
                    if (shard.isGreen()) {
                        if (isMonoColor) {
                            pnlForest.symbolCount += count;
                            continue;
                        }
                        halfCountG += count;
                    }
                }
            }

            //only account for half shards if full shards exist for a given color
            if (pnlPlains.symbolCount > 0 && halfCountW > 0) {
                pnlPlains.symbolCount += halfCountW * 0.5;
            }
            if (pnlIsland.symbolCount > 0 && halfCountU > 0) {
                pnlIsland.symbolCount += halfCountU * 0.5;
            }
            if (pnlSwamp.symbolCount > 0 && halfCountB > 0) {
                pnlSwamp.symbolCount += halfCountB * 0.5;
            }
            if (pnlMountain.symbolCount > 0 && halfCountR > 0) {
                pnlMountain.symbolCount += halfCountR * 0.5;
            }
            if (pnlForest.symbolCount > 0 && halfCountG > 0) {
                pnlForest.symbolCount += halfCountG * 0.5;
            }
        }

        updateDeckInfoLabel();
    }

    public CardPool show() {
        optionPane = new FOptionPane(null, "Add Basic Lands", null, panel, ImmutableList.of("OK", "Cancel"), 0);
        panel.revalidate();
        panel.repaint();
        optionPane.setVisible(true);

        int result = optionPane.getResult();

        optionPane.dispose();

        if (result == 0) {
            CardPool landsToAdd = new CardPool();
            pnlPlains.addToCardPool(landsToAdd);
            pnlIsland.addToCardPool(landsToAdd);
            pnlSwamp.addToCardPool(landsToAdd);
            pnlMountain.addToCardPool(landsToAdd);
            pnlForest.addToCardPool(landsToAdd);
            return landsToAdd;
        }
        return null;
    }

    private void updateDeckInfoLabel() {
        NumberFormat integer = NumberFormat.getIntegerInstance();
        NumberFormat percent = NumberFormat.getPercentInstance();
        int newLandCount = pnlPlains.count + pnlIsland.count + pnlSwamp.count + pnlMountain.count + pnlForest.count;
        double totalSymbolCount = pnlPlains.symbolCount + pnlIsland.symbolCount + pnlSwamp.symbolCount + pnlMountain.symbolCount + pnlForest.symbolCount;
        if (totalSymbolCount == 0) {
            totalSymbolCount = 1; //prevent divide by 0 error
        }
        int newTotalCount = nonLandCount + oldLandCount + newLandCount;
        lblDeckInfo.setText(FSkin.encodeSymbols("<div 'text-align: center;'>" +
                nonLandCount + " non-lands + " +
                oldLandCount + " lands + " +
                newLandCount + " added lands = " +
                newTotalCount + " cards</div><div style='text-align: center;'>" +
                "{W} " + integer.format(pnlPlains.symbolCount) + " (" + percent.format(pnlPlains.symbolCount / totalSymbolCount) + ") | " +
                "{U} " + integer.format(pnlIsland.symbolCount) + " (" + percent.format(pnlIsland.symbolCount / totalSymbolCount) + ") | " +
                "{B} " + integer.format(pnlSwamp.symbolCount) + " (" + percent.format(pnlSwamp.symbolCount / totalSymbolCount) + ") | " +
                "{R} " + integer.format(pnlMountain.symbolCount) + " (" + percent.format(pnlMountain.symbolCount / totalSymbolCount) + ") | " +
                "{G} " + integer.format(pnlForest.symbolCount) + " (" + percent.format(pnlForest.symbolCount / totalSymbolCount) + ")" + 
                "</div>", false));
    }

    private class MainPanel extends SkinnedPanel {
        private MainPanel() {
            super(null);
            setOpaque(false);
        }

        @Override
        public void doLayout() {
            int padding = 10;
            int x = padding;
            int y = padding;
            int w = getWidth() - 2 * padding;

            //layout land set combo box
            int comboBoxHeight = FTextField.HEIGHT * 2;
            cbLandSet.setBounds(x, y, w, comboBoxHeight);
            cbLandSet.setFont(FSkin.getRelativeFont(32).getBaseFont().deriveFont(32.0f));

            //layout card panel scroller
            y += comboBoxHeight + padding;
            int panelExtraHeight = pnlPlains.cbLandArt.getHeight() + ADD_BTN_SIZE + 2 * LAND_PANEL_PADDING;
            int panelWidth = (getWidth() - 6 * padding) / 5;
            int panelHeight = Math.round((float)panelWidth * CardPanel.ASPECT_RATIO) + panelExtraHeight;

            pnlPlains.setBounds(x, y, panelWidth, panelHeight);
            x += panelWidth + padding;
            pnlIsland.setBounds(x, y, panelWidth, panelHeight);
            x += panelWidth + padding;
            pnlSwamp.setBounds(x, y, panelWidth, panelHeight);
            x += panelWidth + padding;
            pnlMountain.setBounds(x, y, panelWidth, panelHeight);
            x += panelWidth + padding;
            pnlForest.setBounds(x, y, panelWidth, panelHeight);

            //layout info label
            x = padding;
            y += panelHeight + padding;
            lblDeckInfo.setBounds(x, y, w, getHeight() - y);
        }
    }

    private class LandPanel extends SkinnedPanel {
        private final LandCardPanel cardPanel;
        private final FComboBox<String> cbLandArt;
        private final FComboBox<String> cbCount;
        private final FTextField txtCount;
        private final String cardName;
        private PaperCard card;
        private int count, maxCount;
        private double symbolCount;

        private LandPanel(String cardName0) {
            super(null);
            setOpaque(false);            
            cardName = cardName0;
            cardPanel = new LandCardPanel();
            cbLandArt = new FComboBox<String>();
            cbLandArt.setFont(FSkin.getRelativeFont(12));
            cbLandArt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent arg0) {
                    int artIndex = cbLandArt.getSelectedIndex();
                    if (artIndex < 0) { return; }
                    card = generateCard(artIndex); //generate card for display
                    cardPanel.repaint();
                }
            });
            
            cbCount = new FComboBox<String>();
            cbCount.setFont(FSkin.getRelativeFont(14));
            int defMaxLands = 25;
            for (int i=0; i<defMaxLands; i++) {
            	cbCount.addItem("" + i);
            }
            cbCount.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent arg0) {
               	 txtCount.setText(cbCount.getSelectedItem());
                }
            });
            
            txtCount = new FTextField.Builder().text("0").build();
            txtCount.setFont(FSkin.getRelativeFont(14));
            txtCount.setBackground(Color.YELLOW);
            txtCount.addChangeListener(new ChangeListener() {
					@Override
					public void textChanged() {
						if (StringUtils.isNumeric(txtCount.getText())) {
	               	count = Integer.parseInt(txtCount.getText());
	                  updateDeckInfoLabel();
						}
					}
           });
            txtCount.addMouseWheelListener(new MouseWheelListener() {
					@Override
					public void mouseWheelMoved(MouseWheelEvent e) {
			      	 int delta = (int)Math.signum(e.getUnitsToScroll());
			      	 int newCount = Math.max(count + delta, 0);
			      	 txtCount.setText("" + newCount);
					}
            });

            add(cardPanel);
            add(cbLandArt);
            add(txtCount);
            add(cbCount);
        }

        private void addToCardPool(CardPool pool) {
            if (count == 0) { return; }
            int artIndex = cbLandArt.getSelectedIndex();
            if (artIndex < 0) { return; }

            if (artIndex > 0 && card != null) {
                pool.add(card, count); //simplify things if art index specified
            }
            else {
                for (int i = 0; i < count; i++) {
                    pool.add(generateCard(artIndex));
                }
            }
        }

        private PaperCard generateCard(int artIndex) {
            PaperCard c = FModel.getMagicDb().getCommonCards().getCard(cardName, landSet.getCode(), artIndex);
            if (c == null) {
                //if can't find land for this set, fall back to Zendikar lands
                c = FModel.getMagicDb().getCommonCards().getCard(cardName, "ZEN");
            }
            return c;
        }

        private void refreshArtChoices() {
            cbLandArt.removeAllItems();
            if (landSet == null) { return; }

            int artChoiceCount = FModel.getMagicDb().getCommonCards().getArtCount(cardName, landSet.getCode());
            cbLandArt.addItem("Assorted Art");
            for (int i = 1; i <= artChoiceCount; i++) {
                cbLandArt.addItem("Card Art " + i);
            }
        }

        @Override
        public void doLayout() {
            int width = getWidth();
            int height = getHeight();
            int x = 0;
            int y = height - ADD_BTN_SIZE;
            int labelWidth = (width) / 2;
            
            cbCount.setBounds(x, y, labelWidth, ADD_BTN_SIZE);
            x += labelWidth;
            txtCount.setBounds(x, y, labelWidth, ADD_BTN_SIZE);

            y -= FTextField.HEIGHT + LAND_PANEL_PADDING;
            cbLandArt.setBounds(0, y, width, FTextField.HEIGHT);

            int cardPanelHeight = y - LAND_PANEL_PADDING;
            int cardPanelWidth = Math.round((float)cardPanelHeight / CardPanel.ASPECT_RATIO);
            cardPanel.setBounds((width - cardPanelWidth) / 2, 0, cardPanelWidth, cardPanelHeight);
        }

        private class LandCardPanel extends SkinnedPanel {
            private LandCardPanel() {
                super(null);
                setOpaque(false);
            }

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                if (card == null) { return; }

                final Graphics2D g2d = (Graphics2D) g;

                int width = getWidth();
                int height = getHeight();

                final BufferedImage img = ImageCache.getImage(card, width, height);
                if (img != null) {
                    g2d.drawImage(img, null, (width - img.getWidth()) / 2, (height - img.getHeight()) / 2);
                }
            }
        }
    }
}
