package forge.screens.home.quest;

import forge.assets.FSkinProp;
import forge.gui.framework.DragCell;
import forge.gui.framework.DragTab;
import forge.gui.framework.EDocID;
import forge.interfaces.IButton;
import forge.quest.IVQuestStats;
import forge.screens.home.*;
import forge.toolbox.*;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Assembles Swing components of quest duels submenu singleton.
 * <p>
 * <br><br><i>(V at beginning of class name denotes a view class.)</i>
 */
public enum VSubmenuDuels implements IVSubmenu<CSubmenuDuels>, IVQuestStats {

	SINGLETON_INSTANCE;

	// Fields used with interface IVDoc
	private DragCell parentCell;
	private final DragTab tab = new DragTab("Quest Duels");

	// Other fields
	private final JPanel pnlStats = new JPanel();

	private final FScrollPanel pnlDuels = new FScrollPanel(new MigLayout("insets 0, gap 0, wrap"), true,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

	private final StartButton btnStart = new StartButton();
	private final FComboBoxWrapper<String> cbxPet = new FComboBoxWrapper<>();
	private final FComboBoxWrapper<String> cbxLotus = new FComboBoxWrapper<>();
	private final FComboBoxWrapper<String> cbxAlly = new FComboBoxWrapper<>();
	private final FComboBoxWrapper<String> cbxMatchLength = new FComboBoxWrapper<>();
	private final FCheckBox cbPlant = new FCheckBox("Summon Plant");
	private final FLabel lblZep = new FLabel.Builder().text("Launch Zeppelin").build();

	private final FLabel lblWorld = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_MAP)).fontStyle(Font.BOLD).build();
	private final FLabel lblLife = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_LIFE)).build();
	private final FLabel lblCredits = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_COINSTACK)).build();
	private final FLabel lblWins = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_PLUS)).build();
	private final FLabel lblLosses = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_MINUS)).build();
	private final FLabel lblWinStreak = new FLabel.Builder()
			.icon(FSkin.getIcon(FSkinProp.ICO_QUEST_PLUSPLUS)).build();
	private final LblHeader lblTitle = new LblHeader("Quest Mode: Duels");

	private final FLabel lblInfo = new FLabel.Builder().text("Select your next duel.")
			.fontStyle(Font.BOLD).fontAlign(SwingConstants.LEFT).build();

	private final FLabel lblCurrentDeck = new FLabel.Builder()
			.text("Current deck hasn't been set yet.").build();

	private final FLabel lblNextChallengeInWins = new FLabel.Builder()
			.text("Next challenge in wins hasn't been set yet.").build();

	private final FLabel btnUnlock = new FLabel.ButtonBuilder().text("Unlock Sets").build();
	private final FLabel btnTravel = new FLabel.ButtonBuilder().text("Travel").build();
	private final FLabel btnBazaar = new FLabel.ButtonBuilder().text("Bazaar").build();
	private final FLabel btnSpellShop = new FLabel.ButtonBuilder().text("Spell Shop").build();

	VSubmenuDuels() {

		final String constraintsStats = "w 100%!, pushy";
		final String constraintsButtons = "w 100%!";
		pnlStats.setLayout(new MigLayout("insets 0, gap 2%, wrap, hidemode 3"));
		pnlStats.add(btnUnlock, constraintsButtons);
		pnlStats.add(btnTravel, constraintsButtons);
		pnlStats.add(btnSpellShop, constraintsButtons);
		pnlStats.add(btnBazaar, constraintsButtons);
		pnlStats.add(lblWins, constraintsStats);
		pnlStats.add(lblLosses, constraintsStats);
		pnlStats.add(lblWinStreak, constraintsStats);
		pnlStats.add(lblLife, constraintsStats);
		pnlStats.add(lblCredits, constraintsStats);
		pnlStats.add(cbPlant, constraintsStats);
		cbxMatchLength.addTo(pnlStats, constraintsStats);
		cbxPet.addTo(pnlStats, constraintsStats);
		cbxLotus.addTo(pnlStats, constraintsStats);
		cbxAlly.addTo(pnlStats, constraintsStats);
		pnlStats.setOpaque(false);
		
		lblWorld.setFont(FSkin.getRelativeBoldFont(24));
	}

	/* (non-Javadoc)
	 * @see forge.view.home.IViewSubmenu#getGroup()
	 */
	@Override
	public EMenuGroup getGroupEnum() {
		return EMenuGroup.QUEST;
	}

	/* (non-Javadoc)
	 * @see forge.gui.home.IVSubmenu#getMenuTitle()
	 */
	@Override
	public String getMenuTitle() {
		return "Duels";
	}

	/* (non-Javadoc)
	 * @see forge.gui.home.IVSubmenu#getMenuName()
	 */
	@Override
	public EDocID getItemEnum() {
		return EDocID.HOME_QUESTDUELS;
	}

	/* (non-Javadoc)
	 * @see forge.view.home.IViewSubmenu#populate()
	 */
	@Override
	public void populate() {
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().removeAll();
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().setLayout(new MigLayout("insets 2%, gap 0, align center, wrap 1"));

		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblTitle, "w 80%!, gap 0 0 0 35px, span 2, ax right");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblWorld, "ax left, gapx 5%");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblInfo, "ax center");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblCurrentDeck, "ax center");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblNextChallengeInWins, "ax center");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(pnlStats, "split 2, w 10%, gap 2% 2% 0 0, ax right");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(pnlDuels, "w 65%, ax left");
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(btnStart, "x 30%, gap 0 0% 3% 3%");

		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().repaintSelf();
		VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().revalidate();
	}

	public FScrollPanel getPnlDuels() {
		return pnlDuels;
	}

	public JPanel getPnlStats() {
		return pnlStats;
	}

	public JLabel getLblTitle() {
		return lblTitle;
	}

	@Override
	public FLabel getLblWorld() {
		return lblWorld;
	}

	@Override
	public FLabel getLblLife() {
		return lblLife;
	}

	@Override
	public FLabel getLblCredits() {
		return lblCredits;
	}

	@Override
	public FLabel getLblWins() {
		return lblWins;
	}

	@Override
	public IButton getLblLosses() {
		return lblLosses;
	}

	@Override
	public FLabel getLblNextChallengeInWins() {
		return lblNextChallengeInWins;
	}

	@Override
	public FLabel getLblWinStreak() {
		return lblWinStreak;
	}

	@Override
	public FLabel getLblCurrentDeck() {
		return lblCurrentDeck;
	}

	@Override
	public FLabel getBtnBazaar() {
		return btnBazaar;
	}

	@Override
	public FLabel getBtnUnlock() {
		return btnUnlock;
	}

	@Override
	public FLabel getBtnTravel() {
		return btnTravel;
	}

	@Override
	public FLabel getBtnSpellShop() {
		return btnSpellShop;
	}

	@Override
	public FCheckBox getCbPlant() {
		return cbPlant;
	}

	@Override
	public FLabel getLblZep() {
		return lblZep;
	}

	@Override
	public FComboBoxWrapper<String> getCbxPet() {
		return cbxPet;
	}

	@Override
	public FComboBoxWrapper<String> getCbxLotus() {
		return cbxLotus;
	}

	@Override
	public FComboBoxWrapper<String> getCbxAlly() {
		return cbxAlly;
	}

	/**
	 * @return {@link javax.swing.JButton}
	 */
	public JButton getBtnStart() {
		return btnStart;
	}

	//========== Overridden from IVDoc

	/* (non-Javadoc)
	 * @see forge.gui.framework.IVDoc#getDocumentID()
	 */
	@Override
	public EDocID getDocumentID() {
		return EDocID.HOME_QUESTDUELS;
	}

	/* (non-Javadoc)
	 * @see forge.gui.framework.IVDoc#getTabLabel()
	 */
	@Override
	public DragTab getTabLabel() {
		return tab;
	}

	/* (non-Javadoc)
	 * @see forge.gui.framework.IVDoc#getLayoutControl()
	 */
	@Override
	public CSubmenuDuels getLayoutControl() {
		return CSubmenuDuels.SINGLETON_INSTANCE;
	}

	/* (non-Javadoc)
	 * @see forge.gui.framework.IVDoc#setParentCell(forge.gui.framework.DragCell)
	 */
	@Override
	public void setParentCell(final DragCell cell0) {
		this.parentCell = cell0;
	}

	/* (non-Javadoc)
	 * @see forge.gui.framework.IVDoc#getParentCell()
	 */
	@Override
	public DragCell getParentCell() {
		return parentCell;
	}

	@Override
	public FComboBoxWrapper<String> getCbxMatchLength() {
		return cbxMatchLength;
	}

	@Override
	public boolean isChallengesView() {
		return false;
	}

	@Override
	public boolean allowHtml() {
		return true;
	}
}
