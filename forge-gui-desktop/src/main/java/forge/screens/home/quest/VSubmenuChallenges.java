package forge.screens.home.quest;

import forge.assets.FSkinProp;
import forge.gui.framework.DragCell;
import forge.gui.framework.DragTab;
import forge.gui.framework.EDocID;
import forge.quest.IVQuestStats;
import forge.screens.home.*;
import forge.toolbox.*;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Assembles Swing components of quest challenges submenu singleton.
 *
 * <br><br><i>(V at beginning of class name denotes a view class.)</i>
 */
public enum VSubmenuChallenges implements IVSubmenu<CSubmenuChallenges>, IVQuestStats {
    /** */
    SINGLETON_INSTANCE;

    // Fields used with interface IVDoc
    private DragCell parentCell;
    private final DragTab tab = new DragTab("Quest Challenges");

    //========== INSTANTIATION
    private final JPanel pnlStats   = new JPanel();
    private final JPanel pnlStart   = new JPanel();

    private final FScrollPanel pnlChallenges = new FScrollPanel(new MigLayout("insets 0, gap 0, wrap"), true,
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    private final StartButton btnStart  = new StartButton();
    private final FComboBoxWrapper<String> cbxPet  = new FComboBoxWrapper<String>();
    private final FComboBoxWrapper<String> cbxLotus  = new FComboBoxWrapper<String>();
    private final FComboBoxWrapper<String> cbxAlly  = new FComboBoxWrapper<String>();
    private final FCheckBox cbPlant = new FCheckBox("Summon Plant");

    private final FComboBoxWrapper<String> cbxMatchLength  = new FComboBoxWrapper<String>();

    private final FLabel lblZep   = new FLabel.Builder().text("<html>Launch<br>Zeppelin</html>")
            .hoverable(true).icon(FSkin.getIcon(FSkinProp.ICO_QUEST_ZEP)).build();
    private final FLabel lblWorld   = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_MAP)).build();
    private final FLabel lblLife      = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_LIFE)).build();
    private final FLabel lblCredits   = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_COINSTACK)).build();
    private final FLabel lblWins      = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_PLUS)).build();
    private final FLabel lblLosses    = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_MINUS)).build();
    private final FLabel lblWinStreak = new FLabel.Builder()
        .icon(FSkin.getIcon(FSkinProp.ICO_QUEST_PLUSPLUS)).build();
    private final LblHeader lblTitle = new LblHeader("Quest Mode: Challenges");

    private final FLabel lblInfo = new FLabel.Builder().text("Which challenge will you attempt?")
            .fontStyle(Font.BOLD).build();

    private final FLabel lblCurrentDeck = new FLabel.Builder()
        .text("Current deck hasn't been set yet.").build();

    private final FLabel lblNextChallengeInWins = new FLabel.Builder()
        .text("Next challenge in wins hasn't been set yet.").build();

    private final FLabel btnUnlock = new FLabel.ButtonBuilder().text("Unlock Sets").build();
    private final FLabel btnTravel = new FLabel.ButtonBuilder().text("Travel").build();
    private final FLabel btnBazaar = new FLabel.ButtonBuilder().text("Bazaar").build();
    private final FLabel btnSpellShop = new FLabel.ButtonBuilder().text("Spell Shop").build();

    /**
     * Constructor.
     */
    private VSubmenuChallenges() {
        final String constraints = "gap 0 0 0 5px";
        pnlStats.setLayout(new MigLayout("insets 0, gap 0, wrap, hidemode 3"));
  		  pnlStats.add(btnUnlock, "w 100%, gap 0 0 0 10px");
  		  pnlStats.add(btnTravel, "w 100%, gap 0 0 0 10px");
  		  pnlStats.add(btnSpellShop, "w 100%, gap 0 0 0 10px");
  		  pnlStats.add(btnBazaar, "w 100%, gap 0 0 0 10px");
        pnlStats.add(lblWins, constraints);
        pnlStats.add(lblLosses, constraints);
        pnlStats.add(lblWinStreak, "gap 0 0 0 5px");
        pnlStats.add(lblLife, constraints);
        pnlStats.add(lblCredits, constraints);
        pnlStats.add(cbPlant, constraints);
        cbxMatchLength.addTo(pnlStats, constraints);
        cbxPet.addTo(pnlStats, constraints);
        cbxLotus.addTo(pnlStats, constraints);
        cbxAlly.addTo(pnlStats, constraints);
        pnlStats.add(lblZep, "w 100%, gap 0 0 0 5px");
        pnlStats.setOpaque(false);
  		
        lblWorld.setFont(FSkin.getRelativeBoldFont(24));
    }

    /* (non-Javadoc)
     * @see forge.view.home.IViewSubmenu#getGroup()
     */
    @Override
    public EMenuGroup getGroupEnum() {
        return EMenuGroup.QUEST;
    }

    /* (non-Javadoc)
     * @see forge.gui.home.IVSubmenu#getMenuTitle()
     */
    @Override
    public String getMenuTitle() {
        return "Challenges";
    }

    /* (non-Javadoc)
     * @see forge.gui.home.IVSubmenu#getMenuName()
     */
    @Override
    public EDocID getItemEnum() {
        return EDocID.HOME_QUESTCHALLENGES;
    }

    /* (non-Javadoc)
     * @see forge.view.home.IViewSubmenu#populate()
     */
    @Override
    public void populate() {
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().removeAll();
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().setLayout(new MigLayout("insets 2%, gap 0, align center, wrap 1"));
        
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblTitle, "w 80%!, gap 0 0 0 35px, span 2, ax right");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblWorld, "ax left, gapx 5%");
  		  VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblInfo, "ax center");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblCurrentDeck, "ax center");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblNextChallengeInWins, "ax center");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(pnlStats, "split 2, w 10%, gap 2% 2% 0 0, ax right");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(pnlChallenges, "w 65%, ax left");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(btnStart, "x 30%, gap 0 0% 3% 3%");

        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().repaintSelf();
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().revalidate();
    }

    public FScrollPanel getPnlChallenges() {
        return pnlChallenges;
    }

    public JPanel getPnlStats() {
        return pnlStats;
    }

    public JPanel getPnlStart() {
        return pnlStart;
    }

    public JLabel getLblTitle() {
        return lblTitle;
    }

    @Override
    public FLabel getLblWorld() {
        return lblWorld;
    }

    @Override
    public FLabel getLblLife() {
        return lblLife;
    }

    @Override
    public FLabel getLblCredits() {
        return lblCredits;
    }

    @Override
    public FLabel getLblWins() {
        return lblWins;
    }

    @Override
    public FLabel getLblLosses() {
        return lblLosses;
    }

    @Override
    public FLabel getLblCurrentDeck() {
        return lblCurrentDeck;
    }

    @Override
    public FLabel getLblNextChallengeInWins() {
        return lblNextChallengeInWins;
    }

    @Override
    public FLabel getLblWinStreak() {
        return lblWinStreak;
    }

    @Override
    public FLabel getBtnBazaar() {
        return btnBazaar;
    }

    @Override
    public FLabel getBtnUnlock() {
        return btnUnlock;
    }

    @Override
    public FLabel getBtnTravel() {
        return btnTravel;
    }
    @Override
    public FLabel getBtnSpellShop() {
        return btnSpellShop;
    }

    @Override
    public FCheckBox getCbPlant() {
        return cbPlant;
    }

    @Override
    public FLabel getLblZep() {
        return lblZep;
    }

    @Override
    public FComboBoxWrapper<String> getCbxPet() {
        return cbxPet;
    }

    @Override
    public FComboBoxWrapper<String> getCbxLotus() {
        return cbxLotus;
    }

    @Override
    public FComboBoxWrapper<String> getCbxAlly() {
        return cbxAlly;
    }

    /** @return {@link javax.swing.JButton} */
    public JButton getBtnStart() {
        return btnStart;
    }

    //========== Overridden from IVDoc

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getDocumentID()
     */
    @Override
    public EDocID getDocumentID() {
        return EDocID.HOME_QUESTCHALLENGES;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getTabLabel()
     */
    @Override
    public DragTab getTabLabel() {
        return tab;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getLayoutControl()
     */
    @Override
    public CSubmenuChallenges getLayoutControl() {
        return CSubmenuChallenges.SINGLETON_INSTANCE;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#setParentCell(forge.gui.framework.DragCell)
     */
    @Override
    public void setParentCell(final DragCell cell0) {
        this.parentCell = cell0;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getParentCell()
     */
    @Override
    public DragCell getParentCell() {
        return parentCell;
    }

    @Override
    public FComboBoxWrapper<String> getCbxMatchLength() {
        return cbxMatchLength;
    }

    @Override
    public boolean isChallengesView() {
        return true;
    }

    @Override
    public boolean allowHtml() {
        return true;
    }
}
