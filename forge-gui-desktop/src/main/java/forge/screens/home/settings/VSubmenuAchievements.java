package forge.screens.home.settings;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import forge.achievement.Achievement;
import forge.achievement.AchievementCollection;
import forge.assets.FSkinProp;
import forge.game.card.CardView;
import forge.gui.framework.DragCell;
import forge.gui.framework.DragTab;
import forge.gui.framework.EDocID;
import forge.item.IPaperCard;
import forge.screens.home.EMenuGroup;
import forge.screens.home.IVSubmenu;
import forge.screens.home.VHomeUI;
import forge.toolbox.*;
import forge.toolbox.FComboBox.TextAlignment;
import forge.toolbox.FSkin.Colors;
import forge.toolbox.FSkin.SkinColor;
import forge.toolbox.FSkin.SkinFont;
import forge.toolbox.FSkin.SkinImage;
import forge.toolbox.special.CardZoomer;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;

/**
 * Assembles Swing components of achievements submenu singleton.
 *
 * <br><br><i>(V at beginning of class name denotes a view class.)</i>
 */
public enum VSubmenuAchievements implements IVSubmenu<CSubmenuAchievements> {
    /** */
    SINGLETON_INSTANCE;

    private static final int MIN_SHELVES = 2;
    private static final int TROPHIES_PER_SHELF = 4;
    private static final int TOOLTIP_PADDING = 15;
    private static final int TROPHY_PADDING = 90;
    private static final int SCALE_FACTOR = 4;
    private static final int SUBTITLE_PADDING = 48;
    private static final SkinFont NAME_FONT = FSkin.getRelativeBoldFont(16);
    private static final SkinFont DESC_FONT = FSkin.getFont();
    private static final SkinColor TEXT_COLOR = FSkin.getColor(Colors.CLR_TEXT);
    private static final SkinColor NOT_EARNED_COLOR = TEXT_COLOR.alphaColor(128);
    private static final SkinColor TEXTURE_OVERLAY_COLOR = FSkin.getColor(Colors.CLR_THEME);
    private static final SkinColor BORDER_COLOR = FSkin.getColor(Colors.CLR_BORDERS);

    // Fields used with interface IVDoc
    private DragCell parentCell;
    private final DragTab tab = new DragTab("Achievements");
    private final FLabel lblTitle = new FLabel.Builder()
        .text("Achievements").fontAlign(SwingConstants.CENTER)
        .opaque(true).build();
    private final FComboBox<AchievementCollection> cbCollections = new FComboBox<AchievementCollection>();
    private final TrophyCase trophyCase = new TrophyCase();
    private final FScrollPane scroller = new FScrollPane(trophyCase, false,
            ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    private VSubmenuAchievements() {
        lblTitle.setBackground(FSkin.getColor(FSkin.Colors.CLR_THEME2));

        trophyCase.setMinimumSize(new Dimension( FSkinProp.IMG_TROPHY_SHELF.getWidth() * SCALE_FACTOR, 0));
        trophyCase.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseMoved(MouseEvent e) {
                trophyCase.setSelectedAchievement(getAchievementAt(e.getX(), e.getY()));
            }

            @Override
            public void mouseDragged(MouseEvent e) {
            }
        });
        trophyCase.addMouseListener(new FMouseAdapter() {
            private boolean preventMouseOut;

            @Override
            public void onMiddleMouseDown(MouseEvent e) {
                showCard(e);
            }

            @Override
            public void onMiddleMouseUp(MouseEvent e) {
                CardZoomer.SINGLETON_INSTANCE.closeZoomer();
            }

            @Override
            public void onLeftDoubleClick(MouseEvent e) {
                showCard(e);
            }

            private void showCard(MouseEvent e) {
                final Achievement achievement = getAchievementAt(e.getX(), e.getY());
                if (achievement != null) {
                    final IPaperCard pc = achievement.getPaperCard();
                    if (pc != null) {
                        preventMouseOut = true;
                        CardZoomer.SINGLETON_INSTANCE.setCard(CardView.getCardForUi(pc).getCurrentState(), true);
                    }
                }
            }

            @Override
            public void onMouseExit(MouseEvent e) {
                if (preventMouseOut) {
                    preventMouseOut = false;
                    return;
                }
                trophyCase.setSelectedAchievement(null);
            }
        });

        AchievementCollection.buildComboBox(cbCollections);

        cbCollections.setSkinFont(FSkin.getRelativeBoldFont(14));
        cbCollections.setTextAlignment(TextAlignment.CENTER);
        cbCollections.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setAchievements(cbCollections.getSelectedItem());
            }
        });
        cbCollections.setSelectedIndex(0);
    }

    /* (non-Javadoc)
     * @see forge.view.home.IViewSubmenu#populate()
     */
    @Override
    public void populate() {
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().removeAll();

        String width = "w " + (trophyCase.getMinimumSize().width + 20) + "px!";
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().setLayout(new MigLayout("insets 10, gap 10, wrap"));
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(lblTitle, "pushx, growx");

        JPanel panel = new JPanel();
        panel.setOpaque(false);
        panel.setLayout(new MigLayout("insets 0, gap 5, wrap, align center"));
        panel.add(cbCollections, width + "");
        panel.add(scroller, width + ", pushy, growy");
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().add(panel, "push, grow");

        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().repaintSelf();
        VHomeUI.SINGLETON_INSTANCE.getPnlDisplay().revalidate();
    }

    /* (non-Javadoc)
     * @see forge.view.home.IViewSubmenu#getGroup()
     */
    @Override
    public EMenuGroup getGroupEnum() {
        return EMenuGroup.SETTINGS;
    }

    /* (non-Javadoc)
     * @see forge.gui.home.IVSubmenu#getMenuTitle()
     */
    @Override
    public String getMenuTitle() {
        return "Achievements";
    }

    /* (non-Javadoc)
     * @see forge.gui.home.IVSubmenu#getItemEnum()
     */
    @Override
    public EDocID getItemEnum() {
        return EDocID.HOME_ACHIEVEMENTS;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getDocumentID()
     */
    @Override
    public EDocID getDocumentID() {
        return EDocID.HOME_ACHIEVEMENTS;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getTabLabel()
     */
    @Override
    public DragTab getTabLabel() {
        return tab;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getLayoutControl()
     */
    @Override
    public CSubmenuAchievements getLayoutControl() {
        return CSubmenuAchievements.SINGLETON_INSTANCE;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#setParentCell(forge.gui.framework.DragCell)
     */
    @Override
    public void setParentCell(DragCell cell0) {
        this.parentCell = cell0;
    }

    /* (non-Javadoc)
     * @see forge.gui.framework.IVDoc#getParentCell()
     */
    @Override
    public DragCell getParentCell() {
        return parentCell;
    }

    private void setAchievements(AchievementCollection achievements0) {
        trophyCase.achievements = achievements0;

        trophyCase.shelfCount = (int)Math.ceil((double)achievements0.getCount() / (double)TROPHIES_PER_SHELF);
        if (trophyCase.shelfCount < MIN_SHELVES) {
            trophyCase.shelfCount = MIN_SHELVES;
        }

        trophyCase.setMinimumSize(new Dimension(trophyCase.getMinimumSize().width, (FSkinProp.IMG_TROPHY_CASE_TOP.getHeight() + trophyCase.shelfCount * FSkinProp.IMG_TROPHY_SHELF.getHeight()) * SCALE_FACTOR));
        trophyCase.setPreferredSize(trophyCase.getMinimumSize());
        scroller.revalidate();
        scroller.repaint();
    }

    private Achievement getAchievementAt(float x0, float y0) {
        float w = scroller.getWidth();
        float shelfH = FSkinProp.IMG_TROPHY_SHELF.getHeight() * SCALE_FACTOR;
        float trophyW = FSkinProp.IMG_COMMON_TROPHY.getWidth() * SCALE_FACTOR + TROPHY_PADDING * 2;
        float x = (w - TROPHIES_PER_SHELF * trophyW) / 2;
        float y = FSkinProp.IMG_TROPHY_CASE_TOP.getHeight() * SCALE_FACTOR;

        int trophyCount = 0;
        float startX = x;

        for (Achievement achievement : trophyCase.achievements) {
            if (trophyCount == TROPHIES_PER_SHELF) {
                trophyCount = 0;
                x = startX;
                y += shelfH;
            }

            if (x <= x0 && x0 < x + trophyW && y <= y0 && y0 < y + shelfH) {
                return achievement;
            }

            trophyCount++;
            x += trophyW;
        }
        return null;
    }

    @SuppressWarnings("serial")
    private static class TrophyCase extends JPanel {
        private static final SkinImage imgTop = FSkin.getImage(FSkinProp.IMG_TROPHY_CASE_TOP);
        private static final SkinImage imgShelf = FSkin.getImage(FSkinProp.IMG_TROPHY_SHELF);
        private static final SkinImage imgTrophyPlate = FSkin.getImage(FSkinProp.IMG_TROPHY_PLATE);
        private static final Font font = FSkin.getRelativeFixedFont(14).deriveFont(Font.BOLD);
        private static final Font subFont = FSkin.getFixedFont();
        private static final Color foreColor = new Color(239, 220, 144);

        private AchievementCollection achievements;
        private int shelfCount;
        private Achievement selectedAchievement;

        private void setSelectedAchievement(Achievement selectedAchievement0) {
            if (selectedAchievement == selectedAchievement0) { return; }
            selectedAchievement = selectedAchievement0;
            repaint();
        }

        @Override
        public void paintComponent(final Graphics g) {
            Graphics2D g2d = (Graphics2D) g.create();

            Dimension imgTopSize = imgTop.getSizeForPaint(g2d);
            RenderingHints hints = new RenderingHints(
                    RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            g2d.setRenderingHints(hints);
            
            // paint trophy case top

            int topX = 0;
            int topY = 0;
            int topW = imgTopSize.width * SCALE_FACTOR;
            int topH = imgTopSize.height * SCALE_FACTOR;

            FSkin.drawImage(g2d, imgTop, topX, topY, topW, topH);
            
            // paint trophy case

            Dimension imgShelfSize = imgShelf.getSizeForPaint(g2d);
            int shelfH = imgShelfSize.height * SCALE_FACTOR;
            int shelfW = imgShelfSize.width * SCALE_FACTOR;
            int shelfX = topX;
            int shelfY = topY + topH;
            for (int i = 0; i < shelfCount; i++) {
                FSkin.drawImage(g2d, imgShelf, shelfX, shelfY, shelfW, shelfH);
                shelfY += shelfH;
            }
            
            // paint trophies

            int trophyImageWidth = FSkinProp.IMG_COMMON_TROPHY.getWidth() * SCALE_FACTOR;
            int trophyW = trophyImageWidth + TROPHY_PADDING * 2;
            int trophyH = FSkinProp.IMG_COMMON_TROPHY.getHeight() * SCALE_FACTOR;
            Dimension trophyPlateSize = imgTrophyPlate.getSizeForPaint(g2d);
            int plateW = trophyPlateSize.width * SCALE_FACTOR;
            int plateH = trophyPlateSize.height * SCALE_FACTOR;
            int plateY = topH + shelfH - plateH;
            int trophyX = shelfX + (shelfW - TROPHIES_PER_SHELF * trophyW) / 2;
            int trophyY = plateY - trophyH;

            FontMetrics fm;
            String label = "";
            String subtitle = "";
            String selectLabel = "";
            String selectSubtitle = "";
            int trophyCount = 0;
            int startX = trophyX;
            int textY;
            int dy = shelfH;
            int plateOffset = (trophyW - plateW) / 2;
            Rectangle selectRect = null;

            for (Achievement achievement : achievements) {
                if (trophyCount == TROPHIES_PER_SHELF) {
                    trophyCount = 0;
                    trophyY += dy;
                    plateY += dy;
                    trophyX = startX;
                }
                FSkin.drawImage(g2d, (SkinImage)achievement.getImage(), trophyX + TROPHY_PADDING, trophyY, trophyImageWidth, trophyH);
                FSkin.drawImage(g2d, imgTrophyPlate, trophyX + plateOffset, plateY, plateW, plateH);

                g2d.setColor(foreColor);
                g2d.setFont(font);

                fm = g2d.getFontMetrics();
                label = achievement.getDisplayName();
                textY = plateY + (plateH * 2 / 3 - fm.getHeight()) / 2 + fm.getAscent();
                g2d.drawString(label, trophyX + plateOffset + (plateW - fm.stringWidth(label)) / 2, textY);

                subtitle = achievement.getSubTitle(false);
                if (subtitle != null) {
                    textY += SUBTITLE_PADDING; //fm.getAscent();
                    g2d.setFont(subFont);
                    fm = g2d.getFontMetrics();
                    g2d.drawString(label, trophyX + plateOffset + (plateW - fm.stringWidth(label)) / 2, textY);
                }

                if (achievement == selectedAchievement) {
                    g2d.setColor(Color.GREEN);
                    int arcSize = trophyW / 10;
                    g2d.drawRoundRect(trophyX + TROPHY_PADDING, trophyY, trophyImageWidth, trophyH, arcSize, arcSize);
                    selectRect = new Rectangle(trophyX + TROPHY_PADDING, trophyY, trophyImageWidth, trophyH);
                    selectLabel = label;
                    selectSubtitle = subtitle;
                }

                trophyCount++;
                trophyX += trophyW;
            }


         //draw tooltip for selected achievement if needed
            
         if (selectRect != null) {      
            /*
            FLabel lblSubTitle = new FLabel.Builder().opaque(true).hoverable(true).text(subTitle).build();
            FLabel sharedDesc = new FLabel.Builder().opaque(true).hoverable(true).text(selectedAchievement.getSharedDesc()).build();
            FLabel mythicDesc = new FLabel.Builder().opaque(true).hoverable(true).text(selectedAchievement.getMythicDesc()).build();
            FLabel rareDesc = new FLabel.Builder().opaque(true).hoverable(true).text(selectedAchievement.getRareDesc()).build();
            FLabel uncommonDesc = new FLabel.Builder().opaque(true).hoverable(true).text(selectedAchievement.getUncommonDesc()).build();
            FLabel commonDesc = new FLabel.Builder().opaque(true).hoverable(true).text(selectedAchievement.getCommonDesc()).build();
            
            JPanel pnlTooltip = new JPanel();
            pnlTooltip.setOpaque(false);
            pnlTooltip.setLayout(new MigLayout("insets 0, gap 0, wrap 2"));
            
            pnlTooltip.add(lblSubTitle, "w 100%");
            pnlTooltip.add(sharedDesc, "w 100%");
            pnlTooltip.add(mythicDesc, "w 100%");
            pnlTooltip.add(rareDesc, "w 100%");
            pnlTooltip.add(uncommonDesc, "w 100%");
            pnlTooltip.add(commonDesc, "w 100%");

            int ttW = trophyW - 2 * TOOLTIP_PADDING - 1;
            int ttH = pnlTooltip.getHeight();
            int ttX = trophyX + plateOffset;
            int ttY = plateY;
            FScrollPane scroller = (FScrollPane)getParent().getParent();
            if (ttY + ttH - scroller.getVerticalScrollBar().getValue() > scroller.getHeight()) {
                if (ttY - TOOLTIP_PADDING > ttH) {
                	ttY = ttY - ttH - TOOLTIP_PADDING;
                }
                else {
                	ttY = getHeight() - ttH;
                }
            }
            
            FSkin.drawImage(g2d, FSkin.getImage(FSkinProp.BG_TEXTURE), ttX, ttY, ttW, ttH);
            FSkin.setGraphicsColor(g2d, TEXTURE_OVERLAY_COLOR);
            g2d.fillRect(ttX, ttY, ttW, ttH);
            FSkin.setGraphicsColor(g2d, BORDER_COLOR);
            g2d.drawRect(ttX, ttY, ttW, ttH);
            
            this.add(pnlTooltip, "ax center, ay center");
            //*/
            
            //*
            String sharedDesc = selectedAchievement.getSharedDesc();
            String mythicDesc = selectedAchievement.getMythicDesc();
            String rareDesc = selectedAchievement.getRareDesc();
            String uncommonDesc = selectedAchievement.getUncommonDesc();
            String commonDesc = selectedAchievement.getCommonDesc();

            int nameHeight = NAME_FONT.getFontMetrics().getHeight();
            int descHeight = DESC_FONT.getFontMetrics().getHeight();

            int ttH = nameHeight + TOOLTIP_PADDING * 5 / 2;
            FSkin.setGraphicsFont(g2d, NAME_FONT);
            double dttW = g2d.getFontMetrics().getStringBounds(selectLabel, g2d).getWidth();
            FSkin.setGraphicsFont(g2d, DESC_FONT);
            fm = g2d.getFontMetrics();
            
            if (selectSubtitle != null) {
         		ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(selectSubtitle, g2d).getWidth());
            }
            if (sharedDesc != null) {
					ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(sharedDesc, g2d).getWidth());
            }
            if (mythicDesc != null) {
					ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(mythicDesc, g2d).getWidth());
            }
            if (rareDesc != null) {
					ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(rareDesc, g2d).getWidth());
            }
            if (uncommonDesc != null) {
					ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(uncommonDesc, g2d).getWidth());
            }
            if (commonDesc != null) {
					ttH += descHeight;
					dttW = Math.max(dttW, fm.getStringBounds(commonDesc, g2d).getWidth());
            }
            
            dttW *= 1.5; // ratio of fontHeight / fontWidth
            int ttW = Math.max((int)dttW, trophyW);

            int ttX = Math.max(startX, selectRect.x + (trophyImageWidth - ttW) / 2);
            int ttY = selectRect.y + trophyH + TOOLTIP_PADDING;
            FScrollPane scroller = (FScrollPane)getParent().getParent();
            if (ttY + ttH - scroller.getVerticalScrollBar().getValue() > scroller.getHeight()) {
                if (ttY - TOOLTIP_PADDING > ttH) {
                	ttY = ttY - ttH - TOOLTIP_PADDING;
                }
                else {
                	ttY = getHeight() - ttH;
                }
            }

            FSkin.drawImage(g2d, FSkin.getImage(FSkinProp.BG_TEXTURE), ttX, ttY, (int)ttW, ttH);
            FSkin.setGraphicsColor(g2d, TEXTURE_OVERLAY_COLOR);
            g2d.fillRect(ttX, ttY, (int)ttW, ttH);
            FSkin.setGraphicsColor(g2d, BORDER_COLOR);
            g2d.drawRect(ttX, ttY, (int)ttW, ttH);

            ttX += TOOLTIP_PADDING;
            ttX += TOOLTIP_PADDING;

            FSkin.setGraphicsFont(g2d, NAME_FONT);
            FSkin.setGraphicsColor(g2d, TEXT_COLOR);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            fm = g2d.getFontMetrics();
            ttY += fm.getAscent();
            g2d.drawString(selectedAchievement.getDisplayName(), ttX, ttY);
            ttY += nameHeight;

            FSkin.setGraphicsFont(g2d, DESC_FONT);
            if (selectSubtitle != null) {
                g2d.drawString(selectSubtitle, ttX, ttY);
                ttY += descHeight;
            }
            ttY += TOOLTIP_PADDING;
            if (sharedDesc != null) {
                g2d.drawString(selectedAchievement.isSpecial() ? sharedDesc : sharedDesc + "...", ttX, ttY);
                ttY += descHeight;
            }
            if (mythicDesc != null) {
                FSkin.setGraphicsColor(g2d, selectedAchievement.earnedMythic() ? TEXT_COLOR : NOT_EARNED_COLOR);
                g2d.drawString(selectedAchievement.isSpecial() ? mythicDesc : "(Mythic) " + mythicDesc, ttX, ttY); //handle flavor text here too
                ttY += descHeight;
            }
            if (rareDesc != null) {
                FSkin.setGraphicsColor(g2d, selectedAchievement.earnedRare() ? TEXT_COLOR : NOT_EARNED_COLOR);
                g2d.drawString("(Rare) " + rareDesc, ttX, ttY);
                ttY += descHeight;
            }
            if (uncommonDesc != null) {
                FSkin.setGraphicsColor(g2d, selectedAchievement.earnedUncommon() ? TEXT_COLOR : NOT_EARNED_COLOR);
                g2d.drawString("(Uncommon) " + uncommonDesc, ttX, ttY);
                ttY += descHeight;
            }
            if (commonDesc != null) {
                FSkin.setGraphicsColor(g2d, selectedAchievement.earnedCommon() ? TEXT_COLOR : NOT_EARNED_COLOR);
                g2d.drawString("(Common) " + commonDesc, ttX, ttY);
            }
            //*/
        }

            g2d.dispose();
        }
    }
}
