package forge.deckchooser;

import forge.deck.CardPool;
import forge.deck.Deck;
import forge.deck.DeckSection;
import forge.game.card.CardView;
import forge.gui.CardDetailPanel;
import forge.gui.CardPicturePanel;
import forge.item.IPaperCard;
import forge.item.PaperCard;
import forge.itemmanager.CardManager;
import forge.itemmanager.ItemManagerConfig;
import forge.itemmanager.ItemManagerContainer;
import forge.itemmanager.ItemManagerModel;
import forge.itemmanager.views.ImageView;
import forge.model.FModel;
import forge.properties.ForgePreferences;
import forge.toolbox.FButton;
import forge.toolbox.FOptionPane;
import forge.toolbox.FSkin;
import forge.view.FDialog;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

@SuppressWarnings("serial")
public class FDeckViewer extends FDialog {
	private final Deck deck;
	private final List<DeckSection> sections = new ArrayList<DeckSection>();
	private final CardManager cardManager;
	private DeckSection currentSection;

	private final CardDetailPanel cardDetail = new CardDetailPanel();
	private final CardPicturePanel cardPicture = new CardPicturePanel();
	private final FButton btnCopyToClipboard = new FButton("Copy to Clipboard");
	private final FButton btnChangeSection = new FButton("Change Section");
	private final FButton btnClose = new FButton("Close");

	public static void show(final Deck deck) {
		if (deck == null) { return; }

		final FDeckViewer deckViewer = new FDeckViewer(deck);
		deckViewer.setVisible(true);
		deckViewer.dispose();
	}

	private FDeckViewer(final Deck deck0) {
		this.deck = deck0;
		this.setTitle(deck.getName());
		this.cardManager = new CardManager(null, false, false) {
			@Override //show hovered card in Image View in dialog instead of main Detail/Picture panes
			protected ImageView<PaperCard> createImageView(final ItemManagerModel<PaperCard> model0) {
				return new ImageView<PaperCard>(this, model0) {
					@Override
					protected void showHoveredItem(PaperCard item) {
						final CardView card = CardView.getCardForUi(item);
						if (card == null) { return; }

						cardDetail.setCard(card);
						cardPicture.setCard(card.getCurrentState());
					}
				};
			}
		};
		this.cardManager.setPool(deck.getMain());
		this.cardManager.addSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				final IPaperCard paperCard = cardManager.getSelectedItem();
				if (paperCard == null) { return; }

				final CardView card = CardView.getCardForUi(paperCard);
				if (card == null) { return; }

				cardDetail.setCard(card);
				cardPicture.setCard(card.getCurrentState());
			}
		});

		for (Entry<DeckSection, CardPool> entry : deck) {
			this.sections.add(entry.getKey());
		}
		this.currentSection = DeckSection.Main;
		updateCaption();

		btnCopyToClipboard.setFont(FSkin.getRelativeFont(14));
		this.btnCopyToClipboard.setFocusable(false);
		this.btnCopyToClipboard.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FDeckViewer.this.copyToClipboard();
			}
		});

		btnChangeSection.setFont(FSkin.getRelativeFont(14));
		this.btnChangeSection.setFocusable(false);
		if (this.sections.size() > 1) {
			this.btnChangeSection.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					FDeckViewer.this.changeSection();
				}
			});
		}
		else {
			this.btnChangeSection.setEnabled(false);
		}

		btnClose.setFont(FSkin.getRelativeFont(14));
		this.btnClose.setFocusable(false);
		this.btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FDeckViewer.this.setVisible(false);
			}
		});

		final int width;
		final int height;
		if(FModel.getPreferences().getPrefBoolean(ForgePreferences.FPref.UI_SMALL_DECK_VIEWER)){
			width = 800;
			height = 600;
		}
		else {
			GraphicsDevice gd = this.getGraphicsConfiguration().getDevice();

			width = (int)(gd.getDisplayMode().getWidth() * 0.8);
			height = (int)(gd.getDisplayMode().getHeight() * 0.9);
		}

		this.setPreferredSize(new Dimension(width, height));
		this.setSize(width, height);

		this.cardPicture.setOpaque(false);

		JPanel mainPanel = new JPanel(new MigLayout("wrap 2, fill"));
		JPanel leftPanel = new JPanel(new MigLayout("insets 10, gap 10, wrap, fill"));
		JPanel cardPanel = new JPanel(new MigLayout("insets 10, gap 10, wrap, fill"));
		JPanel buttonPanel = new JPanel(new MigLayout("insets 0, gap 24"));

		this.add(mainPanel, "w 100%, h 100%");
		mainPanel.add(leftPanel, "w 75%, grow");
		mainPanel.add(cardPanel, "w 25%, grow");

		leftPanel.add(new ItemManagerContainer(this.cardManager), "w 100%, h 95%, grow");
		leftPanel.add(buttonPanel, "h 5%");

		buttonPanel.setOpaque(false);
		buttonPanel.add(this.btnCopyToClipboard, "w pref+48px");
		buttonPanel.add(this.btnChangeSection, "w pref+48px");
		buttonPanel.add(this.btnClose, "w pref+48px");

		cardPanel.setOpaque(false);
		cardPanel.add(this.cardPicture, "grow");
		cardPanel.add(this.cardDetail, "grow");

		this.cardManager.setup(ItemManagerConfig.DECK_VIEWER);
		this.setDefaultFocus(this.cardManager.getCurrentView().getComponent());
	}

	private void changeSection() {
		int index = sections.indexOf(currentSection);
		index = (index + 1) % sections.size();
		currentSection = sections.get(index);
		this.cardManager.setPool(this.deck.get(currentSection));
		updateCaption();
	}

	private void updateCaption() {
		this.cardManager.setCaption(deck.getName() + " - " + currentSection.name());
	}

	private void copyToClipboard() {
		deck.copyToClipboard();
		FOptionPane.showMessageDialog("Deck list for '" + deck.getName() + "' copied to clipboard.");
	}
}
