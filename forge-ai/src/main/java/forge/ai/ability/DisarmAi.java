package forge.ai.ability;

import forge.ai.SpellAbilityAi;
import forge.game.Game;
import forge.game.GameLogEntryType;
import forge.game.card.Card;
import forge.game.card.CardCollectionView;
import forge.game.card.CardLists;
import forge.game.card.CardPredicates;
import forge.game.player.Player;
import forge.game.player.PlayerActionConfirmMode;
import forge.game.spellability.SpellAbility;
import forge.game.zone.ZoneType;
import forge.util.Aggregates;

public class DisarmAi extends SpellAbilityAi {

	/* (non-Javadoc)
	 * @see forge.card.abilityfactory.SpellAiLogic#canPlayAI(forge.game.player.Player, java.util.Map, forge.card.spellability.SpellAbility)
	 */
   @Override
   protected boolean checkAiLogic(final Player ai, final SpellAbility sa, final String aiLogic) {
   	/*
      System.out.println("");
      System.out.println(Aggregates.OPEN_OUTPUT);
  	 System.out.println("DisarmAi.checkAiLogic");
  	 System.out.println(String.format("sa: %s", sa));
    System.out.println(Aggregates.CLOSE_OUTPUT);
    //*/
  	 
  	 
		final Game game = ai.getGame();

		String codeLog = String.format("checkAiLogic");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
      return canPlayAI(ai, sa);   	
   }

	@Override
	protected boolean canPlayAI(Player ai, SpellAbility sa) {
		final Game game = ai.getGame();

		String codeLog = String.format("DisarmAi.canPlayAI");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
		//CardCollectionView choices = CardLists.filter(ai.getCardsIn(ZoneType.Battlefield), CardPredicates.Presets.FACE_DOWN);
		//return chooseCard(ai, choices, true) != null;
		return true;
	}   

	@Override
	public boolean chkAIDrawback(SpellAbility sa, Player ai) {
		final Game game = ai.getGame();

		String codeLog = String.format("DisarmAi.chkAIDrawback");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
      return canPlayAI(ai, sa);
	}

	/*
	@Override
	public boolean confirmAction(Player ai, SpellAbility sa, PlayerActionConfirmMode mode, String message) {
		final Game game = ai.getGame();

		String codeLog = String.format("confirmAction");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
		CardCollectionView choices = CardLists.filter(ai.getCardsIn(ZoneType.Battlefield), CardPredicates.Presets.FACE_DOWN);

		System.out.println(choices);
		
		return chooseCard(ai, choices, true) != null;
	}
	//*/
	@Override
	public boolean confirmAction(Player ai, SpellAbility sa, PlayerActionConfirmMode mode, String message) {
		final Game game = ai.getGame();

		String codeLog = String.format("DisarmAi.confirmAction");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
		CardCollectionView choices = CardLists.filter(sa.getHostCard().getController().getCardsIn(ZoneType.Battlefield), CardPredicates.Presets.FACE_DOWN);

		System.out.println(String.format("Disarm choices: %s", choices.toString()));
		
		return chooseCard(ai, choices, true) != null;
	}
	
	
	@Override
	public Card chooseSingleCard(final Player ai, SpellAbility sa, Iterable<Card> options, boolean isOptional, Player targetedPlayer) {
		final Game game = ai.getGame();

		String codeLog = String.format("DisarmAi.chooseSingleCard");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
		return chooseCard(ai, options, isOptional);
	}

	private Card chooseCard(final Player ai, Iterable<Card> list, boolean isOptional) {
		final Game game = ai.getGame();

		String codeLog = String.format("DisarmAi.chooseCard");
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);
		System.out.println(codeLog);
		
		Card choice = Aggregates.random(list);
		return choice;
	}
}
