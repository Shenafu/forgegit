package forge.ai.ability;

import com.google.common.collect.Iterables;
import forge.ai.*;
import forge.game.Game;
import forge.game.ability.AbilityUtils;
import forge.game.card.Card;
import forge.game.cost.Cost;
import forge.game.cost.CostRemoveCounter;
import forge.game.cost.CostSacrifice;
import forge.game.phase.PhaseHandler;
import forge.game.phase.PhaseType;
import forge.game.player.Player;
import forge.game.player.PlayerCollection;
import forge.game.player.PlayerPredicates;
import forge.game.spellability.AbilitySub;
import forge.game.spellability.SpellAbility;
import forge.util.MyRandom;

public class HonorGainAi extends SpellAbilityAi {

    /*
     * (non-Javadoc)
     * 
     * @see forge.ai.SpellAbilityAi#willPayCosts(forge.game.player.Player,
     * forge.game.spellability.SpellAbility, forge.game.cost.Cost,
     * forge.game.card.Card)
     */
    @Override
    protected boolean willPayCosts(Player ai, SpellAbility sa, Cost cost, Card source) {
        final Game game = source.getGame();
        final PhaseHandler ph = game.getPhaseHandler();

         // return super.willPayCosts(ai, sa, cost, source);
         if (!ComputerUtilCost.checkSacrificeCost(ai, cost, source, sa, false)) {
             return false;
         }
         
         if (!ComputerUtilCost.checkLifeCost(ai, cost, source, 4, sa)) {
             return false;
         }
         
         if (!ComputerUtilCost.checkHonorCost(ai, cost, source, sa)) {
             return false;
         }

         if (!ComputerUtilCost.checkDiscardCost(ai, cost, source)) {
             return false;
         }

         if (!ComputerUtilCost.checkRemoveCounterCost(cost, source)) {
             return false;
         }

        return true;
    }

    @Override
    protected boolean checkPhaseRestrictions(final Player ai, final SpellAbility sa, final PhaseHandler ph) {
        final Game game = ai.getGame();
        final int honor = ai.getHonor();
        boolean activateForCost = ComputerUtil.activateForCost(sa, ai);

        if (sa.isAbility()
                && sa.getHostCard() != null && sa.getHostCard().isCreature()
                && sa.getPayCosts() != null
                && (sa.getPayCosts().hasSpecificCostType(CostRemoveCounter.class) || sa.getPayCosts().hasSpecificCostType(CostSacrifice.class))) {
            if (!game.getStack().isEmpty()) {
                SpellAbility saTop = game.getStack().peekAbility();
                if (saTop.getTargets() != null && Iterables.contains(saTop.getTargets().getTargetPlayers(), ai)) {
                    return ComputerUtil.predictDamageFromSpell(saTop, ai) > 0;
                }
            }
            if (game.getCombat() == null) { return false; }
            if (!ph.is(PhaseType.COMBAT_DECLARE_BLOCKERS)) { return false; }
        }

        // Don't use honorgain before main 2 if possible
        // Xay: Why not?
        /*
        if (ph.getPhase().isBefore(PhaseType.MAIN2) && !sa.hasParam("ActivationPhases")
                && !ComputerUtil.castSpellInMain1(ai, sa)) {
            return false;
        }
        //*/

        if (!activateForCost
                && (!ph.getNextTurn().equals(ai) || ph.getPhase().isBefore(PhaseType.END_OF_TURN))
                && !sa.hasParam("PlayerTurn") && !SpellAbilityAi.isSorcerySpeed(sa)) {
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see forge.ai.SpellAbilityAi#checkApiLogic(forge.game.player.Player,
     * forge.game.spellability.SpellAbility)
     */
    @Override
    protected boolean checkApiLogic(Player ai, SpellAbility sa) {
        final Card source = sa.getHostCard();
        final String sourceName = ComputerUtilAbility.getAbilitySourceName(sa);

        final int myHonor = ai.getHonor();
        final String amountStr = sa.getParam("HonorAmount");
        int honorAmount = 0;
        boolean activateForCost = ComputerUtil.activateForCost(sa, ai);
        if (amountStr.equals("X") && source.getSVar(amountStr).equals("Count$xPaid")) {
            // Set PayX here to maximum value.
            final int xPay = ComputerUtilMana.determineLeftoverMana(sa, ai);
            source.setSVar("PayX", Integer.toString(xPay));
            honorAmount = xPay;
        } else {
            honorAmount = AbilityUtils.calculateAmount(sa.getHostCard(), amountStr, sa);
        }

        // Ugin AI: always use ultimate
        if (sourceName.equals("Ugin, the Spirit Dragon")) {
            // TODO: somehow link with DamageDealAi for cases where +1 = win
            return true;
        }

        // don't use it if no honor to gain
        if (!activateForCost && honorAmount <= 0) {
            return false;
        }
        // don't play if the conditions aren't met, unless it would trigger a
        // beneficial sub-condition
        if (!activateForCost && !sa.getConditions().areMet(sa)) {
            final AbilitySub abSub = sa.getSubAbility();
            if (abSub != null && !sa.isWrapper() && "True".equals(source.getSVar("AIPlayForSub"))) {
                if (!abSub.getConditions().areMet(abSub)) {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (!activateForCost && !ai.canGainHonor()) {
            return false;
        }

        // prevent run-away activations - first time will always return true
        if (ComputerUtil.preventRunAwayActivations(sa)) {
            return false;
        }

        if (sa.usesTargeting()) {
            if (!target(ai, sa, true)) {
                return false;
            }
        }

        if (ComputerUtil.playImmediately(ai, sa)) {
            return true;
        }

        if (SpellAbilityAi.isSorcerySpeed(sa)
                || sa.getSubAbility() != null || SpellAbilityAi.playReusable(ai, sa)) {
            return true;
        }
        
        // Save instant-speed honor-gain unless it is really worth it
        final float value = 0.9f * honorAmount / myHonor;
        if (value < 0.2f) {
            return false;
        }
        return MyRandom.getRandom().nextFloat() < value;
    }

    /**
     * <p>
     * gainHonorDoTriggerAINoCost.
     * </p>
     * @param sa
     *            a {@link forge.game.spellability.SpellAbility} object.
     * @param mandatory
     *            a boolean.
     *
     * @return a boolean.
     */
    @Override
    protected boolean doTriggerAINoCost(final Player ai, final SpellAbility sa,
    final boolean mandatory) {

        // If the Target is gaining honor, target self.
        // if the Target is modifying how much honor is gained, this needs to be
        // handled better
        if (sa.usesTargeting()) {
            if (!target(ai, sa, mandatory)) {
                return false;
            }
        }

        final Card source = sa.getHostCard();
        final String amountStr = sa.getParam("HonorAmount");
        if (amountStr.equals("X") && source.getSVar(amountStr).equals("Count$xPaid")) {
            // Set PayX here to maximum value.
            final int xPay = ComputerUtilMana.determineLeftoverMana(sa, ai);
            source.setSVar("PayX", Integer.toString(xPay));
        }

        return true;
    }
    
    @Override
    public boolean chkAIDrawback(SpellAbility sa, Player ai) {
    	return doTriggerAINoCost(ai, sa, true);
    }

    private boolean target(Player ai, SpellAbility sa, boolean mandatory) {
        Card source = sa.getHostCard();
        sa.resetTargets();
        // TODO : add add even more logic into it
        // try to target opponents first if that would kill them

        PlayerCollection opps = ai.getOpponents().filter(PlayerPredicates.isTargetableBy(sa));
        PlayerCollection allies = ai.getAllies().filter(PlayerPredicates.isTargetableBy(sa));

        if (sa.canTarget(ai) && ComputerUtil.honorgainPositive(ai, source)) {
            sa.getTargets().add(ai);
        } else {
            boolean hasTgt = false;
            // check for Honorgain negative on opponents
            for (Player opp : opps) {
                if (ComputerUtil.honorgainNegative(opp, source)) {
                    sa.getTargets().add(opp);
                    hasTgt = true;
                    break;
                }
            }
            if (!hasTgt) {
                // honorgain on ally
                for (Player ally : allies) {
                    if (ComputerUtil.honorgainPositive(ally, source)) {
                        sa.getTargets().add(ally);
                        hasTgt = true;
                        break;
                    }
                }
            }
            if (!hasTgt && mandatory) {
                // need to target something but its neither negative against
                // opponents,
                // nor posive against allies

                // hurting ally is probably better than healing opponent
                // look for Honorgain not Negative (case of honorgain negated)
                for (Player ally : allies) {
                    if (!ComputerUtil.honorgainNegative(ally, source)) {
                        sa.getTargets().add(ally);
                        hasTgt = true;
                        break;
                    }
                }
                if (!hasTgt) {
                    // same for opponent honorgain not positive
                    for (Player opp : opps) {
                        if (!ComputerUtil.honorgainPositive(opp, source)) {
                            sa.getTargets().add(opp);
                            hasTgt = true;
                            break;
                        }
                    }
                }

                // still no luck, try to target ally with most honor
                if (!allies.isEmpty()) {
                    Player ally = allies.max(PlayerPredicates.compareByHonor());
                    sa.getTargets().add(ally);
                    hasTgt = true;
                }
                // better heal opponent which most honor then the one with the
                // lowest
                if (!hasTgt) {
                    Player opp = opps.max(PlayerPredicates.compareByHonor());
                    sa.getTargets().add(opp);
                    hasTgt = true;
                }
            }
            if (!hasTgt) {
                return false;
            }
        }
        return true;
    }
}
