package forge.ai;

import com.google.common.base.Function;

import forge.game.ability.AbilityUtils;
import forge.game.ability.ApiType;
import forge.game.card.Card;
import forge.game.card.CounterType;
import forge.game.cost.CostPayEnergy;
import forge.game.keyword.Keyword;
import forge.game.keyword.KeywordInterface;
import forge.game.spellability.SpellAbility;

public class CreatureEvaluator implements Function<Card, Integer> {
	protected int getEffectivePower(final Card c) {
		return c.getNetCombatDamage();
	}
	protected int getEffectiveToughness(final Card c) {
		return c.getNetToughness();
	}

	@Override
	public Integer apply(Card c) {
		return evaluateCreature(c);
	}

	public int evaluateCreature(final Card c) {
		return evaluateCreature(c, true, true);
	}

	public int evaluateCreature(final Card c, final boolean considerPT, final boolean considerCMC) {
		int value = 80;
		if (!c.isToken()) {
			value += addValue(20, "non-token"); // tokens should be worth less than actual cards
		}
		int power = getEffectivePower(c);
		final int toughness = getEffectiveToughness(c);
		for (KeywordInterface kw : c.getKeywords()) {
			String keyword = kw.getOriginal();
			if (keyword.equals("Prevent all combat damage that would be dealt by CARDNAME.")
			|| keyword.equals("Prevent all damage that would be dealt by CARDNAME.")
			|| keyword.equals("Prevent all combat damage that would be dealt to and dealt by CARDNAME.")
			|| keyword.equals("Prevent all damage that would be dealt to and dealt by CARDNAME.")) {
				power = 0;
				break;
			}
		}
		if (considerPT) {
			value += addValue(power * 15, "power");
			value += addValue(toughness * 10, "toughness: " + toughness);
		}
		if (considerCMC) {
			value += addValue(c.getCMC() * 5, "cmc");
		}

		// Evasion keywords
		if (c.hasKeyword("Unblockable") || c.hasKeyword(Keyword.ELUDE)) {
			value += addValue(power * 20, "unblockable");
		} else {
			value += c.hasKeyword(Keyword.FLYING) ? addValue(power * 10, "flying") : 0;
			value += c.hasKeyword(Keyword.HORSEMANSHIP) ? addValue(power * 10, "horses") : 0;
			value += c.hasKeyword(Keyword.ANIMOSITY) || c.hasKeyword("You may have CARDNAME assign its combat damage as though it weren't blocked.") ? addValue(power * 20, "animosity") : 0;
			value += c.hasKeyword(Keyword.FEAR) ? addValue(power * 6, "fear") : 0;
			value += c.hasKeyword(Keyword.INTIMIDATE) ? addValue(power * 6, "intimidate") : 0;
			value += c.hasKeyword(Keyword.SUBSURFACE) ? addValue(power * 4, "subsurface") : 0;
			value += c.hasKeyword(Keyword.MENACE) ? addValue(power * 4, "menace") : 0;
			value += c.hasKeyword(Keyword.REARGUARD) ? addValue(power * 6, "rearguard") : 0;
			value += c.hasKeyword("CantBeBlockedBy") ? addValue(power * 3, "block-restrict") : 0;
		}

		// Other good keywords
		if (power > 0) {
			if (c.hasKeyword(Keyword.DOUBLE_STRIKE)) {
				value += addValue(10 + (power * 15), "ds");
			} else if (c.hasKeyword(Keyword.FIRST_STRIKE)) {
				value += addValue(10 + (power * 5), "fs");
			}

			value += c.hasKeyword(Keyword.DEATHTOUCH) ? addValue(15, "dt") : 0;
			value += c.hasKeyword(Keyword.FROSTBITE) ? addValue(15, "frostbite") : 0;
			value += c.hasKeyword(Keyword.LIFELINK) ? addValue(power * 10, "lifelink") : 0;
			value += c.hasKeyword(Keyword.TRAMPLE) ? addValue((power - 1) * 5, "trample") : 0;
			value += c.hasKeyword(Keyword.VIGILANCE) ? addValue((power * 5) + (toughness * 5), "vigilance") : 0;
			value += c.hasKeyword(Keyword.WITHER) ? addValue(power * 10, "wither") : 0;
			value += c.hasKeyword(Keyword.INFECT) ? addValue(power * 15, "infect") : 0;
			value += c.hasKeyword(Keyword.VIRULENT) ? addValue(power * 10, "virulent") : 0;

			value += c.hasKeyword(Keyword.RAMPAGE) ? addValue(c.getKeywordMagnitude(Keyword.RAMPAGE), "rampage") : 0;
			value += c.hasKeyword(Keyword.AFFLICT) ? addValue(c.getKeywordMagnitude(Keyword.AFFLICT) * 5, "afflict") : 0;

			value += c.hasKeyword(Keyword.LEARNING) ? addValue(c.getAmountOfKeyword(Keyword.LEARNING) * 20, "learning") : 0;
			value += c.hasKeyword(Keyword.THRIVE) ? addValue(c.getAmountOfKeyword(Keyword.THRIVE) * 20, "thrive") : 0;
			value += c.hasKeyword(Keyword.RETALIATE) ? addValue(c.getAmountOfKeyword(Keyword.RETALIATE) * power * 10, "retaliate") : 0;

		}

		value += c.hasKeyword(Keyword.ANNIHILATOR) ? addValue(c.getKeywordMagnitude(Keyword.ANNIHILATOR) * 50, "eldrazi") : 0;
		value += c.hasKeyword(Keyword.BUSHIDO) ? addValue(c.getKeywordMagnitude(Keyword.BUSHIDO) * 16, "bushido") : 0;
		value += c.hasKeyword(Keyword.FLANKING) ? addValue(c.getKeywordMagnitude(Keyword.FLANKING) * 15, "flanking") : 0;

		value += c.hasKeyword(Keyword.EXALTED) ? addValue(c.getAmountOfKeyword(Keyword.EXALTED) * 15, "exalted") : 0;
		value += c.hasKeyword(Keyword.INFORM) ? addValue(c.getAmountOfKeyword(Keyword.INFORM) * 20, "inform") : 0;

		// Keywords that may produce temporary or permanent buffs over time
		value += c.hasKeyword(Keyword.PROWESS) ? addValue(5, "prowess") : 0;
		value += c.hasKeyword(Keyword.OUTLAST) ? addValue(10, "outlast") : 0;
		value += c.hasKeyword(Keyword.MIMICRY) ? addValue(5, "mimicry") : 0;
		value += c.hasKeyword(Keyword.FAME) ? addValue(10, "fame") : 0;
		value += c.hasKeyword(Keyword.DEFAME) ? addValue(20, "defame") : 0;
		value += c.hasKeyword(Keyword.TREACHEROUS) ? addValue(25, "treacherous") : 0;
		value += c.hasKeyword(Keyword.NOTORIOUS) ? addValue(50, "notorious") : 0;

		// Defensive Keywords

		value += c.hasKeyword(Keyword.REACH) && !c.hasKeyword(Keyword.FLYING) ? addValue(5, "reach") : 0;
		value += c.hasKeyword("CARDNAME can block creatures with shadow as though they didn't have shadow.") ? addValue(3, "shadow-block") : 0;

		// Protection
		value += c.hasKeyword(Keyword.INDESTRUCTIBLE) ? addValue(70, "darksteel") : 0;
		value += c.hasKeyword(Keyword.PROTECTION) ? addValue(20, "protection") : 0;
		value += c.hasKeyword(Keyword.ABSORB) ? addValue(c.getKeywordMagnitude(Keyword.ABSORB) * 11, "absorb") : 0;
		if (c.hasKeyword("Prevent all damage that would be dealt to CARDNAME.")) {
			value += addValue(60, "cho-manno");
		} else if (c.hasKeyword("Prevent all combat damage that would be dealt to CARDNAME.")) {
			value += addValue(50, "fogbank");
		}
		if (c.hasKeyword(Keyword.HEXPROOF)) {
			value += addValue(35, "hexproof");
		} else if (c.hasKeyword(Keyword.SHROUD)) {
			value += addValue(30, "shroud");
		}
		value += c.hasKeyword(Keyword.RETREAT) ? addValue(15, "retreat") : 0;
		value += c.hasKeyword(Keyword.GUARDIAN) ? addValue(c.getKeywordMagnitude(Keyword.GUARDIAN) * 5, "guardian") : 0;

		// Bad keywords
		if (c.hasKeyword(Keyword.DEFENDER) || c.hasKeyword("CARDNAME can't attack.")) {
			value -= subValue((power * 9) + 40, "cant-attack");
		} else if (c.getSVar("SacrificeEndCombat").equals("True")) {
			value -= subValue(40, "sac-end");
		}
		if (c.hasKeyword(Keyword.DEFENSELESS)) {
			value -= subValue(10, "cant-block");
		}
		else if (c.hasKeyword("CARDNAME can block only creatures with flying.")) {
			value -= subValue(toughness * 5, "reverse-reach");
		}
		if (c.hasKeyword(Keyword.INTERCEPT)) {
			value -= subValue(10, "must-block");
		}
		if (c.hasKeyword(Keyword.BERSERK)) {
			value -= subValue(10, "must-attack");
		}
		if (c.hasStartOfKeyword("CARDNAME attacks specific player each combat if able")) {
			value -= subValue(10, "must-attack-player");
		}

		value -= c.hasSVar("DestroyWhenDamaged") ? subValue((toughness - 1) * 9, "dies-to-dmg") : 0;

		// reset everything - useless
		value = c.hasKeyword("CARDNAME can't attack or block.") ? addValue(50 + (c.getCMC() * 5), "useless") : 0;

		if (c.isFrozen() || c.hasKeyword("CARDNAME doesn't untap during your untap step.")) {
			if (c.isTapped()) {
				value = addValue(50 + (c.getCMC() * 5), "tapped-useless"); // reset everything - useless
			} else {
				value -= subValue(50, "doesnt-untap");
			}
		}
		if (c.hasSVar("EndOfTurnLeavePlay")) {
			value -= subValue(50, "eot-leaves");
		} else if (c.hasStartOfKeyword("Cumulative upkeep")) {
			value -= subValue(30, "cupkeep");
		} else if (c.hasStartOfKeyword("UpkeepCost")) {
			value -= subValue(20, "sac-unless");
		} else if (c.hasKeyword(Keyword.ECHO) && c.cameUnderControlSinceLastUpkeep()) {
			value -= subValue(10, "echo-unpaid");
		}

		value -= c.hasStartOfKeyword("At the beginning of your upkeep, CARDNAME deals") ? subValue(20, "upkeep-dmg") : 0;

		value -= c.hasKeyword(Keyword.FADING) ? addValue(20, "fading") : 0;
		value -= c.hasKeyword(Keyword.VANISHING) ? addValue(20, "vanishing") : 0;
		value -= c.hasKeyword(Keyword.DEMISE) ? addValue(30, "demise") : 0;
		value -= c.hasKeyword(Keyword.HEXDOOM) ? addValue(25, "hexdoom") : 0;
		value -= c.getSVar("Targeting").equals("Dies") ? addValue(25, "dies") : 0;

		for (final SpellAbility sa : c.getSpellAbilities()) {
			if (sa.isAbility()) {
				value += addValue(evaluateSpellAbility(sa), "sa: " + sa);
			}
		}

		value += !c.getManaAbilities().isEmpty() ? addValue(10, "manadork") : 0;
		value += c.isUntapped() ? addValue(1, "untapped") : 0;
		value += c.isUnlagged() ? addValue(1, "unlagged") : 0;
		value += !c.getEncodedCards().isEmpty() ? addValue(24, "encoded") : 0;

		// paired creatures are more valuable because they grant a bonus to the other creature
		value += c.isPaired() ? addValue(14, "paired") : 0;

		// card-specific evaluation modifier
		if (c.hasSVar("AIEvaluationModifier")) {
			int mod = AbilityUtils.calculateAmount(c, c.getSVar("AIEvaluationModifier"), null);
			value += mod;
		}

		return value;
	}

	private int evaluateSpellAbility(SpellAbility sa) {
		// Pump abilities
		if (sa.getApi() == ApiType.Pump) {
			// Pump abilities that grant +X/+X to the card
			if ("+X".equals(sa.getParam("NumAtt"))
			&& "+X".equals(sa.getParam("NumDef"))
			&& !sa.usesTargeting()
			&& (!sa.hasParam("Defined") || "Self".equals(sa.getParam("Defined")))) {
				if (sa.getPayCosts() != null && sa.getPayCosts().hasOnlySpecificCostType(CostPayEnergy.class)) {
					// Electrostatic Pummeler, can be expanded for similar cards
					int initPower = getEffectivePower(sa.getHostCard());
					int pumpedPower = initPower;
					int energy = sa.getHostCard().getController().getCounters(CounterType.ENERGY);
					if (energy > 0) {
						int numActivations = energy / 3;
						for (int i = 0; i < numActivations; i++) {
							pumpedPower *= 2;
						}
						return (pumpedPower - initPower) * 15;
					}
				}
			}
		}

		// default value
		return 10;
	}

	protected int addValue(int value, String text) {
		return value;
	}
	protected int subValue(int value, String text) {
		return -addValue(-value, text);
	}
}
