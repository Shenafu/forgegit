package forge.card;

import com.google.common.collect.Sets;

import forge.card.CardType.CoreType;
import forge.card.mana.ManaCostShard;
import forge.game.GameView;
import forge.game.card.Card;
import forge.game.card.CardView;
import forge.game.card.CardView.CardStateView;
import forge.game.card.CounterType;
import forge.item.InventoryItemFromSet;
import forge.item.PaperCard;
import forge.item.PreconDeck;
import forge.item.SealedProduct;
import forge.model.FModel;
import forge.properties.ForgeConstants;
import forge.properties.ForgePreferences;
import forge.util.Lang;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CardDetailUtil {

	private CardDetailUtil() {
	}

	public enum DetailColors {
		WHITE(254, 253, 244),
		BLUE(90, 146, 202),
		BLACK(32, 34, 31),
		RED(253, 66, 40),
		GREEN(22, 115, 69),
		MULTICOLOR(248, 219, 85),
		COLORLESS(160, 166, 164),
		LAND(190, 153, 112),
		FACE_DOWN(83, 61, 40),
		COMMON(10, 7, 10),
		UNCOMMON(160, 172, 174),
		RARE(193, 170, 100),
		MYTHIC(171, 54, 39),
		SPECIAL(141, 114, 147),
		UNKNOWN(200, 0, 230);

		public final int r, g, b;

		private DetailColors(final int r0, final int g0, final int b0) {
			r = r0;
			g = g0;
			b = b0;
		}
	}

	public static DetailColors getBorderColor(final CardStateView card, final boolean canShow) {
		if (card == null) {
			return getBorderColors(null, false, false, false).iterator().next();
		}
		return getBorderColors(card.getColors(), card.isLand(), canShow, false).iterator().next();
	}
	public static List<DetailColors> getBorderColors(final CardStateView card, final boolean canShow) {
		if (card == null) {
			return getBorderColors(null, false, false, true);
		}
		return getBorderColors(card.getColors(), card.isLand(), canShow, true);
	}
	public static List<DetailColors> getBorderColors(final ColorSet colorSet) {
		return getBorderColors(colorSet, false, true, true);
	}
	private static List<DetailColors> getBorderColors(final ColorSet cardColors, final boolean isLand, final boolean canShow, final boolean supportMultiple) {
		final List<DetailColors> borderColors = new ArrayList<DetailColors>();

		if (cardColors == null || !canShow) {
			borderColors.add(DetailColors.FACE_DOWN);
		}
		else if (cardColors.isColorless()) {
			if (isLand) { //use different color for lands vs. other colorless cards
				borderColors.add(DetailColors.LAND);
			}
			else {
				borderColors.add(DetailColors.COLORLESS);
			}
		}
		else {
			final int colorCount = cardColors.countColors();
			if (colorCount > 3 || (colorCount > 1 && !supportMultiple)) {
				borderColors.add(DetailColors.MULTICOLOR);
			}
			else { //for 3 colors or fewer, return all colors in shard order
				for (ManaCostShard shard : cardColors.getOrderedShards()) {
					switch (shard.getColorMask()) {
						case MagicColor.WHITE:
							borderColors.add(DetailColors.WHITE);
							break;
						case MagicColor.BLUE:
							borderColors.add(DetailColors.BLUE);
							break;
						case MagicColor.BLACK:
							borderColors.add(DetailColors.BLACK);
							break;
						case MagicColor.RED:
							borderColors.add(DetailColors.RED);
							break;
						case MagicColor.GREEN:
							borderColors.add(DetailColors.GREEN);
							break;
					}
				}
			}
		}

		if (borderColors.isEmpty()) { // If your card has a violet border, something is wrong
			borderColors.add(DetailColors.UNKNOWN);
		}
		return borderColors;
	}

	public static String getCurrentColors(final CardStateView c) {
		ColorSet curColors = c.getColors();
		String strCurColors = ""; 

		if (curColors.hasWhite()) { strCurColors += "{W}"; }
		if (curColors.hasBlue())  { strCurColors += "{U}"; }
		if (curColors.hasBlack()) { strCurColors += "{B}"; }
		if (curColors.hasRed())   { strCurColors += "{R}"; }
		if (curColors.hasGreen()) { strCurColors += "{G}"; }

		if (strCurColors.isEmpty()) {
			strCurColors = "{C}";
		}

		return strCurColors;
	}

	public static DetailColors getRarityColor(final CardRarity rarity) {
		switch (rarity) {
			case Uncommon:
				return DetailColors.UNCOMMON;
			case Rare:
				return DetailColors.RARE;
			case MythicRare:
				return DetailColors.MYTHIC;
			case Special: //"Timeshifted" or other Special Rarity Cards
			return DetailColors.SPECIAL;
			default: //case BasicLand: + case Common:
				return DetailColors.COMMON;
		}
	}

	public static String getItemDescription(final InventoryItemFromSet item) {
		if (item instanceof SealedProduct) {
			return ((SealedProduct)item).getDescription();
		}
		if (item instanceof PreconDeck) {
			return ((PreconDeck) item).getDescription();
		}
		return item.getName();
	}

	public static String formatCardName(final CardView card, final boolean canShow, final boolean forAltState) {
		final String name = forAltState ? card.getAlternateState().getName() : card.getName();
		return StringUtils.isEmpty(name) || !canShow ? "???" : name.trim();
	}

	public static String formatCardType(final CardStateView card, final boolean canShow) {
		StringBuilder type = new StringBuilder();
		if (!canShow) {
			return (card.getState() == CardStateName.FaceDown ? "Creature" : "---");   		 
		}

		type.append(card.getType().toString());

		if (card.getCard().hasAlternateState() && card.getCard().isSplitCard() ){
			type.append(" // ");
			type.append(card.getCard().getAlternateState().getType());
		}

		return type.toString();
	}

	public static String formatPowerToughness(final CardStateView card, final boolean canShow) {
		if (!canShow && card.getState() != CardStateName.FaceDown) {
			return "";
		}
		final StringBuilder ptText = new StringBuilder();
		boolean vehicle = card.getType().hasSubtype("Vehicle");
		if (vehicle && !card.isCreature()) {
			ptText.append("{");
		}

		if (card.isCreature() || vehicle) {
			ptText.append(card.getPower()).append(" / ").append(card.getToughness());
		}

		if (vehicle && !card.isCreature()) {
			ptText.append("}");
		}

		if (card.isPlaneswalker()) {
			if (ptText.length() > 0) {
				ptText.insert(0, "P/T: ");
				ptText.append(" - ").append("Loy: ");
			}
			else {
				ptText.append("Loyalty: ");
			}

			ptText.append(card.getLoyalty());
		}

		if (card.isScene()) {
			if (ptText.length() > 0) {
				ptText.insert(0, "P/T: ");
				ptText.append(" - ").append("Fate: ");
			}
			else {
				ptText.append("Fate: ");
			}

			ptText.append(card.getFate());
		}
		return ptText.toString();
	}

	public static String formatCardId(final CardStateView card) {
		final String id = card.getDisplayId();
		return id.isEmpty() ? id : "[" + id + "]";
	}

	public static String formatCurrentCardColors(final CardStateView state) {
		final String showCurColorMode = FModel.getPreferences().getPref(ForgePreferences.FPref.UI_DISPLAY_CURRENT_COLORS);
		boolean showCurColors = false;
		String curColors = "";

		// do not show current colors for temp effect cards, emblems and the like
		if (state.getType().isEmblem() || state.getType().hasSubtype("Effect")) {
			return "";
		}

		if (!showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_NEVER)) {
			final CardView card = state.getCard();
			boolean isMulticolor = state.getColors().isMulticolor();
			boolean isChanged = false;
			curColors = getCurrentColors(state);

			// do not waste CPU cycles on this if the mode does not involve checking for changed colors
			if (showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_CHANGED) || showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_MULTI_OR_CHANGED)) {
				String origIdent = "";
				PaperCard origPaperCard = null;
				Card origCard = null;
				try {
					if (!card.getName().isEmpty()) {
						origPaperCard = FModel.getMagicDb().getCommonCards().getCard(card.getName());
					} else {
						// probably a morph or manifest, try to get its identity from the alternate state
						String altName = card.getAlternateState().getName();
						if (!altName.isEmpty()) {
							origPaperCard = FModel.getMagicDb().getCommonCards().getCard(card.getAlternateState().getName());
						}
					}
					if (origPaperCard != null) {
						origCard = Card.getCardForUi(origPaperCard); // if null, probably a variant card
					}
					origIdent = origCard != null ? getCurrentColors(origCard.isFaceDown() ? CardView.get(origCard).getState(false) : CardView.get(origCard).getCurrentState()) : "";
				} catch(Exception ex) {
					System.err.println("Unexpected behavior: card " + card.getName() + "[" + card.getId() + "] tripped an exception when trying to process current card colors.");
				} 
				isChanged = !curColors.equals(origIdent);
			}

			if ((showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_MULTICOLOR) && isMulticolor) ||
			(showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_CHANGED) && isChanged) ||
			(showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_MULTI_OR_CHANGED) && (isChanged || isMulticolor)) ||
			(showCurColorMode.equals(ForgeConstants.DISP_CURRENT_COLORS_ALWAYS))) {
				showCurColors = true;
			}
		}

		return showCurColors ? curColors : "";
	}

	public static String composeCardText(final CardStateView state, final GameView gameView, final boolean canShow) {
		if (!canShow) { return ""; }

		final CardView card = state.getCard();
		final StringBuilder area = new StringBuilder();

		// Token
		if (card.isToken()) {
			if(card.getCurrentState().getType().hasSubtype("Effect"))
				area.append("Effect");
			else if(card.getCurrentState().getType().isEmblem())
				area.append("Emblem");
			else
				area.append("Token");
			area.append("<hr>");
		}

		// exerted
		if (card.isExertedThisTurn()) {
			area.append("^Exerted^");
			area.append("<hr>");
		}

		// loyalty ability
		if (card.isLoyaltyTurnActivated()) {
			area.append("^Loyalty Ability Activated This Turn^");
			area.append("<hr>");
		}

		// fate ability
		if (card.isFateTurnActivated()) {
			area.append("^Fate Ability Activated This Turn^");
			area.append("<hr>");
		}
		
		String text = card.getText(state);

		// LEVEL [0-9]+-[0-9]+
		// LEVEL [0-9]+\+
		String regex = "LEVEL [0-9]+-[0-9]+ ";
		text = text.replaceAll(regex, "$0\r\n");

		regex = "LEVEL [0-9]+\\+ ";
		text = text.replaceAll(regex, "\r\n$0\r\n");

		// displays keywords that have dots in them a little better:
		regex = "\\., ";
		text = text.replaceAll(regex, ".\r\n");

		area.append(text);
		if (!text.isEmpty() && !text.endsWith("<hr>")) {
			area.append("<hr>");
		}

		if (card.isPhasedOut()) {
			area.append("Phased Out");
			area.append("<hr>");
		}

		// text changes
		final Map<String, String> changedColorWords = card.getChangedColorWords();
		final Map<String, String> changedTypes = card.getChangedTypes();
		if (changedColorWords != null && changedTypes != null) {
			if (!(changedColorWords.isEmpty() && changedTypes.isEmpty())) {
				/*
				if (area.length() != 0) {
					area.append("\n");
				}
				//*/
			}

			for (final Entry<String, String> e : Sets.union(changedColorWords.entrySet(), changedTypes.entrySet())) {
				// ignore lower case and plural form keys, to avoid duplicity
				if (Character.isUpperCase(e.getKey().charAt(0))
				&& !CardType.Constant.singularTypes.containsKey(e.getKey())) {
					area.append("Text changed: all instances of ");
					if (e.getKey().equals("Any")) {
						if (changedColorWords.containsKey(e.getKey())) {
							area.append("color words");
						} else if (forge.card.CardType.getBasicTypes().contains(e.getValue())) {
							area.append("basic land types");
						} else {
							area.append("creature types");
						}
					} else {
						area.append(e.getKey());
					}
					area.append(String.format(" are replaced by %s.", e.getValue()));
					area.append("<hr>");
				}
			}
		}

		// counter text
		if (card.getCounters() != null) {
			for (final Entry<CounterType, Integer> c : card.getCounters().entrySet()) {
				if (c.getValue().intValue() != 0) {
					area.append(c.getKey().getName() + " counters: ");
					area.append(c.getValue());
					area.append("<hr>");
				}
			}
		}

		if (state.isCreature()) {
			final int damage = card.getDamage();
			if (damage > 0) {
				area.append("Damage: " + damage);
				area.append("<hr>");
			}
		}
		if (state.isCreature() || state.isPlaneswalker() || state.isScene()) {
			final int assigned = card.getAssignedDamage();
			if (assigned > 0) {
				area.append("Assigned Damage: " + assigned);
				area.append("<hr>");
			}
		}

		// Regeneration Shields
		final int regenShields = card.getShieldCount();
		if (regenShields > 0) {
			area.append("Regeneration Shields: ").append(regenShields);
			area.append("<hr>");
		}

		// Damage Prevention
		final int preventNextDamage = card.getPreventNextDamage();
		if (preventNextDamage > 0) {
			area.append("Prevent the next ").append(preventNextDamage).append(" damage that would be dealt to ");
			area.append(state.getName()).append(" this turn.");
			area.append("<hr>");
		}

		// chosen type
		if (!card.getChosenType().isEmpty()) {
			area.append("(chosen type: ");
			area.append(card.getChosenType());
			area.append(")");
			area.append("<hr>");
		}

		// chosen color
		if (card.getChosenColors() != null) {
			area.append("(chosen colors: ");
			area.append(Lang.joinHomogenous(card.getChosenColors()));
			area.append(")");
			area.append("<hr>");
		}

		// chosen cards
		if (card.getChosenCards() != null) {
			area.append("(chosen cards: ");
			area.append(Lang.joinHomogenous(card.getChosenCards()));
			area.append(")");
			area.append("<hr>");
		}

		// chosen player
		if (card.getChosenPlayer() != null) {
			area.append("(chosen player: " + card.getChosenPlayer() + ")");
			area.append("<hr>");
		}

		// chosen mode
		if (!card.getChosenMode().isEmpty()) {
			area.append("(chosen mode: " + card.getChosenMode() + ")");
			area.append("<hr>");
		}

		// named card
		if (!card.getNamedCard().isEmpty()) {
			area.append("(named card: ");
			if (card.isFaceDown() && state.getState() == CardStateName.FaceDown) {
				area.append("Hidden");
			} else {
				area.append(card.getNamedCard());
			}
			area.append(")");
			area.append("<hr>");
		}

        // a card has something attached to it
        if (card.hasCardAttachments()) {
            area.append("@ Attachments: ");
            area.append(StringUtils.join(card.getAttachedCards(), ", "));
			area.append(" @");
			area.append("<hr>");
		}

        // a card is attached to something
        if (card.getAttachedTo() != null) {
        	area.append("* Attached to ");
        	area.append(card.getAttachedTo());
			area.append(" *");
			area.append("<hr>");
		}
        
        if (card.getEnchantedPlayer() != null) {
            area.append("* Enchanting player: ");
            area.append(card.getEnchantedPlayer());
			area.append(" *");
			area.append("<hr>");
        }

		// controlling
		if (card.getGainControlTargets() != null) {
			area.append("+ Controlling: ");
			area.append(StringUtils.join(card.getGainControlTargets(), ", "));
			area.append(" +");
			area.append("<hr>");
		}

		// cloned via
		if (card.getCloneOrigin() != null) {
			area.append("^ Cloned via: ");
			area.append(card.getCloneOrigin().getCurrentState().getName());
			area.append(" ^");
			area.append("<hr>");
		}

		// Imprint
		if (card.getImprintedCards() != null) {
			area.append("Imprinting: ");
			area.append(StringUtils.join(card.getImprintedCards(), ", "));
			area.append("<hr>");
		}

		// Teach
		if (card.getTaughtCards() != null) {
			area.append("Taught: ");
			area.append(StringUtils.join(card.getTaughtCards(), ", "));
			area.append("<hr>");
		}

		// Haunt
		if (card.getHauntedBy() != null) {
			area.append("Haunted by: ");
			area.append(StringUtils.join(card.getHauntedBy(), ", "));
			area.append("<hr>");
		}
		if (card.getHaunting() != null) {
			area.append("Haunting " + card.getHaunting());
			area.append("<hr>");
		}

		// Cipher
		if (card.getEncodedCards() != null) {
			area.append("Encoded: " + card.getEncodedCards());
			area.append("<hr>");
		}

		// must block
		if (card.getMustBlockCards() != null) {
			final String mustBlockThese = Lang.joinHomogenous(card.getMustBlockCards());
			area.append("Must block " + mustBlockThese);
			area.append("<hr>");
		}

		//show current card colors if enabled
		String curCardColors = formatCurrentCardColors(state);
		if (!curCardColors.isEmpty()) {
			area.append("Current Card Colors: ");
			area.append(curCardColors);
			area.append("<hr>");
		}

		//show current storm count for storm cards
		if (state.hasStorm()) {
			if (gameView != null) {
				area.append("Current Storm Count: " + gameView.getStormCount());
				area.append("<hr>");
			}
		}
		return area.toString();
	}
}
