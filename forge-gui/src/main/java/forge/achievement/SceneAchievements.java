package forge.achievement;

import forge.GuiBase;
import forge.assets.FSkinProp;
import forge.assets.ISkinImage;
import forge.game.Game;
import forge.game.player.Player;
import forge.item.IPaperCard;
import forge.model.FModel;
import forge.properties.ForgeConstants;

public class SceneAchievements extends AchievementCollection {
    public static final SceneAchievements instance = new SceneAchievements();

    public static ISkinImage getTrophyImage(String sceneName) {
        return GuiBase.getInterface().createLayeredImage(FSkinProp.IMG_SPECIAL_TROPHY, ForgeConstants.CACHE_ACHIEVEMENTS_DIR + "/" + sceneName + ".png", 1);
    }

    private SceneAchievements() {
        super("Scene Climaxes", ForgeConstants.ACHIEVEMENTS_DIR + "scenes.xml", false, ForgeConstants.SCENE_ACHIEVEMENT_LIST_FILE);
    }

    @Override
    protected void addSharedAchivements() {
        //prevent including shared achievements
    }

    protected void add(String cardName0, String displayName0, String flavorText0) {
        add(new SceneClimax(cardName0, displayName0, flavorText0));
    }

    @Override
    public void updateAll(Player player) {
        //only call update achievements for any climaxes activated during the game
        if (player.getOutcome().hasWon()) {
            boolean needSave = false;
            for (String climax : player.getAchievementTracker().activatedClimaxes) {
                Achievement achievement = achievements.get(climax);
                if (achievement != null) {
                    achievement.update(player);
                    needSave = true;
                }
            }
            if (needSave) {
                save();
            }
        }
    }

    private class SceneClimax extends ProgressiveAchievement {
        private SceneClimax(String cardName0, String displayName0, String flavorText0) {
            super(cardName0, displayName0, "Win a game after activating " + cardName0 + "'s climax", flavorText0);
        }

        @Override
        protected boolean eval(Player player, Game game) {
            return true; //if this reaches this point, it can be presumed that alternate win condition achieved
        }

        @Override
        public IPaperCard getPaperCard() {
            return FModel.getMagicDb().getCommonCards().getCard(getKey());
        }

        @Override
        protected String getNoun() {
            return "Win";
        }
    }
}
