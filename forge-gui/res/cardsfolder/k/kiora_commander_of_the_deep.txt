Name:Kiora, Commander of the Deep
ManaCost:3 G U
Types:Legendary Planeswalker Kiora
Loyalty:4

A:AB$ Effect | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | ValidTgts$ You | Name$ Kiora, Commander of the Deep Effect | Triggers$ TrigPlay | SVars$ EffDraw | SpellDescription$ Until end of turn, whenever a land enters the field under your control, draw a card.

SVar:TrigPlay:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Land.YouCtrl | Execute$ EffDraw | TriggerZones$ Command | SpellDescription$ Until end of turn, whenever a land enters the field under your control, draw a card.

SVar:EffDraw:DB$ Draw | NumCards$ 1

A:AB$ ChangeZone | Cost$ SubCounter<2/LOYALTY> | Planeswalker$ True | Origin$ Hand | Destination$ Battlefield | ChangeType$ Creature,Land | ChangeNum$ 1 | SpellDescription$ Put a creature or land card from your hand onto the field.

A:AB$ Effect | Cost$ SubCounter<6/LOYALTY> | Planeswalker$ True | Ultimate$ True | Name$ Emblem - Kiora, Commander of the Deep | StaticAbilities$ PTSet | Stackable$ False | Duration$ Permanent | AILogic$ Always | SpellDescription$ You get an emblem with "Creatures you control have base power and toughness 9/9."

SVar:PTSet:Mode$ Continuous | EffectZone$ Command | Affected$ Creature.YouCtrl | AffectedZone$ Battlefield | SetPower$ 9 | SetToughness$ 9 | Description$ Creatures you control have base power and toughness 9/9.

Oracle:[+1]: Until end of turn, whenever a land enters the field under your control, draw a card.\n[-2]: Put a creature or land card from your hand onto the field.\n[-6]: You get an emblem with "Creatures you control have base power and toughness 9/9."