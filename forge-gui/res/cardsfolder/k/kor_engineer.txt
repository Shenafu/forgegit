Name:Kor Engineer
ManaCost:W W 1
Types:Creature Kor Artificer
PT:2/2

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigSearch | TriggerDescription$ When CARDNAME enters, search your library for an artifact card, reveal it, shuffle your library, then put that card on top of it.

SVar:TrigSearch:DB$ ChangeZone | Origin$ Library | Destination$ Library | LibraryPosition$ 0 | ChangeType$ Artifact | ChangeNum$ 1 | ShuffleNonMandatory$ True

S:Mode$ ReduceCost | ValidCard$ Equipment,Fortification | Type$ Ability | Amount$ 1 | MinMana$ 1 | AffectedZone$ Battlefield | Description$ Activated abilities of Equipments and Fortifications you control cost {1} less to activate. This effect can't reduce the amount of mana an ability costs to activate to less than one mana.

Oracle:When Kor Engineer enters, you may search your library for an artifact card, reveal it, shuffle your library, then put that card on top of it.\nActivated abilities of Equipments and Fortifications you control cost {1} less to activate. This effect can't reduce the amount of mana an ability costs to activate to less than one mana.
