Name:Kaalia, Zenith Seeker
ManaCost:R W B
Types:Legendary Creature Human Cleric
PT:3/3
K:Flying
K:Vigilance

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigDigMulti | TriggerDescription$ When CARDNAME enters, dig six cards for an Angel card, a Demon card, and/or a Dragon card and put them into your hand. Tuck the rest in a random order.

SVar:TrigDigMulti:DB$ DigMultiple | DigNum$ 6 | ChangeValid$ Card.Angel,Card.Demon,Card.Dragon | SourceZone$ Library | DestinationZone$ Hand | DestinationZone2$ Library | LibraryPosition$ -1 | RestRandomOrder$ True

Oracle:Flying, vigilance\nWhen Kaalia, Zenith Seeker enters, dig six cards for an Angel card, a Demon card, and/or a Dragon card and put them into your hand. Tuck the rest in a random order.
