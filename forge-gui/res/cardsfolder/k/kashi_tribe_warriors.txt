Name:Kashi-Tribe Warriors
ManaCost:3 G G
Types:Creature Snake Warrior
PT:2/4
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Creature | CombatDamage$ True | TriggerZones$ Battlefield | Execute$ TrigFreeze | TriggerDescription$ Whenever CARDNAME deals combat damage to a creature, freeze that creature.
SVar:TrigFreeze:DB$ Freeze | Defined$ TriggeredTarget
SVar:HasCombatEffect:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/kashi_tribe_warriors.jpg
Oracle:Whenever CARDNAME deals combat damage to a creature, freeze that creature.
