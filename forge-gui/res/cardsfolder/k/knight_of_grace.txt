Name:Knight of Grace
ManaCost:1 W
Types:Creature Human Knight
PT:2/2
K:First Strike
K:Hexproof:Card.Black:black
S:Mode$ Continuous | Affected$ Card.Self | AddPower$ 1 | IsPresent$ Permanent.Black | Description$ CARDNAME gets +1/+0 while any player controls a black permanent.
SVar:Picture:http://www.wizards.com/global/images/magic/general/knight_of_grace.jpg
Oracle:First strike, hexproof from black\nKnight of Grace gets +1/+0 while any player controls a black permanent.
