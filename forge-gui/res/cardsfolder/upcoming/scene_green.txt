Name:Scene Green
ManaCost:G 2
Types:Scene
Fate:3

A:AB$ Mana | Cost$ AddCounter<1/FATE> | Scene$ True | Produced$ G | Amount$ 1 | AILogic$ ManaRitual | SpellDescription$ Add {G}.

A:AB$ ChangeZone | Cost$ SubCounter<5/FATE> | Scene$ True | Climax$ True | Origin$ Hand | Destination$ Battlefield | ChangeType$ Creature.Green | ChangeNum$ 1 | SpellDescription$ You may put a green creature card from your hand onto the field.

Oracle:[+1]: Add {G}.\n[-5]: You may put a green creature card from your hand onto the field.
