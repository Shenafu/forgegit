Name:Scene Black
ManaCost:B 2
Types:Scene
Fate:2

A:AB$ Discard | Cost$ AddCounter<1/FATE> | Scene$ True | NumCards$ 1 | ValidTgts$ Player | Mode$ TgtChoose | SpellDescription$ Target player discards a card.

A:AB$ ChangeZoneAll | Cost$ SubCounter<5/FATE> | Scene$ True | Climax$ True | Origin$ Graveyard | Destination$ Exile | ValidTgts$ Player | RememberChanged$ True | SubAbility$ DBLoseLife | SpellDescription$ Exile all cards from target player's graveyard. If five or more cards are exiled this way, that player loses 5 life.

SVar:DBLoseLife:DB$ LoseLife | Defined$ Targeted | LifeAmount$ 5 | ConditionCheckSVar$ X | ConditionSVarCompare$ GE5 | References$ X | AILogic$ CheckCondition | SubAbility$ DBCleanup

SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True

SVar:X:Remembered$Amount

Oracle:[+1]: Target player discards a card.\n[-5]: Exile all cards from target player's graveyard. If five or more cards are exiled this way, that player loses 5 life.
