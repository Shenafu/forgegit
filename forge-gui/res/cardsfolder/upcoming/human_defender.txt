Name:Human Defender
ManaCost:W W 2
Types:Creature Human Warrior Cleric
PT: 2/3
K:Escort
K:Champion:Creature.Human+Warrior:Human Warrior

Text:When CARDNAME enters, specialize <b>Sword</b> or <b>Shield</b>.<hr><b><u>Sword</u></b> -- 3/3, Retaliate<hr><b><u>Shield</u></b> -- {W}{1}: Target creature gains absorb 1 until end of turn.

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigSpec | Secondary$ True | TriggerDescription$ When CARDNAME enters, specialize Sword or Shield.

SVar:TrigSpec:DB$ GenericChoice | Choices$ Sword,Shield | Defined$ You | SetChosenMode$ True | ConditionPresent$ Card.Self+ChosenMode | AILogic$ Sword | ShowChoice$ ExceptSelf

SVar:Sword:DB$ Animate | Defined$ Self | Permanent$ True | Power$ 3 | Toughness$ 3 | Keywords$ Retaliate | SpellDescription$ Sword

SVar:Shield:DB$ Animate | Defined$ Self | Permanent$ True | Abilities$ ABPump | SpellDescription$ Shield

SVar:ABPump:AB$ Pump | Cost$ W 1 | ValidCards$ Creature | KW$ Absorb:1 | SpellDescription$ Target creature gains absorb 1 until end of turn.

Oracle:Escort\nChampion a Human Warrior (When this enters, sacrifice it unless you exile another Human Warrior you control. When this leaves, that card returns to the field.)\nWhen CARDNAME enters, specialize Sword or Shield. (If this creature hasn't chosen a specialization, choose one of these choices.)\nSword -- 3/3, Retaliate\nShield -- {W}{1}: Target creature gains absorb 1 until end of turn.
