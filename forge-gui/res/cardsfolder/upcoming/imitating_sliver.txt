Name:Imitating Sliver
ManaCost:U 1
Types:Creature Sliver
PT:1/1

K:ETBReplacement:Copy:DBCopy:Optional

SVar:DBCopy:DB$ Clone | Choices$ Creature.Sliver+Other | SpellDescription$ You may have CARDNAME enter as a copy of any Sliver on the field.

Oracle: You may have CARDNAME enter as a copy of any Sliver on the field.