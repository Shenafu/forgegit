Name:Omenspeaker
ManaCost:1 U
Types:Creature Human Wizard
PT:1/3
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 2.
SVar:TrigScry:DB$ Scry | ScryNum$ 2
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/omenspeaker.jpg
Oracle:When Omenspeaker enters, scry 2.
