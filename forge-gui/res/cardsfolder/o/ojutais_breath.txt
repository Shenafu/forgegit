Name:Ojutai's Breath
ManaCost:2 U
Types:Instant
K:Rebound
A:SP$ Freeze | Cost$ 2 U | ValidTgts$ Creature | SpellDescription$ Freeze target creature.
SVar:Picture:http://www.wizards.com/global/images/magic/general/ojutais_breath.jpg
Oracle:Freeze target creature.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)
