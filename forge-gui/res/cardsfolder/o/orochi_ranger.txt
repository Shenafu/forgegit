Name:Orochi Ranger
ManaCost:1 G
Types:Creature Snake Warrior
PT:2/1
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Creature | CombatDamage$ True | TriggerZones$ Battlefield | Execute$ TrigFreeze | TriggerDescription$ Whenever CARDNAME deals combat damage to a creature, freeze that creature.
SVar:TrigFreeze:DB$ Freeze | Defined$ TriggeredTarget
SVar:HasCombatEffect:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/orochi_ranger.jpg
Oracle:Whenever Orochi Ranger deals combat damage to a creature, freeze that creature.
