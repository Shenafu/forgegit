Name:Oath of Elspeth
ManaCost:W W 3
Types:Legendary Enchantment

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigGainLife | TriggerDescription$ When CARDNAME enters, you gain 5 life.

SVar:TrigGainLife:DB$ GainLife | LifeAmount$ 5

T:Mode$ SpellCast | ValidCard$ Card | ValidActivatingPlayer$ You | Execute$ TrigCounterAll | TriggerZones$ Battlefield | TriggerDescription$ Whenever you cast a spell, each planeswalker you control gets a loyalty counter.

SVar:TrigCounterAll:DB$ PutCounterAll | ValidCards$ Planeswalker.YouCtrl | CounterType$ LOYALTY | CounterNum$ 1

SVar:PlayMain1:TRUE

SVar:BuffedBy:Card

DeckHas:Ability$Counters

Oracle:When CARDNAME enters, you gain 6 life.\nWhenever you cast a spell, each planeswalker you control gets a loyalty counter.
