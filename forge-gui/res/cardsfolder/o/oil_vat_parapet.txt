Name:Oil-Vat Parapet
ManaCost:2 R
Types:Creature Wall
PT:1/4
K:Defender

A:AB$ DamageAll | Cost$ 2 R F | ValidCards$ Creature.attacking | NumDmg$ 2 | SpellDescription$ CARDNAME deals 2 damage to each attacking creature.

Oracle:Defender\n{2}{R}, {F}: ~ deals 2 damage to each attacking creature.
