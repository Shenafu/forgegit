Name:Ojutai, Soul of Winter
ManaCost:5 W U
Types:Legendary Creature Dragon
PT:5/6
K:Flying
K:Vigilance
T:Mode$ Attacks | ValidCard$ Dragon.YouCtrl | Execute$ TrigFreeze | TriggerZones$ Battlefield | TriggerDescription$ Whenever a Dragon you control attacks, freeze target nonland permanent an opponent controls.
SVar:TrigFreeze:DB$ Freeze | ValidTgts$ Permanent.nonLand+OppCtrl | TgtPrompt$ Choose nonland permanent an opponent controls.
SVar:HasAttackEffect:TRUE
DeckHints:Type$Dragon
SVar:Picture:http://www.wizards.com/global/images/magic/general/ojutai_soul_of_winter.jpg
Oracle:Flying, vigilance\nWhenever a Dragon you control attacks, freeze target nonland permanent an opponent controls.
