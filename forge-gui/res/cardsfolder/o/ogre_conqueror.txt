Name:Ogre Conqueror
ManaCost:3 B R
Types:Creature Ogre Warrior
PT:2/4

K:Double strike
K:Trample

T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Player | CombatDamage$ True | Execute$ TrigControl | TriggerZones$ Battlefield | TriggerDescription$ When CARDNAME deals combat damage to a player, gain control of target land they control until end of turn.
SVar:TrigControl:DB$ GainControl | ValidTgts$ Land.DefenderCtrl | TgtPrompt$ Select target land they control | Untap$ True | Unlag$ True | LoseControl$ EOT

Oracle:Double strike, trample
When Ogre Conqueror deals combat damage to a player, gain control of target land they control until end of turn. Untap and unlag that land.
