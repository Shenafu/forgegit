Name:Oketra's Last Mercy
ManaCost:1 W W
Types:Sorcery
A:SP$ SetLife | Cost$ 1 W W | Defined$ You | LifeAmount$ X | References$ X | SubAbility$ DBPumpAll | SpellDescription$ Your life total becomes equal to your starting life total. Lands you control have frozen.
SVar:DBPumpAll:DB$ PumpAll | ValidCards$ Lands.YouCtrl | KW$ Frozen | Duration$ Permanent
SVar:X:Count$YourStartingLife
SVar:Picture:http://www.wizards.com/global/images/magic/general/oketras_last_mercy.jpg
Oracle:Your life total becomes equal to your starting life total. Lands you control have frozen.
