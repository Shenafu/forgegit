Name:Otarian Juggernaut
ManaCost:4
Types:Artifact Creature Juggernaut
PT:2/3
K:Elude:Creature.Wall:Wall
S:Mode$ Continuous | Affected$ Card.Self | AddPower$ 3 | AddKeyword$ Berserk | Condition$ Threshold | Description$ Threshold -- While seven or more cards are in your graveyard, CARDNAME gets +3/+0 and has berserk.
SVar:Picture:http://www.wizards.com/global/images/magic/general/otarian_juggernaut.jpg
Oracle:Eludes Walls.\nThreshold -- While seven or more cards are in your graveyard, Otarian Juggernaut gets +3/+0 and has berserk.
