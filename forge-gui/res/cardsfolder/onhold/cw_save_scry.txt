Name:CW Save Scry
ManaCost:W 1
Types:Instant

A:SP$ Protection | Cost$ W 1 | ValidTgts$ Creature.YouCtrl | TgtPrompt$ Select target creature you control | Gains$ Choice | Choices$ AnyColor | SubAbility$ DBScry | SpellDescription$ Target creature you control gains protection from the color of your choice until end of turn. Scry 2.
SVar:DBScry:DB$ Scry | ScryNum$ 2

DeckHas:Ability$Scry

Oracle:Target creature you control gains protection from the color of your choice until end of turn. Scry 2.
