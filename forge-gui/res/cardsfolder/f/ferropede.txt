Name:Ferropede
ManaCost:3
Types:Artifact Creature Insect
PT:1/1
K:Elude
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Player | CombatDamage$ True | OptionalDecider$ You | Execute$ TrigRemoveCounter | TriggerZones$ Battlefield | TriggerDescription$ Whenever CARDNAME deals combat damage to a player, you may remove a counter from target permanent.
SVar:TrigRemoveCounter:DB$ RemoveCounter | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | CounterType$ Any | CounterNum$ 1
SVar:Picture:http://www.wizards.com/global/images/magic/general/ferropede.jpg
Oracle:Elude\nWhenever Ferropede deals combat damage to a player, you may remove a counter from target permanent.
