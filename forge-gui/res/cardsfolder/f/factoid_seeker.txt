Name:Factoid Seeker
ManaCost:U U 1
Types:Creature Human Pirate
PT:3/2
K:Inform

Oracle:Inform (When CARDNAME attacks and isn't blocked, draw a card.)
