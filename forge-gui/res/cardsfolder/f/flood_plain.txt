Name:Flood Plain
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
A:AB$ ChangeZone | Cost$ T Sac<1/CARDNAME> | Origin$ Library | Destination$ Battlefield | ChangeType$ Plains,Island | ChangeNum$ 1 | SpellDescription$ Search your library for a Plains or Island card, put it onto the field, then shuffle your library.
SVar:Picture:http://www.wizards.com/global/images/magic/general/flood_plain.jpg
Oracle:Flood Plain enters tapped.\n{T}, Sacrifice Flood Plain: Search your library for a Plains or Island card, put it onto the field, then shuffle your library.
