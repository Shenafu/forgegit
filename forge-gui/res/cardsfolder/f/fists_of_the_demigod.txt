Name:Fists of the Demigod
ManaCost:1 BR
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 1 BR | ValidTgts$ Creature | AITgts$ Card.Black,Card.Red | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy+Black | AddPower$ 1 | AddToughness$ 1 | AddKeyword$ Wither | Description$ While enchanted creature is black, it gets +1/+1 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)
S:Mode$ Continuous | Affected$ Creature.EnchantedBy+Red | AddPower$ 1 | AddToughness$ 1 | AddKeyword$ First Strike | Description$ While enchanted creature is red, it gets +1/+1 and has first strike.
SVar:Picture:http://www.wizards.com/global/images/magic/general/fists_of_the_demigod.jpg
Oracle:Enchant creature\nWhile enchanted creature is black, it gets +1/+1 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)\nWhile enchanted creature is red, it gets +1/+1 and has first strike.
