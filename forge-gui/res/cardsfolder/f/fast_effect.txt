Name:Fast Effect
ManaCost:R R 1
Types:Tribal Instant Elemental
K:Cycling:4
K:Materialize:2

A:SP$ Pump | Cost$ R R 1 | KW$ Haste & Double Strike | ValidTgts$ Creature | SpellDescription$ Target creature gains haste and double strike until end of turn.

Oracle:Target creature gains haste and double strike until end of turn.\nMaterialize 2\nCycling {4}