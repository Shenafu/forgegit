Name:Flood Pipes
ManaCost:2 U
Types:Instant Arcane
K:Splice:Arcane:2 U U
A:SP$ TapAll | Cost$ 2 U | ValidCards$ Creature.withoutFlying | SpellDescription$ Tap all creatures without flying.

Oracle:Tap all creatures without flying.\nSplice onto Arcane {2}{U}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card's effects to that spell.)

