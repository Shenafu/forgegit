Name:Famous Dwarf
ManaCost:R R
Types:Creature Dwarf Knight
PT:0/1
K:Haste
K:Fame
Oracle:Haste\nFame (When this enters, you gain 1 honor until it leaves.)