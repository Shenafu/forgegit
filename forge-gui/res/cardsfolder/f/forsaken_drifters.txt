Name:Forsaken Drifters
ManaCost:3 B
Types:Creature Zombie
PT:4/2
T:Mode$ ChangesZone | Origin$ Battlefield | Destination$ Graveyard | ValidCard$ Card.Self | TriggerController$ TriggeredCardController | Execute$ TrigMill | TriggerDescription$ When CARDNAME dies, you mill four cards.
SVar:TrigMill:DB$ Mill | NumCards$ 4 | Defined$ You
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/forsaken_drifters.jpg
Oracle:When Forsaken Drifters dies, you mill four cards.
