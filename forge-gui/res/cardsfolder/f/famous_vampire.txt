Name:Famous Vampire
ManaCost:2 B B
Types:Creature Vampire Warrior
PT:1/1
K:Flying
K:Fame
Oracle:Flying\nFame (When this enters, you gain 1 honor until it leaves.)