Name:Fact Check
ManaCost:1 U
Types:Instant
K:Lore
A:SP$ Counter | Cost$ 1 U | TargetType$ Spell | TgtPrompt$ Select target spell | ValidTgts$ Card | UnlessCost$ 2 | SpellDescription$ Counter target spell unless its controller pays {2}.
Oracle:Counter target spell unless its controller pays {2}.\nLore (When you cast this, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)