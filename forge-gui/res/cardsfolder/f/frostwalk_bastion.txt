Name:Frostwalk Bastion
ManaCost:no cost
Types:Snow Land
A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Add {C}.
A:AB$ Animate | Cost$ 1 S | Defined$ Self | Power$ 2 | Toughness$ 3 | Types$ Creature,Artifact,Construct | SpellDescription$ Until end of turn, CARDNAME becomes a 2/3 Construct artifact creature. It's still a land. ({S} can be paid with one mana from a snow permanent.)
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Creature | CombatDamage$ True | TriggerZones$ Battlefield | Execute$ TrigFreeze | TriggerDescription$ Whenever CARDNAME deals combat damage to a creature, freeze that creature.
SVar:TrigFreeze:DB$ Freeze | Defined$ TriggeredTarget
SVar:HasCombatEffect:TRUE
Oracle:{T}: Add {C}.\n{1}{S}: Until end of turn, CARDNAME becomes also a 2/3 Construct artifact creature. ({S} can be paid with one mana from a snow permanent.)\nWhenever CARDNAME deals combat damage to a creature, freeze that creature.
