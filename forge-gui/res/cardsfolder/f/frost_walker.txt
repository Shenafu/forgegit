Name:Frost Walker
ManaCost:1 U
Types:Creature Elemental
PT:4/1
K:Hexdoom
SVar:Picture:http://www.wizards.com/global/images/magic/general/frost_walker.jpg
Oracle:Hexdoom (When Frost Walker becomes the target of a spell or ability, sacrifice it.)
