Name:Furnace Dragon
ManaCost:6 R R R
Types:Creature Dragon
PT:5/5
K:Flying
T:Mode$ ChangesZone | ValidCard$ Card.wasCastFromHand+Self | Destination$ Battlefield | Execute$ TrigExile | TriggerDescription$ When CARDNAME enters, if you cast it from your hand, exile all artifacts.
SVar:TrigExile:DB$ChangeZoneAll | ChangeType$ Artifact | Origin$ Battlefield | Destination$ Exile
S:Mode$ ReduceCost | ValidCard$ Card.Self | Type$ Spell | Amount$ X | EffectZone$ All | Description$ Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)
SVar:X:Count$Valid Artifact.YouCtrl
SVar:Picture:http://www.wizards.com/global/images/magic/general/furnace_dragon.jpg
Oracle:Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying\nWhen Furnace Dragon enters, if you cast it from your hand, exile all artifacts.
