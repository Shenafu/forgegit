Name:Yellow Scarves General
ManaCost:3 R
Types:Creature Human Soldier
PT:2/2
K:Defenseless
K:Horsemanship
SVar:Picture:http://www.wizards.com/global/images/magic/general/yellow_scarves_general.jpg
Oracle:Defenseless\nHorsemanship (This creature can't be blocked except by creatures with horsemanship.)
