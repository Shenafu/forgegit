Name:Yensid Gargoyle
ManaCost:5
Types:Artifact Creature Gargoyle
PT:5/5
K:Flying
K:Defender

S:Mode$ Continuous | Affected$ Card.Self+equipped,Card.Self+enchanted | AddHiddenKeyword$ CARDNAME can attack as though it didn't have defender. | Description$ CARDNAME can attack as though it didn't have defender while it's enchanted or equipped.

Oracle:Flying, defender\nYensid Gargoyle can attack as though it didn't have defender while it's enchanted or equipped.