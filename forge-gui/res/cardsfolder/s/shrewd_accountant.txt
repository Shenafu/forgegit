Name:Shrewd Accountant
ManaCost:W W 2
Types:Creature Cat Advisor
PT:2/3

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigGainLife | TriggerDescription$ When CARDNAME enters, you gain 1 life for each artifact, creature, and enchantment you control.

SVar:TrigGainLife:DB$ GainLife | LifeAmount$ X | References$ X

SVar:X:Count$Valid Artifact,Creature,Enchantment

Oracle:When Shrewd Accountant enters, you gain 1 life for each artifact, creature, and enchantment you control.