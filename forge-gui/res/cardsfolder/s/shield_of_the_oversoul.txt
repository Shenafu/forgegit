Name:Shield of the Oversoul
ManaCost:2 GW
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 2 GW | ValidTgts$ Creature | AITgts$ Card.Green,Card.White | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy+Green | AddPower$ 1 | AddToughness$ 1 | AddKeyword$ Indestructible | Description$ While enchanted creature is green, it gets +1/+1 and has indestructible.
S:Mode$ Continuous | Affected$ Creature.EnchantedBy+White | AddPower$ 1 | AddToughness$ 1 | AddKeyword$ Flying | Description$ While enchanted creature is white, it gets +1/+1 and has flying.
SVar:Picture:http://www.wizards.com/global/images/magic/general/shield_of_the_oversoul.jpg
Oracle:Enchant creature\nWhile enchanted creature is green, it gets +1/+1 and has indestructible.\nWhile enchanted creature is white, it gets +1/+1 and has flying.
