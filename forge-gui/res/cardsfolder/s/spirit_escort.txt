Name:Spirit Escort
ManaCost:W 1
Types:Creature Spirit
PT:1/1
K:Flying

S:Mode$ ReduceCost | ValidCard$ Aura,Spirit | Type$ Spell | Activator$ You | Amount$ 1 | Description$ Aura spells and Spirit spells you cast cost {1} less to cast.

Oracle:Flying\nAura spells and Spirit spells you cast cost {1} less to cast.