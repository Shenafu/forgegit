Name:Sengir Bats
ManaCost:1 B B
Types:Creature Bat
PT:1/2
K:Flying
K:Predation:1
SVar:Picture:http://www.wizards.com/global/images/magic/general/sengir_bats.jpg
Oracle:Flying\nPredation (Whenever Sengir Bats defeats another creature, it gets a +1/+1 counter.)
