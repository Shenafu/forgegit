Name:Swiftwater Cliffs
ManaCost:no cost
Types:Land
A:AB$ Mana | Cost$ T | Produced$ U | SpellDescription$ Add {U}.
A:AB$ Mana | Cost$ T | Produced$ R | SpellDescription$ Add {R}.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigGainLife | TriggerDescription$ When CARDNAME enters, you gain 1 life.
SVar:TrigGainLife:DB$GainLife | LifeAmount$ 1
K:CARDNAME enters tapped.
SVar:Picture:http://www.wizards.com/global/images/magic/general/swiftwater_cliffs.jpg
Oracle:Swiftwater Cliffs enters tapped.\nWhen Swiftwater Cliffs enters, you gain 1 life.\n{T}: Add {U} or {R}.