Name:Sower of Temptation
ManaCost:2 U U
Types:Creature Faerie Wizard
PT:2/2
K:Flying
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigChange | TriggerDescription$ When CARDNAME enters, gain control of target creature while CARDNAME remains on the field.
SVar:TrigChange:DB$ GainControl | TgtPrompt$ Choose target creature | ValidTgts$ Creature | LoseControl$ LeavesPlay | SpellDescription$ Gain control of target creature while CARDNAME remains on the field.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/sower_of_temptation.jpg
Oracle:Flying\nWhen Sower of Temptation enters, gain control of target creature while Sower of Temptation remains on the field.
