Name:Soratami Mirror-Guard
ManaCost:3 U
Types:Creature Moonfolk Wizard
PT:3/1
K:Flying
A:AB$ Pump | Cost$ 2 Return<1/Land> | ValidTgts$ Creature.powerLE2 | TgtPrompt$ Select target creature with power 2 or less | KW$ Elude | SpellDescription$ Target creature with power 2 or less gains elude until end of turn.
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/soratami_mirror_guard.jpg
Oracle:Flying\n{2}, Return a land you control to its owner's hand: Target creature with power 2 or less gains elude until end of turn.
