Name:Soul Search
ManaCost:W 1
Types:Tribal Sorcery Spirit

A:SP$ ChangeZone | Cost$ W 1 | Origin$ Library | Destination$ Hand | ChangeType$ Aura,Spirit | ChangeNum$ 1 | SpellDescription$ Search your library for an Aura or Spirit card, reveal it, put it into your hand, and shuffle.

Oracle:Search your library for an Aura or Spirit card, reveal it, put it into your hand, and shuffle.