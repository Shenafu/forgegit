Name:Street Riot
ManaCost:4 R
Types:Enchantment
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddPower$ 1 | AddKeyword$ Trample | Condition$ PlayerTurn | Description$ While it's your turn, creatures you control get +1/+0 and have trample.
Oracle:While it's your turn, creatures you control get +1/+0 and have trample.
