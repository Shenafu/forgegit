Name:Sacred Ground
ManaCost:1 W
Types:Enchantment
T:Mode$ Sacrificed | ValidCard$ Land.YouOwn | ValidSourceController$ Player.Opponent | Execute$ TrigReturn | TriggerZones$ Battlefield | TriggerDescription$ Whenever a spell or ability an opponent controls causes a land to be put into your graveyard from the field, return that card to the field.
T:Mode$ Destroyed | ValidCauser$ Player.Opponent | ValidCard$ Land.YouOwn | Execute$ TrigReturn | Secondary$ True | TriggerZones$ Battlefield | TriggerDescription$ Whenever a spell or ability an opponent controls causes a land to be put into your graveyard from the field, return that card to the field.
SVar:TrigReturn:DB$ ChangeZone | Defined$ TriggeredCard | Origin$ Graveyard | Destination$ Battlefield
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/sacred_ground.jpg
Oracle:Whenever a spell or ability an opponent controls causes a land to be put into your graveyard from the field, return that card to the field.
