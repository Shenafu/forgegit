Name:Susyon of Summer Storms
ManaCost:G
Types:Legendary Planeswalker Susyon
Loyalty:1

T:Mode$ Phase | Phase$ Upkeep | ValidPlayer$ You | TriggerZones$ Battlefield | IsPresent$ Land.YouCtrl | PresentCompare$ GE6 | Execute$ TrigTransform | TriggerDescription$ At the beginning of your upkeep, if you control six or more lands, you may transform CARDNAME.

SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform | AILogic$ Always

A:AB$ Mana | Cost$ AddCounter<2/LOYALTY> | Planeswalker$ True | Produced$ C | Amount$ 1 | SpellDescription$ Add {C}.

A:AB$ DealDamage | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | ValidTgts$ Creature.withFlying | TgtPrompt$ Select target creature with flying | NumDmg$ 2 | SpellDescription$ CARDNAME deals 2 damage to target creature with flying.

AlternateMode:DoubleFaced

Oracle:At the beginning of your upkeep, if you control six or more lands, you may transform Susyon of Summer Storms.\n[+2]: Add {C}.\n[+1]: Susyon of Summer Storms deals 2 damage to target creature with flying.

ALTERNATE

Name:Susyon of Fall Harvest
ManaCost:no cost
Colors:green
Types:Legendary Planeswalker Susyon

A:AB$ Token | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | TokenAmount$ 1 | TokenName$ Elf Druid | TokenTypes$ Creature,Elf,Druid | TokenOwner$ You | TokenColors$ Green | TokenPower$ 1 | TokenToughness$ 1 | TokenAbilities$ DBMana | SpellDescription$ Create a 1/1 green Elf Druid creature token with "{T}: Add {G}."
SVar:DBMana:AB$ Mana | Cost$ T | Produced$ G | SpellDescription$ Add {G}.

A:AB$ UntapAll | Cost$ SubCounter<4/LOYALTY> | Planeswalker$ True | ValidCards$ Land.YouCtrl | SpellDescription$ Untap all lands you control.

A:AB$ GainLife | Cost$ SubCounter<7/LOYALTY> | Planeswalker$ True | Ultimate$ True | Defined$ You | References$ X | LifeAmount$ X | SpellDescription$ You gain life equal to the amount of unspent mana you have.
SVar:X:Count$ManaPool:All

Oracle:[+1]: Create a 1/1 green Elf Druid creature token with "{T}: Add {G}".\n[-4]: Untap all lands you control.\n[-7]: ULTIMATE -- You gain life equal to the amount of unspent mana you have.
