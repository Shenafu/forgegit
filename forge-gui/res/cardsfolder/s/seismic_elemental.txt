Name:Seismic Elemental
ManaCost:3 R R
Types:Creature Elemental
PT:4/4
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigEffect | TriggerDescription$ When CARDNAME enters, creatures without flying can't block this turn.
SVar:TrigEffect:DB$Effect | Name$ Seismic Elemental Effect | StaticAbilities$ KWPump | SpellDescription$ Creatures without flying can't block this turn.
SVar:KWPump:Mode$ Continuous | EffectZone$ Command | AffectedZone$ Battlefield | Affected$ Creature.withoutFlying | AddKeyword$ Defenseless | Description$ Creatures without flying gain defenseless until end of turn.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/seismic_elemental.jpg
Oracle:When Seismic Elemental enters, creatures without flying gain defenseless until end of turn.
