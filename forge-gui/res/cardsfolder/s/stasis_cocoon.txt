Name:Stasis Cocoon
ManaCost:1 W
Types:Enchantment Aura
K:Enchant artifact
A:SP$ Attach | Cost$ 1 W | ValidTgts$ Artifact | AITgts$ Creature,Card.hasActivatedAbility | AILogic$ Curse
S:Mode$ Continuous | Affected$ Card.EnchantedBy | AddKeyword$ Defender & Defenseless & Disability | Description$ Enchanted artifact has defender, defenseless, and disability.
SVar:Picture:http://www.wizards.com/global/images/magic/general/stasis_cocoon.jpg
Oracle:Enchant artifact\nEnchanted artifact has defender, defenseless, and disability.
