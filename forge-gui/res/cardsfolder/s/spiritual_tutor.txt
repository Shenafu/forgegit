Name:Spiritual Tutor
ManaCost:B P2 P2
Types:Tribal Sorcery Spirit

A:SP$ ChangeZone | Cost$ B P2 P2 | Origin$ Library,Graveyard | Destination$ Hand | ChangeType$ Card | ChangeNum$ 1 | SpellDescription$ Search your library and graveyard for a card and put that card into your hand. If you searched your library, shuffle.

Oracle:Search your library and/or graveyard for a card and put that card into your hand. If you searched your library, shuffle.