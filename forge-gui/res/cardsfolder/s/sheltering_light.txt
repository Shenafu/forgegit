Name:Sheltering Light
ManaCost:W
Types:Instant
A:SP$ Pump | Cost$ W | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Indestructible | SubAbility$ DBScry | SpellDescription$ Target creature gains indestructible until end of turn. Scry 1.
SVar:DBScry:DB$Scry | ScryNum$ 1
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/sheltering_light.jpg
Oracle:Target creature gains indestructible until end of turn. Scry 1.