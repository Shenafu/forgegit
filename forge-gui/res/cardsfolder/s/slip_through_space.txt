Name:Slip Through Space
ManaCost:U
Types:Sorcery
K:Devoid
A:SP$ Pump | Cost$ U | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Elude | SubAbility$ DBDraw | SpellDescription$ Target creature gains elude until end of turn. Draw a card.
SVar:DBDraw:DB$ Draw | NumCards$ 1
SVar:Picture:http://www.wizards.com/global/images/magic/general/slip_through_space.jpg
Oracle:Devoid (This card has no color.)\nTarget creature gains elude until end of turn.\nDraw a card.
