Name:Shield of Reflection
ManaCost:4
Types:Artifact Creature Armament
PT:3/1

A:AB$ Pump | Cost$ WB 1 X | ValidTgts$ Damageable | References$ X | SubAbility$ DBPrevention | StackDescription$ none | AILogic$ Never | SpellDescription$ Prevent the next X damage that would be dealt to CARDNAME this turn. If damage is prevented this way, CARDNAME deals that much damage to any target.

SVar:DBPrevention:DB$ PreventDamage | Defined$ Self | Amount$ X | References$ X | PreventionSubAbility$ DBReflect | ShieldEffectTarget$ ParentTarget

SVar:DBReflect:DB$ DealDamage | Defined$ ShieldEffectTarget | NumDmg$ PreventedDamage | SpellDescription$ CARDNAME deals that much damage to any target.

SVar:X:Count$xPaid

Oracle:{WB}{1}{X}: Prevent the next X damage that would be dealt to Shield of Reflection this turn. If damage is prevented this way, Shield of Reflection deals that much damage to any target.