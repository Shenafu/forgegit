Name:Smoldering Spires
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
A:AB$ Mana | Cost$ T | Produced$ R | SpellDescription$ Add {R}.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | TriggerDescription$ When CARDNAME enters, target creature gains defenseless until end of turn.
SVar:TrigPump:DB$ Pump | ValidTgts$ Creature | IsCurse$ True | KW$ Defenseless | TgtPrompt$ Select target creature.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/Smoldering_Spires.jpg
Oracle:Smoldering Spires enters tapped.\nWhen Smoldering Spires enters, target creature gains defenseless until end of turn.\n{T}: Add {R}.
