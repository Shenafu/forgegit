Name:Soul Leverage
ManaCost:B
Types:Tribal Instant Spirit

A:SP$ Pump | Cost$ B | ValidTgts$ Creature | NumAtt$ 2 | KW$ Indestructible | SpellDescription$ Target creature gets +2/+0 and gains indestructible until end of turn.

Oracle:Target creature gets +2/+0 and gains indestructible until end of turn.