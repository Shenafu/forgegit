Name:Suncleanser
ManaCost:1 W
Types:Creature Human Cleric
T:Mode$ ChangesZone | ValidCard$ Card.Self | Origin$ Any | Destination$ Battlefield | Execute$ TrigCharm | TriggerDescription$ When CARDNAME enters, choose one --\n• Remove all counters from target creature. It can't have counters put on it while CARDNAME remains on the field.\n• Target opponent loses all counters. That player can't get counters while CARDNAME remains on the field.
SVar:TrigCharm:DB$ Charm | Choices$ CreatureDBRemoveCounter,OpponentDBRemoveCounter | CharmNum$ 1
SVar:CreatureDBRemoveCounter:DB$ RemoveCounter | ValidTgts$ Creature | RememberObjects$ Targeted | TgtPrompt$ Select target creature | CounterType$ All | CounterNum$ All | SubAbility$ DBPumpCreature | SpellDescription$ Remove all counters from target creature. It can't have counters put on it while CARDNAME remains on the field.
SVar:OpponentDBRemoveCounter:DB$ RemoveCounter | ValidTgts$ Opponent | RememberObjects$ Targeted | TgtPrompt$ Select target opponent | CounterType$ All | CounterNum$ All | SubAbility$ DBPumpOpponent | SpellDescription$ Target opponent loses all counters. That player can't get counters while CARDNAME remains on the field.
SVar:DBPumpCreature:DB$ Pump | Defined$ Targeted | KW$ CARDNAME can't have counters put on it. | UntilLoseControlOfHost$ True
SVar:DBPumpOpponent:DB$ Pump | Defined$ Targeted | KW$ PLAYER can't have counters put on him or her. | UntilLoseControlOfHost$ True
Oracle:When Suncleanser enters, choose one --\n• Remove all counters from target creature. It can't have counters put on it while Suncleanser remains on the field.\n• Target opponent loses all counters. That player can't get counters while Suncleanser remains on the field.
PT:1/4