Name:Subterranean Scout
ManaCost:1 R
Types:Creature Goblin Scout
PT:2/1
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | TriggerDescription$ When CARDNAME enters, target creature with power 2 or less gains elude until end of turn.
SVar:TrigPump:DB$ Pump | ValidTgts$ Creature.powerLE2 | TgtPrompt$ Select target creature with power 2 or less. | KW$ Elude
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/subterranean_scout.jpg
Oracle:When Subterranean Scout enters, target creature with power 2 or less gains elude until end of turn.
