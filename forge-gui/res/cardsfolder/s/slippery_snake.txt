Name:Slippery Snake
ManaCost:G U 1
Types:Creature Snake Mutant
PT:3/2
K:Subsurface
T:Mode$ LandPlayed | ValidCard$ Land.YouCtrl | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigTransform | TriggerDescription$ Whenever you play a land, you may transform CARDNAME.
SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform | AILogic$ Always
AlternateMode:DoubleFaced

Oracle:Subsurface\nWhenever you play a land, you may transform Slippery Snake.

ALTERNATE

Name:Slippery Serpent
ManaCost:no cost
Colors:green,blue
Types:Creature Serpent Mutant
PT:5/4
K:Subsurface
K:Hexproof

T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Opponent | TriggerZones$ Battlefield | MutantUntransformCondition$ True | Execute$ TrigTransform | TriggerDescription$ At the beginning of each opponent's end step, if that player didn't play a land, transform CARDNAME.

SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform

Oracle:Subsurface, hexproof\nAt the beginning of each opponent's end step, if that player didn't play a land, transform Slippery Serpent.
