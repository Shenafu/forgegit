Name:Supreme Exemplar
ManaCost:6 U
Types:Creature Elemental
PT:10/10
K:Flying
K:Champion:Elemental
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/supreme_exemplar.jpg
Oracle:Flying\nChampion an Elemental (When this enters, sacrifice it unless you exile another Elemental you control. When this leaves, that card returns to the field.)
