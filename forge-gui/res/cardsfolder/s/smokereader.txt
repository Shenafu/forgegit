Name:Smokereader
ManaCost:R 2
Types:Creature Ogre Shaman
PT:2/2
K:First strike

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigMill | TriggerDescription$ When CARDNAME enters, <b><u>imagine 1.</u></b> (Exile the top card of your library. Until end of turn, you may play that card.)
SVar:TrigMill:DB$ Mill | Defined$ You | NumCards$ 1 | Destination$ Exile | RememberMilled$ True | SubAbility$ DBEffect
SVar:DBEffect:DB$ Effect | RememberObjects$ RememberedCard | StaticAbilities$ Play | SubAbility$ DBCleanup | ExileOnMoved$ Exile
SVar:Play:Mode$ Continuous | MayPlay$ True | EffectZone$ Command | Affected$ Card.IsRemembered | AffectedZone$ Exile | Description$ You may play remembered card.
SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True
SVar:PlayMain1:ALWAYS
DeckHas:Ability$Mill
Oracle:First Strike\nWhen Smokereader enters, imagine 1. (Exile the top card of your library. Until end of turn, you may play that card.)
