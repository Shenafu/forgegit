Name:Shambling Remains
ManaCost:1 B R
Types:Creature Zombie Horror
PT:4/3
K:Defenseless
K:Unearth:B R
SVar:Picture:http://www.wizards.com/global/images/magic/general/shambling_remains.jpg
Oracle:Defenseless\nUnearth {B}{R} ({B}{R}: Return this card from your graveyard to the field. It gains haste. Exile it at the beginning of the next end step or if it would leave. Unearth only as a sorcery.)
