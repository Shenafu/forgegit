Name:Striped Riverwinder
ManaCost:6 U
Types:Creature Serpent
PT:5/5
K:Hexproof
K:Cycling:U
SVar:Picture:http://www.wizards.com/global/images/magic/general/striped_riverwinder.jpg
Oracle:Hexproof\nCycling {U} ({U}, Discard this card: Draw a card.)