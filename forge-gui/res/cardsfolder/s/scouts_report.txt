Name:Scout's Report
ManaCost:1
Types:Artifact

A:AB$ ChangeZone | Cost$ 2 T Sac<1/CARDNAME> | Origin$ Library | Destination$ Battlefield | ChangeNum$ 1 | ChangeType$ Land.Plains,Land.Swamp | SpellDescription$ Search your library for a Plains or Swamp card and put it onto the field tapped. Shuffle.

Oracle:{2}, {T}, Sacrifice Scout's Report: Search your library for a Plains or Swamp card and put it onto the field tapped. Shuffle.