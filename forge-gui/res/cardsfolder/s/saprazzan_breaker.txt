Name:Saprazzan Breaker
ManaCost:4 U
Types:Creature Beast
PT:3/3
A:AB$ Dig | Cost$ U | DigNum$ 1 | Reveal$ True | ChangeNum$ All | RememberRevealed$ True | DestinationZone$ Graveyard | SubAbility$ DBPump | SpellDescription$ You mill a card. If that card is a land card, CARDNAME gains elude until end of turn.
SVar:DBPump:DB$ Pump | Defined$ Self | KW$ Elude | ConditionDefined$ Remembered | ConditionPresent$ Card.Land | ConditionCompare$ EQ1 | SubAbility$ DBCleanup
SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/saprazzan_breaker.jpg
Oracle:{U}: You mill a card. If that card is a land card, Saprazzan Breaker gains elude until end of turn.
