Name:Suicidal Charge
ManaCost:3 B R
Types:Enchantment
A:AB$ PumpAll | Cost$ Sac<1/CARDNAME> | ValidCards$ Creature.OppCtrl | NumAtt$ -1 | NumDef$ -1 | KW$ Berserk | IsCurse$ True | SpellDescription$ Creatures your opponents control get -1/-1 and gain berserk until end of turn.
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/suicidal_charge.jpg
Oracle:Sacrifice Suicidal Charge: Creatures your opponents control get -1/-1 and gain berserk until end of turn.
