Name:Stymied Hopes
ManaCost:1 U
Types:Instant
A:SP$ Counter | Cost$ 1 U | TargetType$ Spell | TgtPrompt$ Select target spell | ValidTgts$ Card | UnlessCost$ 1 | SubAbility$ DBScry | SpellDescription$ Counter target spell unless its controller pays {1}. Scry 1.
SVar:DBScry:DB$ Scry | ScryNum$ 1
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/stymied_hopes.jpg
Oracle:Counter target spell unless its controller pays {1}. Scry 1.
