Name:Suspicious Bookcase
ManaCost:2
Types:Artifact Creature Wall
PT:0/4
K:Defender
A:AB$ Pump | Cost$ 3 T | ValidTgts$ Creature | KW$ Elude | TgtPrompt$ Select target creature. | SpellDescription$ Target creature gains elude until end of turn.
SVar:Picture:http://www.wizards.com/global/images/magic/general/suspicious_bookcase.jpg
Oracle:Defender\n{3}, {T}: Target creature gains elude until end of turn.
