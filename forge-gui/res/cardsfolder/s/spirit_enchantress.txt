Name:Spirit Enchantress
ManaCost:W W 2
Types:Creature Spirit
PT:2/2

T:Mode$ SpellCast | ValidCard$ Aura,Spirit | ValidActivatingPlayer$ You | TriggerZones$ Battlefield | Execute$ TrigDraw | OptionalDecider$ You | TriggerDescription$ Whenever you cast an Aura or Spirit spell, you may draw a card.

SVar:TrigDraw:DB$ Draw | NumCards$ 1 | Defined$ You

S:Mode$ Continuous | Affected$ Creature.Essence+YouCtrl,Creature.Spirit+Other+YouCtrl | AddPower$ 1 | AddToughness$ 1 | Description$ Essence creatures and other Spirit creatures you control get +1/+1.

SVar:PlayMain1:TRUE

Oracle:Whenever you cast an Aura or Spirit spell, you may draw a card.\nEssence creatures and other Spirit creatures you control get +1/+1.