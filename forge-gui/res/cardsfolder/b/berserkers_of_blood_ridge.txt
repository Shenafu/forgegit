Name:Berserkers of Blood Ridge
ManaCost:4 R
Types:Creature Human Berserker
PT:4/4
K:Berserk
SVar:Picture:http://www.wizards.com/global/images/magic/general/berserkers_of_blood_ridge.jpg
Oracle:Berserk (Attacks each combat if able)
