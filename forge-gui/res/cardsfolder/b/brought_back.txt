Name:Brought Back
ManaCost:W W
Types:Instant
A:SP$ ChangeZone | Cost$ W W | ValidTgts$ Permanent.YouOwn+ThisTurnEntered_Graveyard_from_Battlefield | TgtPrompt$ Select up to two target permanent cards in your graveyard that were put there from the field this turn | TargetMin$ 0 | TargetMax$ 2 | Origin$ Graveyard | Destination$ Battlefield | Tapped$ True | SpellDescription$ Choose up to two target permanent cards in your graveyard that were put there from the field this turn. Return them to the field tapped.
Oracle:Choose up to two target permanent cards in your graveyard that were put there from the field this turn. Return them to the field tapped.
