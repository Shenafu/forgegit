Name:Bloodsucker Bats
ManaCost:B B 1
Types:Creature Bat
PT:1/2
K:Flying
K:Predation:1

Oracle:Flying\nPredation (Whenever this creature defeats another creature, it gets a +1/+1 counter.)
