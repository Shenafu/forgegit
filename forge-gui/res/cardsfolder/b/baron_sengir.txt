Name:Baron Sengir
ManaCost:5 B B B
Types:Legendary Creature Vampire
PT:5/5
K:Flying
K:Predation:2
A:AB$Regenerate | ValidTgts$ Creature.Vampire+Other | TgtPrompt$ Select another target Vampire | Cost$ T | SpellDescription$ Regenerate another target Vampire.
SVar:Picture:http://www.wizards.com/global/images/magic/general/baron_sengir.jpg
Oracle:Flying, predation 2\n{T}: Regenerate another target Vampire.
