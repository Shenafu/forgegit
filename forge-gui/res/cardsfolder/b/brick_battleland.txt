Name:Brick Battleland
ManaCost:no cost
Types:Land

A:AB$ Mana | Cost$ T | Produced$ B | SpellDescription$ Add {B}.
A:AB$ Mana | Cost$ T | Produced$ R | SpellDescription$ Add {R}.

K:ETBReplacement:Other:LandTapped

SVar:LandTapped:DB$ Tap | Defined$ Self | ETB$ True | ConditionCheckSVar$ X | ConditionSVarCompare$ M21 | SpellDescription$ CARDNAME enters tapped unless you control an odd number of lands including this.

SVar:X:Count$Valid Land.YouCtrl

Oracle:Brick Battleland enters tapped unless you control an odd number of lands including this.\n{T}: Add {B} or {R}.