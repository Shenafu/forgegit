Name:Boneshard Slasher
ManaCost:1 B
Types:Creature Horror
PT:1/1
K:Flying
S:Mode$ Continuous | Affected$ Card.Self | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Hexdoom | Condition$ Threshold | Description$ Threshold -- While seven or more cards are in your graveyard, CARDNAME gets +2/+2 and has hexdoom.

SVar:Picture:http://www.wizards.com/global/images/magic/general/boneshard_slasher.jpg
Oracle:Flying\nThreshold -- While seven or more cards are in your graveyard, Boneshard Slasher gets +2/+2 and has hexdoom.
