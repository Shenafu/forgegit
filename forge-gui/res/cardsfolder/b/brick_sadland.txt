Name:Brick Sadland
ManaCost:no cost
Types:Land

A:AB$ Mana | Cost$ T PayLife<1> | Produced$ B | SpellDescription$ Add {B}.
A:AB$ Mana | Cost$ T PayLife<1> | Produced$ R | SpellDescription$ Add {R}.
A:AB$ Mana | Cost$ T Sac<1/Creature> | Produced$ B | SpellDescription$ Add {B}.
A:AB$ Mana | Cost$ T Sac<1/Creature> | Produced$ R | SpellDescription$ Add {R}.

Oracle:{T}, Pay 1 life or sacrifice a creature: Add {B} or {R}.