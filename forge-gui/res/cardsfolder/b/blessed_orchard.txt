Name:Blessed Orchard
ManaCost:W W 1
Types:Enchantment Aura
K:Enchant land

A:SP$ Attach | Cost$ W W 1 | ValidTgts$ Land

T:Mode$ Taps | ValidCard$ Card.AttachedBy | TriggerZones$ Battlefield | Execute$ DBGainLife | TriggerDescription$ Whenever enchanted land becomes tapped, you gain 2 life.

SVar:DBGainLife:DB$ GainLife | LifeAmount$ 2

Oracle:Enchant land\nWhenever enchanted land becomes tapped, you gain 2 life.
