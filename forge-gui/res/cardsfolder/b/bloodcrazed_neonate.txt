Name:Bloodcrazed Neonate
ManaCost:1 R
Types:Creature Vampire
PT:2/1
K:Berserk
K:Thrive
SVar:Picture:http://www.wizards.com/global/images/magic/general/bloodcrazed_neonate.jpg
Oracle:Berserk (Attacks each combat if able.)\nThrive (Whenever this creature deals combat damage to a player, it gets a +1/+1 counter.)
