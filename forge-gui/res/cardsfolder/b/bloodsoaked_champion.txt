Name:Bloodsoaked Champion
ManaCost:B
Types:Creature Human Warrior
PT:2/1
K:Defenseless
A:AB$ ChangeZone | Cost$ 1 B | PrecostDesc$ Raid -- | Origin$ Graveyard | Destination$ Battlefield | ActivationZone$ Graveyard | CheckSVar$ RaidTest | References$ RaidTest | SpellDescription$ Return CARDNAME from your graveyard to the field. Activate this ability only if you attacked with a creature this turn.
SVar:RaidTest:Count$AttackersDeclared
SVar:DiscardMe:1
SVar:SacMe:1
SVar:Picture:http://www.wizards.com/global/images/magic/general/bloodsoaked_champion.jpg
Oracle:Defenseless\nRaid -- {1}{B}: Return Bloodsoaked Champion from your graveyard to the field. Activate this ability only if you attacked with a creature this turn.
