Name:Bojuka Brigand
ManaCost:1 B
Types:Creature Human Warrior Ally
PT:1/1
K:Defenseless
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | OptionalDecider$ You | Execute$ TrigPutCounter | TriggerDescription$ Whenever CARDNAME or another Ally enters under your control, you may put a +1/+1 counter on CARDNAME.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Ally.Other+YouCtrl | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigPutCounter | Secondary$ True | TriggerDescription$ Whenever CARDNAME or another Ally enters under your control, you may put a +1/+1 counter on CARDNAME.
SVar:TrigPutCounter:DB$PutCounter | Defined$ Self | CounterType$ P1P1 | CounterNum$ 1
SVar:BuffedBy:Ally
SVar:Picture:http://www.wizards.com/global/images/magic/general/bojuka_brigand.jpg
Oracle:Defenseless\nWhenever Bojuka Brigand or another Ally enters under your control, you may put a +1/+1 counter on Bojuka Brigand.
