Name:Bloodghast
ManaCost:B B
Types:Creature Vampire Spirit
PT:2/1
K:Defenseless
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Haste | CheckSVar$ X | SVarCompare$ LE10| Description$ CARDNAME has haste while an opponent has 10 or less life.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Land.YouCtrl | OptionalDecider$ You | TriggerZones$ Graveyard | Execute$ TrigChange | TriggerDescription$ Landfall -- Whenever a land enters under your control, you may return CARDNAME from your graveyard to the field.
SVar:TrigChange:DB$ChangeZone | Origin$ Graveyard | Destination$ Battlefield
SVar:X:PlayerCountOpponents$LowestLifeTotal
SVar:SacMe:3
SVar:DiscardMe:3
SVar:Picture:http://www.wizards.com/global/images/magic/general/bloodghast.jpg
Oracle:Defenseless\nBloodghast has haste while an opponent has 10 or less life.\nLandfall -- Whenever a land enters under your control, you may return Bloodghast from your graveyard to the field.
