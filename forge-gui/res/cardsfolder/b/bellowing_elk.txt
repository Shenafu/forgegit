Name:Bellowing Elk
ManaCost:3 G
Types:Creature Elk
PT:4/2
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Trample & Indestructible | CheckSVar$ X | SVarCompare$ GE1 | Description$ While you had another creature enter under your control this turn, CARDNAME has trample and indestructible.
SVar:X:Count$ThisTurnEntered_Battlefield_Creature.YouCtrl+Other
Oracle:While you had another creature enter under your control this turn, Bellowing Elk has trample and indestructible.
