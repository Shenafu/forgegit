Name:Hooded Horror
ManaCost:4 B
Types:Creature Horror
PT:4/4
S:Mode$ Continuous | Affected$ Card.Self+attacking | AddKeyword$ Elude | CheckSVar$ X | References$ X,Y | SVarCompare$ EQY | Description$ CARDNAME has elude while defending player controls the most creatures or is tied for the most.
SVar:X:Count$Valid Creature.DefenderCtrl
SVar:Y:PlayerCountPlayers$HighestValid Creature.YouCtrl
SVar:Picture:http://www.wizards.com/global/images/magic/general/hooded_horror.jpg
Oracle:Hooded Horror has elude while defending player controls the most creatures or is tied for the most.
