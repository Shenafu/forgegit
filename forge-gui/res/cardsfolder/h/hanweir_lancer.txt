Name:Hanweir Lancer
ManaCost:2 R
Types:Creature Human Knight
PT:2/2
K:Soulbond
S:Mode$ Continuous | Affected$ Creature.PairedWith,Creature.Self+Paired | AddKeyword$ First Strike | Description$ While CARDNAME is paired with another creature, both creature have first strike.
SVar:Picture:http://www.wizards.com/global/images/magic/general/hanweir_lancer.jpg
Oracle:Soulbond (You may pair this creature with another unpaired creature when either enters. They remain paired while you control both of them.)\nWhile Hanweir Lancer is paired with another creature, both creatures have first strike.
