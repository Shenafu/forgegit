Name:Heart-Pumping Song
ManaCost:R
Types:Instant
K:Lore
A:SP$ Pump | Cost$ R | ValidTgts$ Creature | TgtPrompt$ Select target creature to gain First Strike | KW$ First Strike | SpellDescription$ Target creature gains first strike until end of turn.
Oracle:Target creature gains first strike until end of turn.\nLore (When you cast this, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)