Name:Hallowed Fountain
ManaCost:no cost
Types:Land Plains Island
K:ETBReplacement:Other:DBTap
SVar:DBTap:DB$ Tap | ETB$ True | Defined$ Self | UnlessCost$ PayLife<2> | UnlessPayer$ You | UnlessAI$ Shockland | StackDescription$ enters tapped. | SpellDescription$ As CARDNAME enters, you may pay 2 life. If you don't, CARDNAME enters tapped.
SVar:Picture:http://resources.wizards.com/magic/cards/dis/en-us/card97071.jpg
Oracle:({T}: Add {W} or {U}.)\nAs Hallowed Fountain enters, you may pay 2 life. If you don't, Hallowed Fountain enters tapped.
