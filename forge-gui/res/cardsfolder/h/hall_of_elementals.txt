Name:Hall of Elementals
ManaCost:G2 U2 R2
Types:Tribal Enchantment Elemental

T:Mode$ SpellCast | ValidCard$ Card | ValidActivatingPlayer$ You | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigToken | TriggerDescription$ Whenever you cast a spell, you may create a 1/1 Elemental creature token that is all colors with "Sacrifice this creature: Add one mana of any color."

SVar:TrigToken:DB$ Token | TokenScript$ soling_elemental

Oracle:Whenever you cast a spell, you may create a 1/1 Elemental creature token that is all colors with "Sacrifice this creature: Add one mana of any color."