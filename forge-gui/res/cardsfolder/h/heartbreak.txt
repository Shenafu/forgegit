Name:Heartbreak
ManaCost:R
Types:Tribal Instant Elemental
K:Cycling:2
K:Materialize:1

A:SP$ DealDamage | Cost$ R | ValidTgts$ Damageable | NumDmg$ 1 | SpellDescription$ CARDNAME deals 1 damage to any target.

Oracle:Heartbreak deals 1 damage to any target.\nMaterialize 1\nCycling {2}