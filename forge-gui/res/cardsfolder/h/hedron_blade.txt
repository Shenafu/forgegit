Name:Hedron Blade
ManaCost:1
Types:Artifact Equipment
S:Mode$ Continuous | Affected$ Creature.EquippedBy | AddPower$ 1 | AddToughness$ 1 | Description$ Equipped creature gets +1/+1.
T:Mode$ AttackerBlocked | ValidCard$ Card.EquippedBy | ValidBlocker$ Creature.Colorless | Execute$ TrigPump | TriggerDescription$ Whenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn.
SVar:TrigPump:DB$Pump | Defined$ TriggeredAttacker | KW$ Deathtouch
K:Equip:2
SVar:Picture:http://www.wizards.com/global/images/magic/general/hedron_blade.jpg
Oracle:Equipped creature gets +1/+1.\nWhenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn.\nEquip {2}
