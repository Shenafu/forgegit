Name:Horizon Scholar
ManaCost:5 U
Types:Creature Sphinx
PT:4/4
K:Flying
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 2.
SVar:TrigScry:DB$ Scry | ScryNum$ 2
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/horizon_scholar.jpg
Oracle:Flying\nWhen Horizon Scholar enters, scry 2.
