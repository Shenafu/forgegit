Name:Hall of Fame
ManaCost:2
Types:Artifact

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ FreeCard | TriggerDescription$ When CARDNAME enters, draw a card.

SVar:FreeCard:DB$ Draw | Defined$ You | NumCards$ 1

A:AB$ GainLife | Cost$ 1 Sac<1/CARDNAME> | ValidTgts$ Player | LifeAmount$ X | ConditionHonorTotal$ Targeted | ConditionHonorAmount$ GT0 | SubAbility$ TrigHonorSet | TgtPrompt$ Select a player | SpellDescription$ If target player's honor is greater than 0, their honor becomes 0 and they gain life equal to honor lost this way.

SVar:TrigHonorSet:DB$ SetHonor | Defined$ ParentTarget | HonorAmount$ 0 | ConditionHonorTotal$ Targeted | ConditionHonorAmount$ GT0

SVar:X:Count$TargetedHonorTotal

Oracle:When Hall of Fame enters, draw a card.\n{1}, Sacrifice Hall of Fame: If target player's honor is greater than 0, their honor becomes 0 and they gain life equal to honor lost this way.
