Name:Hostile Wurm
ManaCost:G G G 3
Types:Creature Wurm
PT:6/6
K:Trample

A:AB$ Pump | Cost$ G | Defined$ Self | KW$ Hexproof | SpellDescription$ CARDNAME gains hexproof until end of turn.

Oracle:Trample\n{G}: CARDNAME gains hexproof until end of turn.
