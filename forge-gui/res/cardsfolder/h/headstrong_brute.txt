Name:Headstrong Brute
ManaCost:2 R
Types:Creature Orc Pirate
PT:3/3
K:Defenseless
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Menace | CheckSVar$ X | SVarCompare$ GE1 | Description$ CARDNAME has menace while you control another Pirate.
SVar:X:Count$Valid Pirate.Other+YouCtrl
SVar:BuffedBy:Pirate
SVar:Picture:http://www.wizards.com/global/images/magic/general/headstrong_brute.jpg
Oracle:Defenseless\nHeadstrong Brute has menace while you control another Pirate.