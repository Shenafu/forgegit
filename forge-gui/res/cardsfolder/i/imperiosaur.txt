Name:Imperiosaur
ManaCost:2 G G
PT:5/5
Types:Creature Dinosaur
Text:Spend only mana produced by basic lands to cast CARDNAME.
A:SP$ PermanentCreature | Cost$ Mana<2 G G\Basic>
SVar:Picture:http://resources.wizards.com/magic/cards/fut/en-us/card130634.jpg
Oracle:Spend only mana produced by basic lands to cast Imperiosaur.
