Name:Incite
ManaCost:R
Types:Instant
A:SP$ Animate | Cost$ R | ValidTgts$ Creature | TgtPrompt$ Select target creature | Colors$ Red | OverwriteColors$ True | SubAbility$ DBPump | SpellDescription$ Target creature becomes red and gains berserk until end of turn.
SVar:DBPump:DB$ Pump | KW$ Berserk | Defined$ Targeted
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/incite.jpg
Oracle:Target creature becomes red and gains berserk until end of turn.
