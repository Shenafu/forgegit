Name:Induce Paranoia
ManaCost:2 U U
Types:Instant
A:SP$ Counter | Cost$ 2 U U | TargetType$ Spell | ValidTgts$ Card | RememberCounteredCMC$ True | SubAbility$ DBMill | SpellDescription$ Counter target spell. If {B} was spent to cast CARDNAME, that spell's controller mills X cards, where X is the spell's converted mana cost.
SVar:DBMill:DB$ Mill | NumCards$ X | Defined$ TargetedController | ConditionManaSpent$ B | References$ X | SubAbility$ DBCleanup
SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True
SVar:X:Count$RememberedNumber
SVar:ManaNeededToAvoidNegativeEffect:black
AI:RemoveDeck:Random
DeckNeeds:Color$Black
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/induce_paranoia.jpg
Oracle:Counter target spell. If {B} was spent to cast Induce Paranoia, that spell's controller mills X cards, where X is the spell's converted mana cost.
