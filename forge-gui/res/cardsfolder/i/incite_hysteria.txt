Name:Incite Hysteria
ManaCost:2 R
Types:Sorcery
A:SP$Pump | Cost$ 2 R | ValidTgts$ Creature | Radiance$ True | KW$ Defenseless | SpellDescription$ Radiance -- Until end of turn, target creature and each other creature that shares a color with it gain defenseless.
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/incite_hysteria.jpg
Oracle:Radiance -- Until end of turn, target creature and each other creature that shares a color with it gain defenseless.
