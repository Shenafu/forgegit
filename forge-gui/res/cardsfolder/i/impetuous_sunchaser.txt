Name:Impetuous Sunchaser
ManaCost:1 R
Types:Creature Human Soldier
PT:1/1
K:Flying
K:Haste
K:Berserk
SVar:Picture:http://www.wizards.com/global/images/magic/general/impetuous_sunchaser.jpg
Oracle:Flying, berserk, haste
