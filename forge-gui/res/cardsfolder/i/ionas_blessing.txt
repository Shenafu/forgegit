Name:Iona's Blessing
ManaCost:3 W
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 3 W | ValidTgts$ Creature | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Vigilance & Guardian:1 | Description$ Enchanted creature gets +2/+2, and has vigilance and guardian.
SVar:Picture:http://www.wizards.com/global/images/magic/general/ionas_blessing.jpg
Oracle:Enchant creature\nEnchanted creature gets +2/+2, and has vigilance and guardian.
