Name:Imps' Taunt
ManaCost:1 B
Types:Instant
K:Buyback:3
A:SP$ Pump | Cost$ 1 B | ValidTgts$ Creature | KW$ Berserk | TgtPrompt$ Select target creature | SpellDescription$ Target creature gains berserk until end of turn.
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/imps_taunt.jpg
Oracle:Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature gains berserk until end of turn.
