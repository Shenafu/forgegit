Name:Icy Blast
ManaCost:X U
Types:Instant
A:SP$ Tap | Cost$ X U | ValidTgts$ Creature | TargetMin$ 0 | TargetMax$ MaxTgts | References$ X,MaxTgts | SubAbility$ DBFreeze | SpellDescription$ Tap X target creatures. Ferocious -- If you control a creature with power 4 or greater, freeze those creatures instead.
SVar:DBFreeze:DB$ Freeze | Defined$ Targeted | ConditionPresent$ Creature.YouCtrl+powerGE4 | ConditionCompare$ GE1
SVar:X:Targeted$Amount
SVar:MaxTgts:Count$Valid Creature
SVar:Picture:http://www.wizards.com/global/images/magic/general/icy_blast.jpg
Oracle:Tap X target creatures.\nFerocious -- If you control a creature with power 4 or greater, freeze those creatures instead.
