Name:Imbibe Essence
ManaCost:GU U2
Types:Instant

A:SP$ Counter | Cost$ GU U2 | ValidTgts$ Card | TargetType$ Activated,Triggered | SubAbility$ DBDraw | SpellDescription$ Counter target activated or triggered ability. Draw a card.

SVar:DBDraw:DB$ Draw | NumCards$ 1

Oracle:Counter target activated or triggered ability.\nDraw a card.