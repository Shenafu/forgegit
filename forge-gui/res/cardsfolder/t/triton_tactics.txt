Name:Triton Tactics
ManaCost:U
Types:Instant
A:SP$ Pump | Cost$ U | TargetMin$ 0 | TargetMax$ 2 | NumDef$ +3 | ValidTgts$ Creature | TgtPrompt$ Select target Creature | SubAbility$ DBUntap | SpellDescription$ Up to two target creatures each get +0/+3 until end of turn. Untap those creatures. At this turn's next end of combat, freeze each creature that was blocked by one of those creatures this turn.
SVar:DBUntap:DB$ Untap | Defined$ ParentTarget | SubAbility$ DBDelTrig
SVar:DBDelTrig:DB$ DelayedTrigger | ThisTurn$ True | Mode$ Phase | Phase$ EndCombat | Execute$ TrigRemember | TriggerDescription$ At this turn's next end of combat, freeze each creature that was blocked by one of those creatures this turn. | RememberObjects$ ParentTarget
SVar:TrigRemember:DB$ Pump | RememberObjects$ DelayTriggerRemembered | SubAbility$ TrigFreeze
SVar:TrigFreeze:DB$ FreezeAll | ValidCards$ Creature.blockedByRemembered | SubAbility$ DBCleanup
SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/triton_tactics.jpg
Oracle:Up to two target creatures each get +0/+3 until end of turn. Untap those creatures. At this turn's next end of combat, freeze each creature that was blocked by one of those creatures this turn.
