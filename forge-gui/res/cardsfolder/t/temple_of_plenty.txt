Name:Temple of Plenty
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 1.
SVar:TrigScry:DB$ Scry | ScryNum$ 1
A:AB$ Mana | Cost$ T | Produced$ G | SpellDescription$ Add {G}.
A:AB$ Mana | Cost$ T | Produced$ W | SpellDescription$ Add {W}.
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/temple_of_plenty.jpg
Oracle:Temple of Plenty enters tapped.\nWhen Temple of Plenty enters, scry 1.\n{T}: Add {G} or {W}.
