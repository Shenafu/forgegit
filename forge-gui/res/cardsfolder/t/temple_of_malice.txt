Name:Temple of Malice
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 1.
SVar:TrigScry:DB$ Scry | ScryNum$ 1
A:AB$ Mana | Cost$ T | Produced$ B | SpellDescription$ Add {B}.
A:AB$ Mana | Cost$ T | Produced$ R | SpellDescription$ Add {R}.
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/temple_of_malice.jpg
Oracle:Temple of Malice enters tapped.\nWhen Temple of Malice enters, scry 1.\n{T}: Add {B} or {R}.
