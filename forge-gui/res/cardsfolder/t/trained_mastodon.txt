Name:Trained Mastodon
ManaCost:W 4
Types:Creature Elephant
PT:3/3
K:Vigilance

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigToken | TriggerDescription$ When CARDNAME enters, create a 1/1 white Citizen creature token.
SVar:TrigToken:DB$ Token | TokenAmount$ 1 | TokenName$ Citizen | TokenTypes$ Creature,Citizen | TokenOwner$ You | TokenColors$ White | TokenPower$ 1 | TokenToughness$ 1

Oracle:Vigilance\nWhen Trained Mastodon enters, create a 1/1 white Citizen creature token.
