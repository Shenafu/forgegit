Name:Thoughtweft Trio
ManaCost:2 W W
Types:Creature Kithkin Soldier
PT:5/5
K:First Strike
K:Vigilance
K:Champion:Kithkin
K:CARDNAME can block any number of creatures.
SVar:Picture:http://www.wizards.com/global/images/magic/general/thoughtweft_trio.jpg
Oracle:First strike, vigilance\nChampion a Kithkin (When this enters, sacrifice it unless you exile another Kithkin you control. When this leaves, that card returns to the field.)\nThoughtweft Trio can block any number of creatures.
