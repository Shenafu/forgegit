Name:Temple of Enlightenment
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 1.
SVar:TrigScry:DB$ Scry | ScryNum$ 1
A:AB$ Mana | Cost$ T | Produced$ W | SpellDescription$ Add {W}.
A:AB$ Mana | Cost$ T | Produced$ U | SpellDescription$ Add {U}.
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/temple_of_enlightenment.jpg
Oracle:Temple of Enlightenment enters tapped.\nWhen Temple of Enlightenment enters, scry 1.\n{T}: Add {W} or {U}.
