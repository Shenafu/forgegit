Name:Talisman Merchant
ManaCost:W 1
Types:Creature Dwarf Citizen
PT:1/3

A:AB$ Protection | Cost$ Sac<1/CARDNAME> | ValidTgts$ Creature.YouCtrl | TgtPrompt$ Select target creature you control | Gains$ Choice | Choices$ AnyColor | SpellDescription$ Target creature you control gains protection from the color of your choice until end of turn.

Oracle:Sacrifice Talisman Merchant: Target creature you control gains protection from the color of your choice until end of turn.
