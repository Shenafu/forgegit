Name:Thrill-Kill Assassin
ManaCost:1 B
Types:Creature Human Assassin
PT:1/2
K:Deathtouch
K:Unleash
SVar:Picture:http://www.wizards.com/global/images/magic/general/thrill_kill_assassin.jpg
Oracle:Deathtouch\nUnleash (You may have this creature enter with a +1/+1 counter on it. It can't block while it has a +1/+1 counter on it.)
