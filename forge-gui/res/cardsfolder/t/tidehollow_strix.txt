Name:Tidehollow Strix
ManaCost:U B
Types:Artifact Creature Bird
PT:2/1
K:Flying
K:Deathtouch
SVar:Picture:http://www.wizards.com/global/images/magic/general/tidehollow_strix.jpg
Oracle:Flying, deathtouch
