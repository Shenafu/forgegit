Name:Tormented Soul
ManaCost:B
Types:Creature Spirit
PT:1/1
K:Elude
K:Defenseless
SVar:Picture:http://www.wizards.com/global/images/magic/general/tormented_soul.jpg
Oracle:Elude, defenseless
