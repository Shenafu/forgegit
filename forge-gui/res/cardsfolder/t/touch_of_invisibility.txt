Name:Touch of Invisibility
ManaCost:3 U
Types:Sorcery
A:SP$ Pump | Cost$ 3 U | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Elude | SpellDescription$ Target creature can't be blocked this turn. | SubAbility$ DBDraw
SVar:DBDraw:DB$Draw | NumCards$ 1 | SpellDescription$ Draw a card.
SVar:Picture:http://www.wizards.com/global/images/magic/general/touch_of_invisibility.jpg
Oracle:Target creature gains elude until end of turn.\nDraw a card.
