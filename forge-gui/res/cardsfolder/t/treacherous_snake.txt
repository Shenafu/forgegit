Name:Treacherous Snake
ManaCost:1 B
Types:Creature Snake Soldier
PT:1/1
K:Deathtouch
K:Treacherous
Oracle:Deathtouch\nTreacherous (When this creature dies, target opponent loses 1 honor.)