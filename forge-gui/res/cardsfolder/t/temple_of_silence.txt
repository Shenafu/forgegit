Name:Temple of Silence
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 1.
SVar:TrigScry:DB$ Scry | ScryNum$ 1
A:AB$ Mana | Cost$ T | Produced$ W | SpellDescription$ Add {W}.
A:AB$ Mana | Cost$ T | Produced$ B | SpellDescription$ Add {B}.
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/temple_of_silence.jpg
Oracle:Temple of Silence enters tapped.\nWhen Temple of Silence enters, scry 1.\n{T}: Add {W} or {B}.
