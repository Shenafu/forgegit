Name:Tricks of the Trade
ManaCost:3 U
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 3 U | ValidTgts$ Creature | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddKeyword$ Elude | Description$ Enchanted creature gets +2/+0 and has elude.
SVar:Picture:http://www.wizards.com/global/images/magic/general/tricks_of_the_trade.jpg
Oracle:Enchant creature\nEnchanted creature gets +2/+0 and has elude.
