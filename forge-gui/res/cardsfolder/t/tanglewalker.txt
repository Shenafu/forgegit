Name:Tanglewalker
ManaCost:2 G
Types:Creature Dryad
PT:2/2
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddKeyword$ Elude | CheckSVar$ X | SVarCompare$ GE1 | Description$ Each creature you control has elude while defending player controls an artifact land.
SVar:X:Count$Valid Land.Artifact+YouDontCtrl
SVar:Picture:http://www.wizards.com/global/images/magic/general/tanglewalker.jpg
Oracle:Each creature you control has elude while defending player controls an artifact land.
