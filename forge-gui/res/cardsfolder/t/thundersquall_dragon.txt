Name:Thundersquall Dragon
ManaCost:R R 5
Types:Creature Dragon
PT:6/6
K:Flying

A:AB$ DealDamage | Cost$ G U R | NumDmg$ 3 | ValidTgts$ Creature | RememberObjects$ True | SubAbility$ DBEffect | SpellDescription$ CARDNAME deals 3 damage to target creature. When a creature dealt damage this way dies this turn, draw a card.

SVar:DBEffect:DB$ Effect | Triggers$ TrigDies | SVars$ DBDraw,ExileSelf | RememberObjects$ Targeted | SubAbility$ DBCleanup

SVar:TrigDies:Mode$ ChangesZone | ValidCard$ Creature.IsRemembered | Origin$ Battlefield | Destination$ Graveyard | Execute$ DBDraw | TriggerDescription$ When a creature dealt damage this way dies this turn, you draw a card.

SVar:DBDraw:DB$ Draw | NumCards$ 1 | Defined$ You | SubAbility$ ExileSelf

SVar:ExileSelf:DB$ ChangeZone | Origin$ Command | Destination$ Exile | Defined$ Self

SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True

Oracle:Flying\n{G}{U}{R}: Thundersquall Dragon deals 3 damage to target creature. When a creature dealt damage this way dies this turn, you draw a card.