Name:Traditional Law
ManaCost:W P2
Types:Instant

A:SP$ Counter | Cost$ W P2 | TargetType$ Activated,Triggered | TgtPrompt$ Select target activated or triggered ability | ValidTgts$ Card | UnlessCost$ P2 | SpellDescription$ Counter target activated or triggered ability unless its controller pays {P2}.

Oracle:Counter target activated or triggered ability unless its controller pays {P2}.