Name:Teroh's Vanguard
ManaCost:3 W
Types:Creature Human Nomad
PT:2/3
K:Flash
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | Threshold$ True | TriggerDescription$ Threshold -- While seven or more cards are in your graveyard, CARDNAME has "When CARDNAME enters, creatures you control gain protection from black until end of turn."
SVar:TrigPump:DB$ PumpAll | ValidCards$ Creature.YouCtrl | KW$ Protection from black
SVar:Picture:http://www.wizards.com/global/images/magic/general/terohs_vanguard.jpg
Oracle:Flash\nThreshold -- While seven or more cards are in your graveyard, Teroh's Vanguard has "When Teroh's Vanguard enters, creatures you control gain protection from black until end of turn."
