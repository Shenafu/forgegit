Name:Triad Initiate
ManaCost:B 1
Types:Creature Rat Rogue Master
PT:2/2
K:Extort

Oracle:Extort (Whenever you cast a spell, you may pay {WB}. If you do, each opponent loses 1 life and you gain that much life.)