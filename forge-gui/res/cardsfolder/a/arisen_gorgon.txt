Name:Arisen Gorgon
ManaCost:1 B B
Types:Creature Zombie Gorgon
PT:3/3
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Deathtouch | IsPresent$ Planeswalker.Liliana+YouCtrl | Description$ Arisen Gorgon has deathtouch while you control a Liliana planeswalker.
Oracle:Arisen Gorgon has deathtouch while you control a Liliana planeswalker.
