Name:Atroex, Temporal Watcher
ManaCost:4 U R
Types:Legendary Creature Elemental Warrior Wizard
PT:4/4
K:Flying
K:Prowess
T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ You | TriggerZones$ Battlefield | OptionalDecider$ You | CheckSVar$ CheckThisTurnCast | Execute$ TrigExile | TriggerDescription$ At the beginning of your end step, if you cast a noncreature spell this turn, you may exile the top card of your library. If it's a land card, take an extra turn after this one.
SVar:CheckThisTurnCast:Count$ThisTurnCast_Card.YouCtrl+nonCreature
SVar:TrigExile:DB$ Dig | DigNum$ 1 | Reveal$ True | DestinationZone$ Exile | RememberChanged$ True | SubAbility$ DBLand
SVar:DBLand:DB$ AddTurn | Defined$ You | ConditionDefined$ Remembered | ConditionPresent$ Land | NumTurns$ 1 | SubAbility$ DBCleanup
SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True
Oracle:Prowess\nAt the beginning of your end step, if you cast a noncreature spell this turn, you may exile the top card of your library. If it's a land card, take an extra turn after this one.
