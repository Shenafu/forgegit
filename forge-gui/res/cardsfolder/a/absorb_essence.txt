Name:Absorb Essence
ManaCost:U U2 1
Types:Instant

A:SP$ Counter | Cost$ U U2 1 | TargetType$ Spell | ValidTgts$ Card.nonCreature | TgtPrompt$ Select target nonCreature spell | SubAbility$ DBDraw | SpellDescription$ Counter target noncreature spell. Draw a card.

SVar:DBDraw:DB$ Draw | NumCards$ 1

Oracle:Counter target noncreature spell.\nDraw a card.
