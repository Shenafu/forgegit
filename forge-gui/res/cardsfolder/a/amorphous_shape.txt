Name:Amorphous Shape
ManaCost:G 2
Types:Enchantment Aura
K:Enchant creature

A:SP$ Attach | Cost$ 2 G | ValidTgts$ Creature | AILogic$ Pump

S:Mode$ Continuous | Affected$ Card.EnchantedBy | SetPower$ AffectedX | SetToughness$ AffectedX | Description$ Enchanted creature's base power and toughness are each equal to its converted mana cost.

SVar:AffectedX:Count$CardManaCost

Oracle:Enchant creature\nEnchanted creature's base power and toughness are each equal to its converted mana cost.
