Name:Aggressive Mammoth
ManaCost:3 G G G
Types:Creature Elephant
PT:8/8
K:Trample
S:Mode$ Continuous | Affected$ Creature.Other+YouCtrl | AddKeyword$ Trample | Description$ Other creatures you control have trample.
Oracle:Trample\nOther creatures you control have trample.