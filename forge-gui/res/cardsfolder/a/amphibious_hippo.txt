Name:Amphibious Hippo
ManaCost:2 G U
Types:Creature Hippo Mutant
PT:3/3
T:Mode$ LandPlayed | ValidCard$ Land.YouCtrl | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigTransform | TriggerDescription$ Whenever you play a land, you may transform CARDNAME.
SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform | AILogic$ Always
AlternateMode:DoubleFaced
Oracle:Whenever you play a land, you may transform Amphibious Hippo.

ALTERNATE

Name:Hungry Hippo
ManaCost:no cost
Colors:green,blue
Types:Creature Hippo Mutant
PT:4/4
T:Mode$ Transformed | ValidCard$ Card.Self | OptionalDecider$ You | Execute$ DBDraw | TriggerDescription$ Whenever this creature transforms into CARDNAME, you may draw a card.
SVar:DBDraw:DB$Draw | Defined$ You | NumCards$ 1
T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Opponent | TriggerZones$ Battlefield | MutantUntransformCondition$ True | Execute$ TrigTransform | TriggerDescription$ At the beginning of each opponent's end step, if that player didn't play a land, transform CARDNAME.

SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform
Oracle:Whenever this creature transforms into Hungry Hippo, you may draw a card.\nAt the beginning of each opponent's end step, if that player didn't play a land, transform Hungry Hippo.

