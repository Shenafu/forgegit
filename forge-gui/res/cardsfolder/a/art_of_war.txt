Name:Art of War
ManaCost:W
Types:Sorcery
K:Lore
A:SP$ Token | Cost$ W | TokenAmount$ 1 | TokenName$ Soldier | TokenTypes$ Creature,Soldier | TokenOwner$ You | TokenColors$ White | TokenPower$ 1 | TokenToughness$ 1 | TokenImage$ w 1 1 soldier M15 | SpellDescription$ Create a 1/1 white Soldier creature token.
Oracle:Create a 1/1 white Soldier creature token.\nLore (When you cast this, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)