Name:Azorius Justiciar
ManaCost:2 W W
Types:Creature Human Wizard
PT:2/2
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ Detain | TriggerDescription$ When CARDNAME enters, detain up to two target creatures your opponents control. (Until your next turn, those creatures can't attack or block and their activated abilities can't be activated.)
SVar:Detain:DB$ Pump | TargetMin$ 0 | TargetMax$ 2 | KW$ Defender & Defenseless & Disability | IsCurse$ True | UntilYourNextTurn$ True | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Select target creature your opponent controls to detain.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/azorius_justiciar.jpg
Oracle:When Azorius Justiciar enters, detain up to two target creatures your opponents control. (Until your next turn, those creatures can't attack or block and their activated abilities can't be activated.)
