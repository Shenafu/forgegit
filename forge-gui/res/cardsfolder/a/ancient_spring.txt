Name:Ancient Spring
ManaCost:no cost
Types:Land
K:CARDNAME enters tapped.
A:AB$ Mana | Cost$ T | Produced$ U | SpellDescription$ Add {U}.
A:AB$ Mana | Cost$ T Sac<1/CARDNAME> | Produced$ W B | SpellDescription$ Add {W}{B}.
SVar:Picture:http://www.wizards.com/global/images/magic/general/ancient_spring.jpg
Oracle:Ancient Spring enters tapped.\n{T}: Add {U}.\n{T}, Sacrifice Ancient Spring: Add {W}{B}.
