Name:Altar of Dementia
ManaCost:2
Types:Artifact
A:AB$ Mill | Cost$ Sac<1/Creature> | NumCards$ X | ValidTgts$ Player | TgtPrompt$ Choose a player | References$ X | SpellDescription$ Target player mills X cards, where X is equal to the sacrificed creature's power.
SVar:X:Sacrificed$CardPower
SVar:NonStackingEffect:True
AI:RemoveDeck:All
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/altar_of_dementia.jpg
Oracle:Sacrifice a creature: Target player mills X cards, where X is equal to the sacrificed creature's power.
