Name:Awaken the Inner Spirit
ManaCost:W X
Types:Tribal Sorcery Spirit

A:SP$ Token | Cost$ W X ExileFromGrave<X/Card> | Announce$ X | TokenScript$ w_1_1_spirit_flying | TokenAmount$ X | References$ X | SpellDescription$ Create X 1/1 white Spirit creature tokens with flying.

SVar:X:Count$xPaid/LimitMax.Y
SVar:Y:Count$CardsInGraveyard

Oracle:As as additional cost to cast Awaken the Inner Spirit, exile X cards from your graveyard.\nCreate X 1/1 white Spirit creature tokens with flying.
