Name:Avatar of Darkness
ManaCost:B B 3
Types:Enchantment Aura
K:Living Aura
K:Enchant creature

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 3 | AddToughness$ 3 | AddKeyword$ Intimidate & Protection:Card.White:Protection from white | Description$ Enchanted creature gets +3/+3 and has intimidate and protection from white.

Oracle:Living aura (You may play this spell without choosing a target. When this Aura enters, if it's unattached, create a 0/0 white Essence creature token and attach this to it.)\nEnchant creature\nEnchanted creature gets +3/+3 and has intimidate and protection from white.
