Name:Watertrap Weaver
ManaCost:2 U
Types:Creature Merfolk Wizard
PT:2/2
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigFreeze | TriggerDescription$ When CARDNAME enters, freeze target creature an opponent controls.
SVar:TrigFreeze:DB$ Freeze | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Choose target creature an opponent controls.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/watertrap_weaver.jpg
Oracle:When Watertrap Weaver enters, freeze target creature an opponent controls.
