Name:Wrath of Marit Lage
ManaCost:3 U U
Types:Enchantment
S:Mode$ Continuous | Affected$ Creature.Red | AddKeyword$ EternallyFrozen | Description$ Red creatures have eternally frozen.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigTapAll | TriggerDescription$ When CARDNAME enters, tap all red creatures.
SVar:TrigTapAll:DB$TapAll | ValidCards$ Creature.Red
SVar:NonStackingEffect:True
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/wrath_of_marit_lage.jpg
Oracle:When CARDNAME enters, tap all red creatures.\nRed creatures have eternally frozen.
