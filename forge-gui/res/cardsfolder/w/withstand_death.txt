Name:Withstand Death
ManaCost:G
Types:Instant
A:SP$ Pump | Cost$ G | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Indestructible | SpellDescription$ Target creature gains indestructible until end of turn.
SVar:Picture:http://www.wizards.com/global/images/magic/general/withstand_death.jpg
Oracle:Target creature gains indestructible until end of turn.
