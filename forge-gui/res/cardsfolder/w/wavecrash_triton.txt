Name:Wavecrash Triton
ManaCost:2 U
Types:Creature Merfolk Wizard
PT:1/4
T:Mode$ SpellCast | ValidActivatingPlayer$ You | TargetsValid$ Card.Self | TriggerZones$ Battlefield | Execute$ TrigFreeze | TriggerDescription$ Heroic -- Whenever you cast a spell that targets CARDNAME, freeze target creature an opponent controls.
SVar:TrigFreeze:DB$ Freeze | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Choose target creature an opponent controls.
SVar:Picture:http://www.wizards.com/global/images/magic/general/wavecrash_triton.jpg
Oracle:Heroic -- Whenever you cast a spell that targets CARDNAME, freeze target creature an opponent controls.
