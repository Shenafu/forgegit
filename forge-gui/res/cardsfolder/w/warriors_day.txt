Name:Warrior's Day
ManaCost:1 R R
Types:Sorcery
A:SP$ Token | Cost$ 1 R R | TokenAmount$ 2 | TokenName$ Warrior | TokenTypes$ Creature,Warrior | TokenOwner$ You | TokenColors$ Red | TokenPower$ 1 | TokenToughness$ 1 | SubAbility$ GainHaste | SpellDescription$ Create two 1/1 red Warrior creature tokens. Creatures you control gain haste until end of turn.
SVar:GainHaste:DB$ PumpAll | ValidCards$ Creature.YouCtrl | KW$ Haste
Oracle:Create two 1/1 red Warrior creature tokens. Creatures you control gain haste until end of turn.
