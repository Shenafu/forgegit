Name:Wall of Frost
ManaCost:1 U U
Types:Creature Wall
PT:0/7
K:Defender
T:Mode$ AttackerBlocked | ValidBlocker$ Card.Self | Execute$ TrigPump | TriggerDescription$ Whenever CARDNAME blocks a creature, that creature gains frozen.
SVar:TrigPump:DB$Pump | Defined$ TriggeredAttacker | KW$ Frozen | Permanent$ True
SVar:HasBlockEffect:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/wall_of_frost.jpg
Oracle:Defender\nWhenever CARDNAME blocks a creature, that creature gains frozen.
