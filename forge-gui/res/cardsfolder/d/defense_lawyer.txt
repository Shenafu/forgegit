Name:Defense Lawyer
ManaCost:W B
Types:Creature Human Advisor
PT:2/2

T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Player | TriggerZones$ Battlefield | Condition$ PlayerTurn | CheckSVar$ X | References$ X | Execute$ TrigDraw | TriggerDescription$ At the beginning of each end step, if you lost life this turn, you may draw a card.

SVar:TrigDraw:DB$ Draw | Defined$ You | NumCards$ 1

SVar:X:Count$LifeYouLostThisTurn

Oracle:At the beginning of each end step, if you lost life this turn, you may draw a card.
