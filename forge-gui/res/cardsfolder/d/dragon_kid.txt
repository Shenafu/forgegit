Name:Dragon Kid
ManaCost:2 R
Types:Creature Dragon Warrior
PT:1/1
K:Flying
A:AB$ Pump | Cost$ 1 R | NumAtt$ +2 | SpellDescription$ CARDNAME gets +2/+0 until end of turn.
Oracle:Flying\n{1}{R}: Dragon Kid gets +2/+0 until end of turn.