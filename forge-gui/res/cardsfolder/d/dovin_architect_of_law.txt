Name:Dovin, Architect of Law
ManaCost:4 W U
Types:Legendary Planeswalker Dovin
Loyalty:5
A:AB$ GainLife | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | Defined$ You | LifeAmount$ 2 | SubAbility$ DBDraw | SpellDescription$ You gain 2 life and draw a card.
SVar:DBDraw:DB$ Draw | Defined$ You | NumCards$ 1
A:AB$ Freeze | Cost$ SubCounter<1/LOYALTY> | ValidTgts$ Creature | Planeswalker$ True | SpellDescription$ Freeze target creature.
A:AB$ TapAll | Cost$ SubCounter<9/LOYALTY> | ValidTgts$ Opponent | TgtPrompt$ Select target opponent | ValidCards$ Permanent | Planeswalker$ True | Ultimate$ True | SubAbility$ NoUntap | SpellDescription$ Tap all permanents target opponent controls. That player skips their next untap step.
SVar:NoUntap:DB$ Pump | Defined$ TargetedPlayer | IsCurse$ True | KW$ Skip your next untap step. | Permanent$ True 
Oracle:[+1]: You gain 2 life and draw a card.\n[-1]: Freeze target creature.\n[-9]: Tap all permanents target opponent controls. That player skips their next untap step.
