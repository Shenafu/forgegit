Name:Decision Paralysis
ManaCost:3 U
Types:Instant
A:SP$ Freeze | Cost$ 3 U | TargetMin$ 0 | TargetMax$ 2 | ValidTgts$ Creature | SpellDescription$ Freeze up to two target creatures.
SVar:Picture:http://www.wizards.com/global/images/magic/general/decision_paralysis.jpg
Oracle:Freeze up to two target creatures.
