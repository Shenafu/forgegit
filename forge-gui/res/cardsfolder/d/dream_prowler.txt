Name:Dream Prowler
ManaCost:2 U U
Types:Creature Illusion
PT:1/5
S:Mode$ Continuous | Affected$ Card.Self+attacking | AddKeyword$ Elude | CheckSVar$ X | SVarCompare$ EQ1 | Description$ CARDNAME has elude while it's attacking alone.
SVar:X:Count$Valid Card.attacking
SVar:Picture:http://www.wizards.com/global/images/magic/general/dream_prowler.jpg
Oracle:Dream Prowler has elude while it's attacking alone.
