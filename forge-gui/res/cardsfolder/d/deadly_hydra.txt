Name:Deadly Hydra
ManaCost:G X
Types:Creature Hydra
PT:0/0
K:etbCounter:P1P1:X

SVar:X:Count$xPaid

T:Mode$ KeywordGained | ValidCard$ Card.Self | IsPresent$ Card.Self+withoutDeathtouch | NonValidKeyword$ Deathtouch | OncePerEffect$ True | Execute$ TrigPump | TriggerDescription$ When CARDNAME gains a keyword ability except deathtouch, if it doesn't have deathtouch, it gains deathtouch until end of turn.

SVar:TrigPump:DB$ Pump | KW$ Deathtouch | Defined$ Self

Oracle:CARDNAME enters with X +1/+1 counters on it.\nWhen CARDNAME gains a keyword ability except deathtouch, if it doesn't have deathtouch, it gains deathtouch until end of turn.