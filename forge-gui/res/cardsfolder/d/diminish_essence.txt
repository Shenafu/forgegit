Name:Diminish Essence
ManaCost:U U2 U2
Types:Instant

A:SP$ PumpAll | Cost$ U U2 U2 | ValidCards$ Creature.OppCtrl | KW$ Disempowered | SpellDescription$ Creatures your opponents control gain disempowered until end of turn. (Their base power is zero.)

Oracle:Creatures your opponents control gain disempowered until end of turn. (Their base power is zero.)
