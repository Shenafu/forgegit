Name:Develop Artifact
ManaCost:3
Types:Artifact
K:Materialize:1
K:Develop:4:3

A:AB$ Mana | Cost$ T | Produced$ Any | SpellDescription$ Add one mana of any color.

Oracle:{T}: Add one mana of any color.\nMaterialize 1\nDevelop 4--{3} ({3}, Discard this card: Dig %s cards for a card that shares a card type with this card, reveal it, put it into your hand, and tuck the rest randomly.)