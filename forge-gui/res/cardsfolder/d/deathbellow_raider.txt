Name:Deathbellow Raider
ManaCost:1 R
Types:Creature Minotaur Berserker
PT:2/3
K:Berserk
A:AB$ Regenerate | Cost$ 2 B | SpellDescription$ Regenerate CARDNAME.
SVar:Picture:http://www.wizards.com/global/images/magic/general/deathbellow_raider.jpg
Oracle:Berserk\n{2}{B}: Regenerate Deathbellow Raider.
