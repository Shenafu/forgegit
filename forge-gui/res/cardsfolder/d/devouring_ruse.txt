Name:Devouring Ruse
ManaCost:R 4
Types:Instant

A:SP$ Pump | Announce$ X | Cost$ R 4 Return<X/Creature> | ValidTgts$ Creature | NumAtt$ Z | References$ X,Y,Z | KW$ Trample | SpellDescription$ Target creature gets +2/+0 and gains trample until end of turn. It gets an additional +2/+0 for each creature returned to the hand this way.

SVar:X:Count$XChoice
SVar:Y:Number$2/Times.X
SVar:Z:Number$2/Plus.Y

Oracle:As as additional cost to cast this spell, you may return any number of creatures you control to their owners' hands.\nTarget creature gets +2/+0 and gains trample until end of turn. It gets an additional +2/+0 for each creature returned to the hand this way.
