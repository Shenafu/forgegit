Name:Develop Instant
ManaCost:U U 1
Types:Instant
K:Materialize:1
K:Develop:4:U U 1

A:SP$ Counter | Cost$ U U 1 | TargetType$ Spell | ValidTgts$ Card | SpellDescription$ Counter target spell.

Oracle:Counter target spell.\nMaterialize 1\nDevelop 4--{U}{U}{1} ({U}{U}{1}, Discard this card: Dig four cards for a card that shares a card type with this card, reveal it, put it into your hand, and tuck the rest randomly.)