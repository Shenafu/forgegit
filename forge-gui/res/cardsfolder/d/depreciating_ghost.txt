Name:Depreciating Ghost
ManaCost:B B P2
Types:Creature Spirit
PT:3/1
K:Flying

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigLoseLife | TriggerDescription$ When CARDNAME enters, you lose 2 life.

SVar:TrigLoseLife:DB$ LoseLife | LifeAmount$ 2

Oracle:Flying\nWhenever Depreciating Ghost enters, you lose 2 life.
