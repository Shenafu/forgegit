Name:Regenerate
ManaCost:1 G
Types:Instant
A:SP$ Regenerate | Cost$ 1 G | ValidTgts$ Creature | TgtPrompt$ Select target creature | SpellDescription$ Regenerate target creature.
SVar:Picture:http://www.wizards.com/global/images/magic/general/regenerate.jpg
Oracle:Regenerate target creature.