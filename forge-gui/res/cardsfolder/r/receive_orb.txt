Name:Receive Globe
ManaCost:0
Types:Instant

A:SP$ Token | Cost$ 0 | TokenScript$ c_globe | SpellDescription$ Create an Globe artifact token.

Oracle:Receive a Globe.
