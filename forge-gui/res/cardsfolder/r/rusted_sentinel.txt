Name:Rusted Sentinel
ManaCost:4
Types:Artifact Creature Golem
PT:3/4
K:CARDNAME enters tapped.
SVar:Picture:http://www.wizards.com/global/images/magic/general/rusted_sentinel.jpg
Oracle:Rusted Sentinel enters tapped.
