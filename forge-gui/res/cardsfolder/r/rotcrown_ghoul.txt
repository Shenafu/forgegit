Name:Rotcrown Ghoul
ManaCost:4 U
Types:Creature Zombie
PT:3/3
T:Mode$ ChangesZone | Origin$ Battlefield | Destination$ Graveyard | ValidCard$ Card.Self | Execute$ TrigMill | TriggerDescription$ When CARDNAME dies, target player mills five cards.
SVar:TrigMill:DB$ Mill | NumCards$ 5 | ValidTgts$ Player | TgtPrompt$ Select target player
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/rotcrown_ghoul.jpg
Oracle:When Rotcrown Ghoul dies, target player mills five cards.
