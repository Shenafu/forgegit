Name:Rat Infestation
ManaCost:1 B B
Types:Scene Tsinbork
Fate:2

A:Cost$ AddCounter<1/FATE> | Scene$ True | AB$ Token | TokenOwner$ You | TokenAmount$ 1 | TokenPower$ 1 | TokenToughness$ 1 | TokenColors$ Black | TokenName$ Rat | TokenTypes$ Rat,Creature | TokenImage$ b 1 1 rat CHK | SubAbility$ DBLoseLife | SpellDescription$ Create a 1/1 black Rat creature token.

SVar:DBLoseLife:DB$ LoseLife | Defined$ You | LifeAmount$ 1

A:Cost$ SubCounter<7/FATE> | Scene$ True | Climax$ True | AB$ Token | TokenOwner$ You | TokenAmount$ X | TokenPower$ 1 | TokenToughness$ 1 | TokenColors$ Black | TokenName$ Rat | TokenTypes$ Creature,Rat | TokenImage$ b 1 1 rat CHK | References$ X | SpellDescription$ Create X 1/1 black Rat creature tokens, where X is the number of Rats you control.

SVar:X:Count$TypeYouCtrl.Rat

DeckHas:Ability$Token | Type$Rat

Oracle:[+1]: Create a 1/1 black Rat creature token. You lose 1 life.\n[-7]: CLIMAX -- Create X 1/1 black Rat creature tokens, where X equals the number of Rats you control.
