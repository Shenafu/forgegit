Name:Relieved Ecstasy
ManaCost:W 2
Types:Enchantment
K:Flying

A:AB$ Animate | Cost$ 2 GainLife<2/Player.Opponent> | Defined$ Self | Power$ 6 | Toughness$ 5 | Types$ Creature,Enchantment,Angel | SpellDescription$ CARDNAME becomes also a 6/5 Angel creature until end of turn.

Oracle:Flying\n{2}, Have an opponent gain 2 life: CARDNAME becomes also a 6/5 Angel creature until end of turn.