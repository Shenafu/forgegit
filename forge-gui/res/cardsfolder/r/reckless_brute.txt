Name:Reckless Brute
ManaCost:2 R
Types:Creature Ogre Warrior
PT:3/1
K:Berserk
K:Haste
SVar:Picture:http://www.wizards.com/global/images/magic/general/reckless_brute.jpg
Oracle:Berserk, haste
