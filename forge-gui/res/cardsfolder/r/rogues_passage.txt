Name:Rogue's Passage
ManaCost:no cost
Types:Land
A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Add {C}.
A:AB$ Pump | Cost$ 4 T | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Elude | SpellDescription$ Target creature gains elude until end of turn.
SVar:Picture:http://www.wizards.com/global/images/magic/general/rogues_passage.jpg
Oracle:{T}: Add {C}.\n{4}, {T}: Target creature gains elude until end of turn.
