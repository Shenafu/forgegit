Name:Revealer of Dark Secrets
ManaCost:B 1
Types:Creature Human Warlock
PT:1/3
K:Inform

Oracle:Inform (When this creature attacks and isn't blocked, draw a card.)
