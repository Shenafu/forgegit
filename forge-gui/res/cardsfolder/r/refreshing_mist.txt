Name:Refreshing Mist
ManaCost:G
Types:Instant
K:Multikicker:1

A:SP$ Fog | Cost$ G | SubAbility$ DBGainLife | SpellDescription$ Prevent all combat damage that whould de dealt this turn. You gain 1 life for each time Refreshing Mist was kicked.

SVar:DBGainLife:DB$ GainLife | LifeAmount$ X | References$ X

SVar:X:Count$TimesKicked

Oracle:Multikicker {1}\nPrevent all combat damage that would be dealt this turn. You gain 1 life for each time Refreshing Mist was kicked.

