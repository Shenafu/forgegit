Name:Revenant Patriarch
ManaCost:4 B
Types:Creature Spirit
PT:4/3
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ RevenantPump | TriggerDescription$ When CARDNAME enters, if {W} was spent to cast it, target player skips their next combat phase.
SVar:RevenantPump:DB$Pump | ValidTgts$ Player | KW$ Skip your next combat phase. | Permanent$ True | TgtPrompt$ Select target Player | IsCurse$ True
K:Defenseless
SVar:ManaNeededToAvoidNegativeEffect:white
AI:RemoveDeck:Random
DeckNeeds:Color$White
SVar:Picture:http://www.wizards.com/global/images/magic/general/revenant_patriarch.jpg
Oracle:Defenseless\nWhen Revenant Patriarch enters, if {W} was spent to cast it, target player skips their next combat phase.
