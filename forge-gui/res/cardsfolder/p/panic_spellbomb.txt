Name:Panic Spellbomb
ManaCost:1
Types:Artifact
A:AB$ Pump | Cost$ T Sac<1/CARDNAME> | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Defenseless | IsCurse$ True | SpellDescription$ Target creature gains defenseless until end of turn.
T:Mode$ ChangesZone | Origin$ Battlefield | Destination$ Graveyard | ValidCard$ Card.Self | OptionalDecider$ TriggeredCardController | Execute$ TrigDraw | TriggerController$ TriggeredCardController | TriggerDescription$ When CARDNAME is put into a graveyard from the field, you may pay {R}. If you do, draw a card.
SVar:TrigDraw:AB$Draw | Cost$ R | NumCards$ 1
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/panic_spellbomb.jpg
Oracle:{T}, Sacrifice Panic Spellbomb: Target creature gains defenseless until end of turn.\nWhen Panic Spellbomb is put into a graveyard from the field, you may pay {R}. If you do, draw a card.
