Name:Pleasures from Treasures
ManaCost:R W 2
Types:Sorcery

A:SP$ DealDamage | Cost$ R W 2 | ValidTgts$ Damageable | TgtPrompt$ Select any target | NumDmg$ X | References$ X | SubAbility$ DBLife | SpellDescription$ Pleasures from Treasures deals X damage to any target. You gain X life. X is the number of artifacts you control.

SVar:DBLife:DB$ GainLife | LifeAmount$ X | References$ X

SVar:X:Count$Valid Artifact.YouCtrl

Oracle:Pleasures from Treasures deals X damage to any target. You gain X life. X is the number of artifacts you control.
