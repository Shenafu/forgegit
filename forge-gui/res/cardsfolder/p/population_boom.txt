Name:Population Boom
ManaCost:W 3
Types:Sorcery
A:SP$ Token | Cost$ W 3 | TokenAmount$ 3 | TokenName$ Citizen | TokenTypes$ Creature,Citizen | TokenOwner$ You | TokenColors$ White | TokenPower$ 1 | TokenToughness$ 1 | SpellDescription$ Create three 1/1 white Citizen creature tokens.

Oracle:Create three 1/1 white Citizen creature tokens.
