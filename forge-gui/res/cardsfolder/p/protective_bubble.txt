Name:Protective Bubble
ManaCost:3 U
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 3 U | ValidTgts$ Creature | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddKeyword$ Elude & Shroud | Description$ Enchanted creature gains elude and shroud until end of turn.
SVar:Picture:http://www.wizards.com/global/images/magic/general/protective_bubble.jpg
Oracle:Enchant creature\nEnchanted creature gains elude and shroud until end of turn.
