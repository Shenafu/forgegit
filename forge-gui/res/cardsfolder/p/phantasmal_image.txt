Name:Phantasmal Image
ManaCost:1 U
Types:Creature Illusion
PT:0/0
# Make Svars for granting abilities and triggers on clones distinct to avoid SVars getting overwritten when cloning a clone
K:ETBReplacement:Copy:DBCopy:Optional
SVar:DBCopy:DB$ Clone | Choices$ Creature.Other | AddTypes$ Illusion | AddKeywords$ Hexdoom | SpellDescription$ You may have CARDNAME enter as a copy of any creature on the field, except it's an Illusion in addition to its other types and it gains hexdoom.
SVar:Picture:http://www.wizards.com/global/images/magic/general/phantasmal_image.jpg
Oracle:You may have Phantasmal Image enter as a copy of any creature on the field, except it's an Illusion in addition to its other types and it gains hexdoom.
