Name:Prophet of the Peak
ManaCost:6
Types:Artifact Creature Cat
PT:5/5
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 2.
SVar:TrigScry:DB$ Scry | ScryNum$ 2
DeckHas:Ability$Scry
Oracle:When Prophet of the Peak enters, scry 2.
