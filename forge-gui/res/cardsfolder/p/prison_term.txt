Name:Prison Term
ManaCost:1 W W
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 1 W W | ValidTgts$ Creature | AILogic$ Curse
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddKeyword$ Defender & Defenseless & Disability | Description$ Enchanted creature has defender, defenseless, and disability.
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Creature.OppCtrl | TriggerZones$ Battlefield | Execute$ TrigAttach | OptionalDecider$ You | TriggerDescription$ Whenever a creature enters under an opponent's control, you may attach CARDNAME to that creature.
SVar:TrigAttach:DB$ Attach | Defined$ TriggeredCard
SVar:Picture:http://www.wizards.com/global/images/magic/general/prison_term.jpg
Oracle:Enchant creature\nEnchanted creature has defender, defenseless, and disability.\nWhenever a creature enters under an opponent's control, you may attach Prison Term to that creature.
