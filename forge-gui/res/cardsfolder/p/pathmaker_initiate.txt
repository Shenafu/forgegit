Name:Pathmaker Initiate
ManaCost:1 R
Types:Creature Human Wizard
PT:2/1
A:AB$ Pump | Cost$ T | ValidTgts$ Creature.powerLE2 | KW$ Elude | TgtPrompt$ Select target creature with power 2 or less. | SpellDescription$ Target creature with power 2 or less gains elude until end of turn.
SVar:Picture:http://www.wizards.com/global/images/magic/general/pathmaker_initiate.jpg
Oracle:{T}: Target creature with power 2 or less gains elude until end of turn.
