Name:Periodic Table
ManaCost:5
Types:Artifact

A:AB$ ChangeZone | Cost$ 4 T | Origin$ Library | Destination$ Hand | ChangeType$ Card.Elemental | ChangeNum$ 1

Oracle:{4}, {T}: Search your library for an Elemental card, reveal it, put it into your hand, then shuffle.