Name:Periodic Lizard
ManaCost:R 3
Types:Creature Lizard
PT:1/3
K:TypeCycling:Mountain:2
K:TypeBicycling:Mountain:5

A:AB$ Pump | Cost$ R | NumAtt$ +1 | SpellDescription$ CARDNAME gets +1/+0 until end of turn.

Oracle:{R}: Periodic Lizard gets +1/+0 until end of turn.\nMountaincycling {2}\nMountainbicycling {5}