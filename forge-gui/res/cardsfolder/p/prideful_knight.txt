Name:Prideful Knight
ManaCost:W W
Types:Creature Cat Knight
PT:2/1
K:Vigilance
K:Treacherous
Oracle:Vigilance\nTreacherous (When this creature dies, target opponent loses 1 honor.)
