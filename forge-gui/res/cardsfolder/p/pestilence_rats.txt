Name:Pestilence Rats
ManaCost:2 B
Types:Creature Rat
PT:*/3
S:Mode$ Continuous | EffectZone$ All | CharacteristicDefining$ True | SetPower$ X | References$ X | Description$ CARDNAME's power is equal to the number of other Rats on the field.
SVar:X:Count$Valid Rat.Other
SVar:BuffedBy:Rat
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/pestilence_rats.jpg
Oracle:Pestilence Rats's power is equal to the number of other Rats on the field. (For example, while there are two other Rats on the field, Pestilence Rats's power and toughness are 2/3.)
