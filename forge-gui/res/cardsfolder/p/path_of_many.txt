Name:Path of Many
ManaCost:G 1
Types:Tribal Enchantment Elemental Aura
K:Enchant creature
K:Cycling:2
K:Materialize:1

A:SP$ Attach | Cost$ G 1 | ValidTgts$ Creature | AILogic$ Pump

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddAbility$ ManaTap | Description$ Enchanted creature has "{T}: Add one mana of any color."

SVar:ManaTap:AB$ Mana | Cost$ T | Produced$ Any | SpellDescription$ Add one mana of any color.

Oracle:Enchant creature\nEnchanted creature has "{T}: Add one mana of any color."\nMaterialize 1\nCycling {2}