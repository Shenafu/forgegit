Name:Pet Guppy
ManaCost:1 U
Types:Creature Fish Mutant
PT:2/1
T:Mode$ LandPlayed | ValidCard$ Land.YouCtrl | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigTransform | TriggerDescription$ Whenever you play a land, you may transform CARDNAME.
SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform | AILogic$ Always
AlternateMode:DoubleFaced
Oracle:Whenever you play a land, you may transform Pet Guppy.

ALTERNATE

Name:Deadly Piranha
ManaCost:no cost
Colors:blue
Types:Creature Fish Beast Mutant
PT:3/2
K:Menace
T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Opponent | TriggerZones$ Battlefield | MutantUntransformCondition$ True | Execute$ TrigTransform | TriggerDescription$ At the beginning of each opponent's end step, if that player didn't play a land, transform CARDNAME.

SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform
Oracle:Menace\nAt the beginning of each opponent's end step, if that player didn't play a land, transform Deadly Piranha.

