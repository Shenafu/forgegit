Name:Pollenhost Leadwood
ManaCost:G G 3
Types:Creature Treefolk Warrior
PT:3/4

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | TriggerDescription$ When CARDNAME enters, creatures target player controls gain intercept until end of turn.

SVar:TrigPump:DB$ PumpAll | IsCurse$ True | ValidTgts$ Player | ValidCards$ Creature | KW$ Intercept

A:AB$ Pump | Cost$ G 1 | ValidTgts$ Creature | KW$ Absorb | SpellDescription$ Target creature gains absorb until end of turn.

Oracle:When Pollenhost Leadwood enters, creatures target player controls gain intercept until end of turn.\n{G}{1}: Target creature gains absorb until end of turn.