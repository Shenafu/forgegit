Name:Phyrexian Juggernaut
ManaCost:6
Types:Artifact Creature Juggernaut
PT:5/5
K:Infect
K:Berserk
SVar:Picture:http://www.wizards.com/global/images/magic/general/phyrexian_juggernaut.jpg
Oracle:Berserk\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)
