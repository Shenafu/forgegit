Name:Pentacast Hellion
ManaCost:R R 4
Types:Creature Hellion
PT:4/4

T:Mode$ SpellAbilityCast | ValidCard$ Card | ValidActivatingPlayer$ You | ConditionTotalManaSpent$ GE5 | TriggerZones$ Battlefield | Execute$ TrigDamage | TriggerDescription$ Whenever you spend five or more mana to cast a spell or activate an ability, CARDNAME deals 1 damage to target player.

SVar:TrigDamage:DB$ DealDamage | NumDmg$ 1 | ValidTgts$ Player

Oracle:Whenever you spend five or more mana to cast a spell or activate an ability, Pentacast Hellion deals 1 damage to target player.