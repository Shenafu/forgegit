Name:Penterra
ManaCost:no cost
Types:Land
K:TypeBicycling:Land:5

A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Add {C}.

Oracle:{T}: Add {C}.\nLandbicycling {5} ({5}, Discard this card: Search your library for two land cards, reveal them, put them into your hand, then shuffle.)