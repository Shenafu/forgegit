Name:Pinching Spider
ManaCost:G 2
Types:Creature Spider Mutant
PT:2/3

T:Mode$ LandPlayed | ValidCard$ Land.YouCtrl | TriggerZones$ Battlefield | OptionalDecider$ You | Execute$ TrigTransform | TriggerDescription$ Whenever you play a land, you may transform CARDNAME.
SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform | AILogic$ Always
AlternateMode:DoubleFaced

Oracle:Whenever you play a land, you may transform Pinching Spider.

ALTERNATE

Name:Charlotte's Arachnid
ManaCost:no cost
Colors:green
Types:Creature Spider Mutant
PT:4/5
K:Reach

T:Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Opponent | TriggerZones$ Battlefield | MutantUntransformCondition$ True | Execute$ TrigTransform | TriggerDescription$ At the beginning of each opponent's end step, if that player didn't play a land, transform CARDNAME.

SVar:TrigTransform:DB$ SetState | Defined$ Self | Mode$ Transform

Oracle:Reach\nAt the beginning of each opponent's end step, if that player didn't play a land, transform Charlotte's Arachnid.
