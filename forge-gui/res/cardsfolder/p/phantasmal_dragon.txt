Name:Phantasmal Dragon
ManaCost:2 U U
Types:Creature Dragon Illusion
PT:5/5
K:Flying
K:Hexdoom

SVar:Picture:http://www.wizards.com/global/images/magic/general/phantasmal_dragon.jpg
Oracle:Flying\nHexdoom (When Phantasmal Dragon becomes the target of a spell or ability, sacrifice it.)
