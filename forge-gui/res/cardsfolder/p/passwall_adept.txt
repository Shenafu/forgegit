Name:Passwall Adept
ManaCost:1 U
Types:Creature Human Wizard
PT:1/3
A:AB$ Pump | Cost$ 2 U | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Elude | SpellDescription$ Target creature gains elude until end of turn.
Oracle:{2}{U}: Target creature gains elude until end of turn.
