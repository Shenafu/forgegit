Name:Predict
ManaCost:1 U
Types:Instant
A:SP$ NameCard | Cost$ 1 U | Defined$ You | SubAbility$ DBMill | SpellDescription$ Choose a card name, then target player mills a card. If that card has the chosen name, you draw two cards. Otherwise, you draw a card.
SVar:DBMill:DB$ Mill | ValidTgts$ Player | TgtPrompt$ Select target player | NumCards$ 1 | RememberMilled$ True | SubAbility$ DBDraw
SVar:DBDraw:DB$ Draw | Defined$ You | NumCards$ X | References$ X | SubAbility$ DBDraw2
SVar:DBDraw2:DB$ Draw | Defined$ You | NumCards$ 1
SVar:X:Remembered$Valid Card.NamedCard
AI:RemoveDeck:All
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/predict.jpg
Oracle:Choose a card name, then target player mills a card. If that card has the chosen name, you draw two cards. Otherwise, you draw a card.
