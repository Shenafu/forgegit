Name:Prosecutor
ManaCost:W B 1
Types:Creature Human Advisor
PT:2/2

S:Mode$ CantAttackUnless | ValidCard$ Creature | Target$ You,Permanent.YouCtrl | Cost$ P2 | UnlessAI$ MorePowerful | Description$ Creatures can't attack you or a permanent you control unless their controller pays {2} or 2 life for each of those creatures.

Oracle:Creatures can't attack you or a permanent you control unless their controller pays {2} or 2 life for each of those creatures.