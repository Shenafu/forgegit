Name:Zhalfirin Void
ManaCost:no cost
Types:Land
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 1.
SVar:TrigScry:DB$ Scry | ScryNum$ 1
A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Add {C}.
DeckHas:Ability$Mana.Colorless|Scry
Oracle:When Zhalfirin Void enters, scry 1.\n{T}: Add {C}.
