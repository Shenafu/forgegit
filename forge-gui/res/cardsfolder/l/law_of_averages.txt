Name:Law of Averages
ManaCost:WB WB WB 2
Types:Enchantment

S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddPower$ 1 | AddToughness$ 1 | Description$ Creatures you control get +1/+1.

S:Mode$ Continuous | Affected$ Creature.OppCtrl | AddPower$ -1 | AddToughness$ -1 | Description$ Creatures your opponents control get -1/-1.

SVar:PlayMain1:TRUE

Oracle:Creatures you control get +1/+1.\nCreatures your opponents control get -1/-1.