Name:Lorthos, the Tidemaker
ManaCost:5 U U U
Types:Legendary Creature Octopus
PT:8/8
T:Mode$ Attacks | ValidCard$ Card.Self | Execute$ TrigFreeze | OptionalDecider$ You | TriggerDescription$ Whenever CARDNAME attacks, you may pay {8}. If you do, freeze up to eight target permanents.
SVar:TrigFreeze:AB$ Freeze | Cost$ 8 | ValidTgts$ Permanent | TargetMin$ 0 | TargetMax$ 8
SVar:Picture:http://www.wizards.com/global/images/magic/general/lorthos_the_tidemaker.jpg
Oracle:Whenever CARDNAME attacks, you may pay {8}. If you do, freeze up to eight target permanents.
