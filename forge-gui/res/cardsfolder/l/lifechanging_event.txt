Name:Lifechanging Event
ManaCost:W 1
Types:Instant
K:Lore

A:SP$ GainLife | Cost$ W 1 | ValidTgts$ Player | TgtPrompt$ Select target player (to gain 4 life) | LifeAmount$ 4 | SpellDescription$ Target player gains 4 life.

Oracle:Target player gains 4 life.\nLore (When you cast this spell, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)
