Name:Linvala, Keeper of Silence
ManaCost:2 W W
Types:Legendary Creature Angel
PT:3/4
K:Flying
S:Mode$ Continuous | Affected$ Creature.OppCtrl | AddKeyword$ Disability | Description$ Creatures your opponents control have disability.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/linvala_keeper_of_silence.jpg
Oracle:Flying\nCreatures your opponents control have disability.
