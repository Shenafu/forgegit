Name:Life of Glory
ManaCost:W W 3
Types:Enchantment Aura
K:Living Aura
K:Enchant creature

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddToughness$ 4 | AddKeyword$ Protection:Permanent.nonCreature:Protection from other noncreature permanents:Card.CardUID_HostCardUID | Description$ Enchanted creature gets +2/+4 and has protection from noncreature permanents except this Aura.

Oracle:Living aura (You may play this spell without choosing a target. When this Aura enters, if it's unattached, create a 0/0 white Essence creature token and attach this to it.)\nEnchant creature\nEnchanted creature gets +2/+4 and has protection from noncreature permanents except this Aura.
