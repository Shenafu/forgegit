Name:Leadwood Defender
ManaCost:G G 3
Types:Creature Treefolk Warrior
PT:3/4

T:Mode$ KeywordGained | ValidCard$ Card.Self | IsPresent$ Card.Self+withoutHexproof | NonValidKeyword$ Hexproof | OncePerEffect$ True | Execute$ TrigPump | TriggerDescription$ When CARDNAME gains a keyword ability except hexproof, if it doesn't have hexproof, it gains hexproof until end of turn.

SVar:TrigPump:DB$ Pump | KW$ Hexproof | Defined$ Self

Oracle:When CARDNAME gains a keyword ability except hexproof, if it doesn't have hexproof, it gains hexproof until end of turn.