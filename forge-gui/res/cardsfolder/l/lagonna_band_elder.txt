Name:Lagonna-Band Elder
ManaCost:2 W
Types:Creature Centaur Advisor
PT:3/2
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | IsPresent$ Enchantment.YouCtrl | Execute$ TrigGainLife | TriggerDescription$ When CARDNAME enters, if you control an enchantment, you gain 3 life.
SVar:TrigGainLife:DB$ GainLife | LifeAmount$ 3
SVar:Picture:http://www.wizards.com/global/images/magic/general/lagonna_band_elder.jpg
Oracle:When Lagonna-Band Elder enters, if you control an enchantment, you gain 3 life.
