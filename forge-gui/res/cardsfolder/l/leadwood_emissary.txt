Name:Leadwood Emissary
ManaCost:GU UR RG
Types:Creature Treefolk Druid
PT:3/3

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigMana | TriggerDescription$ When CARDNAME enters, add three mana in any combination of {G}, {U}, and/or {R}.

SVar:TrigMana:DB$ Mana | Produced$ Combo G U R | Amount$ 3

Oracle:When Leadwood Emissary enters, add three mana in any combination of {G}, {U}, and/or {R}.