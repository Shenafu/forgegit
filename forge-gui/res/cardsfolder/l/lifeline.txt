Name:Lifeline
ManaCost:5
Types:Artifact
T:Mode$ ChangesZone | ValidCard$ Card.Creature | Origin$ Battlefield | Destination$ Graveyard | CheckSVar$ LifelineX | Referneces$ LifelineX | Execute$ TrigLifelineDelay | TriggerZones$ Battlefield | TriggerDescription$ Whenever a creature dies, if another creature is on the field, return the first card to the field under its owner's control at the beginning of the next end step.
SVar:TrigLifelineDelay:DB$ DelayedTrigger | Mode$ Phase | Phase$ End of Turn | ValidPlayer$ Player | Execute$ TrigLifelineReturn | TriggerDescription$ Return that creature to the field. | RememberObjects$ TriggeredCard
SVar:TrigLifelineReturn:DB$ ChangeZone | Defined$ DelayTriggerRemembered | Origin$ Graveyard | Destination$ Battlefield
SVar:LifelineX:Count$Valid Creature
AI:RemoveDeck:Random
SVar:NonStackingEffect:True
SVar:Picture:http://www.wizards.com/global/images/magic/general/lifeline.jpg
Oracle:Whenever a creature dies, if another creature is on the field, return the first card to the field under its owner's control at the beginning of the next end step.
