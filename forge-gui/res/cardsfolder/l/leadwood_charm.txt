Name:Leadwood Charm
ManaCost:GU
Types:Instant Charm

A:SP$ Charm | Cost$ GU | Choices$ DBCounter,DBPump,DBTapUntap

SVar:DBCounter:DB$ PutCounter | ValidTgts$ Creature | CounterType$ P1P1 | CounterNum$ 1 | SpellDescription$ Target creature gets a +1/+1 counter.

SVar:DBPump:DB$ Pump | ValidTgts$ Creature | KW$ Hexproof | SpellDescription$ Target creature gains hexproof until end of turn.

SVar:DBTapUntap:DB$ TapOrUntap | ValidTgts$ Creature | SpellDescription$ Tap or untap target creature.

Oracle:Choose one of these--\n* Target creature gets a +1/+1 counter.\n* Target creature gains hexproof until end of turn.\n* Tap or untap target creature.