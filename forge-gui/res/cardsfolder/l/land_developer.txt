Name:Land Developer
ManaCost:G
Types:Creature Elf Spellshaper
PT:0/2

A:AB$ ChangeZone | Cost$ G T Discard<1/Card> | Origin$ Library | Destination$ Hand | ChangeType$ Land.Basic | ChangeNum$ 1 | SpellDescription$ Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle.

Oracle:{G}, {T}, Discard a card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle.
