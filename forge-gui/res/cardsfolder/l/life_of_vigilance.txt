Name:Life of Vigilance
ManaCost:W 2
Types:Enchantment Aura
K:Living Aura
K:Enchant creature

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Vigilance | Description$ Enchanted creature gets +2/+2 and has vigilance.

Oracle:Living aura (You may play this spell without choosing a target. When this Aura enters, if it's unattached, create a 0/0 white Essence creature token and attach this to it.)\nEnchant creature\nEnchanted creature gets +2/+2 and has vigilance.
