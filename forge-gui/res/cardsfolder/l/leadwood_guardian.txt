Name:Leadwood Guardian
ManaCost:G G 4
Types:Fast Creature Treefolk Shaman
PT:3/6
K:Hexproof

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | TriggerDescription$ When CARDNAME enters, target creature you control gets +3/+3 and gains hexproof until end of turn.

SVar:TrigPump:DB$ Pump | ValidTgts$ Creature.YouCtrl | NumAtt$ 3 | NumDef$ 3 | KW$ Hexproof

SVar:PlayMain1:TRUE

Oracle:Hexproof\nWhen Leadwood Guardian enters, target creature you control gets +3/+3 and gains hexproof until end of turn.