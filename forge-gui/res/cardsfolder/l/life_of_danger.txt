Name:Life of Danger
ManaCost:B B 1
Types:Enchantment Aura
K:Living Aura
K:Enchant creature

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 3 | AddToughness$ 1 | AddKeyword$ Defenseless | Description$ Enchanted creature gets +3/+1 and has defenseless.

Oracle:Living aura (You may play this spell without choosing a target. When this Aura enters, if it's unattached, create a 0/0 white Essence creature token and attach this to it.)\nEnchant creature\nEnchanted creature gets +3/+1 and has defenseless.
