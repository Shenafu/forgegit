Name:Locthwain Paladin
ManaCost:3 B
Types:Creature Human Knight
PT:3/2
K:Menace
K:etbCounter:P1P1:1:Adamant$ Black:Adamant -- If at least three black mana was spent to cast this spell, CARDNAME enters with a +1/+1 counter on it.
DeckHas:Ability$Counters
Oracle:Menace\nAdamant -- If at least three black mana was spent to cast this spell, Locthwain Paladin enters with a +1/+1 counter on it.
