Name:Caught in the Spider's Web
ManaCost:G G 1
Types:Enchantment Aura
K:Enchant creature

A:SP$ Attach | Cost$ G G 1 | ValidTgts$ Creature | AILogic$ KeepTapped

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddKeyword$ EternallyFrozen & Vulnerable | Description$ Enchanted creature has eternally frozen and vulnerable.

Oracle:Enchant creature\nEnchanted creature has eternally frozen and vulnerable. (It doesn't untap. When it's dealt damage, destroy it.)
