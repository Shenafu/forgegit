Name:Calcium
ManaCost:G G 2
Types:Creature Elemental
PT:4/4

A:AB$ Pump | Cost$ G 1 | ValidTgts$ Creature | TgtPrompt$ Select target creature | KW$ Absorb | SpellDescription$ Target creature gains absorb until end of turn.

A:AB$ Draw | Cost$ U 1 Sac<1/CARDNAME> | NumCards$ 2 | SpellDescription$ Draw two cards.

Oracle:{G}{1}: Target creature gains absorb until end of turn.\n{U}{1}, Sacrifice Calcium: Draw two cards.
