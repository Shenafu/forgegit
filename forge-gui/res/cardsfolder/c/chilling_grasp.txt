Name:Chilling Grasp
ManaCost:2 U
Types:Instant
K:Madness:3 U
A:SP$ Freeze | Cost$ 2 U | TargetMin$ 0 | TargetMax$ 2 | ValidTgts$ Creature | SpellDescription$ Freeze up to two target creatures.
DeckHints:Ability$Discard
SVar:Picture:http://www.wizards.com/global/images/magic/general/chilling_grasp.jpg
Oracle:Freeze up to two target creatures.\nMadness {3}{U} (If you discard this card, discard it into exile. When you do, cast it for its madness cost or put it into your graveyard.)
