Name:Censor Lifegain
ManaCost:W 4
Types:Instant

A:SP$ RepeatEach | Cost$ W 4 | RepeatSubAbility$ DBGainLife | RepeatPlayers$ NonOpponent | SpellDescription$ Each player except your opponents gains 7 life.

A:SP$ RepeatEach | Cost$ W 2 | RepeatSubAbility$ DBGainLife | RepeatPlayers$ Player | PrecostDesc$ Censor "except your opponents"-- | CostDesc$ {W}{2} | NonBasicSpell$ True | SpellDescription$ (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "except your opponents".) | StackDescription$ Each player gains 7 life.

SVar:DBGainLife:DB$ GainLife | LifeAmount$ 7 | Defined$ Remembered

Oracle:Each player except your opponents gains 7 life.\nCensor "except your opponents"--{W}{2} (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "except your opponents".)