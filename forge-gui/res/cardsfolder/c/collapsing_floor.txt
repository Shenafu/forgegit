Name:Collapsing Floor
ManaCost:2 R
Types:Sorcery Arcane

A:SP$ DamageAll | Cost$ 2 R | NumDmg$ 2 | ValidCards$ Creature.withoutFlying | ValidDescription$ each creature without flying. | SpellDescription$ CARDNAME deals 2 damage to each creature without flying.

Oracle:CARDNAME deals 2 damage to each creature without flying.
