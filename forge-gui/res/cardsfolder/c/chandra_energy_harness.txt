Name:Chandra, Energy Harness
ManaCost:R R 1
Types:Legendary Planeswalker Chandra
Loyalty:3

A:AB$ Token | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | TokenScript$ r_3_1_elemental | SpellDescription$ Create a 3/1 red Elemental creature token.

A:AB$ DealDamage | Cost$ SubCounter<1/LOYALTY> | Planeswalker$ True |  ValidTgts$ Creature.wasDealtDamageThisTurn | TgtPrompt$ Select target creature | NumDmg$ 3 | SpellDescription$ CARDNAME deals 3 damage to target creature that has been dealt damage this turn.

A:AB$ Sacrifice | Cost$ SubCounter<7/LOYALTY> | Planeswalker$ True |  Ultimate$ True | Defined$ You | Amount$ SacX | References$ SacX | SacValid$ Creature | SacMessage$ any number of creatures | RememberSacrificed$ True | Optional$ True | SubAbility$ UltimateDamage | SpellDescription$ Sacrifice any number of creatures. CARDNAME deals damage to target player equal to the total power of the sacrificed creatures.

SVar:UltimateDamage:DB$ DealDamage | ValidTgts$ Player | NumDmg$ SacPower | References$ SacPower

SVar:SacX:Count$Valid Creature.YouCtrl
SVar:SacPower:RememberedLKI$CardPower

Oracle:[+1]: Create a 3/1 red Elemental creature token.\n[-1]: Chandra, Energy Harness deals 3 damage to target creature that has been dealt damage this turn.\n[-7]: Sacrifice any number of creatures. Chandra deals damage to target player equal to the total power of the sacrificed creatures.
