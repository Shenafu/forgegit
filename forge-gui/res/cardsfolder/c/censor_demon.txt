Name:Censor Demon
ManaCost:B B 1
Types:Sorcery

A:SP$ Token | Cost$ B B 1 | TokenAmount$ 1 | TokenScript$ b_5_5_demon_hexdoom | SpellDescription$ Create a 5/5 black Demon creature token with hexdoom.

A:SP$ Token | Cost$ B B 4 | TokenAmount$ 1 | TokenScript$ b_5_5_demon | PrecostDesc$ Censor "with hexdoom"-- | CostDesc$ {B}{B}{4} | NonBasicSpell$ True | SpellDescription$ (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "with hexdoom".) | StackDescription$ Create a 5/5 black Demon creature token.

Oracle:Create a 5/5 black Demon creature token with hexdoom.\nCensor "with hexdoom"--{B}{B}{4} (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "with hexdoom".)