Name:Craven Giant
ManaCost:2 R
Types:Creature Giant
PT:4/1
K:Defenseless
SVar:Picture:http://resources.wizards.com/magic/cards/st/en-us/card5130.jpg
Oracle:Defenseless (can't block)
