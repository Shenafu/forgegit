Name:Counterburner
ManaCost:U U R 1
Types:Fast Creature Elemental
PT:3/1
K:Multikicker:R 1
K:etbCounter:P1P1:XKicked:no condition:CARDNAME enters with a +1/+1 counter on it for each time it was kicked.

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigCounter | TriggerDescription$ When CARDNAME enters, counter target spell.

SVar:TrigCounter:DB$ Counter | TargetType$ Spell | TgtPrompt$ Select target spell | ValidTgts$ Card

SVar:XKicked:Count$TimesKicked

Oracle:Multikicker {R}{1}\nCounterburner enters with a +1/+1 counter on it for each time it was kicked.\nWhen Counterburner enters, counter target spell.