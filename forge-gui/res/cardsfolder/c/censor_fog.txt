Name:Censor Fog
ManaCost:W 2
Types:Instant

A:SP$ Effect | Cost$ W 2 | Name$ Censor Fog Nonwhite | ReplacementEffects$ RPreventNonwhite | SpellDescription$ Prevent all damage that would be dealt by nonwhite creatures this turn.

SVar:RPreventNonwhite:Event$ DamageDone | Prevent$ True | ValidSource$ Creature.nonWhite | ActiveZones$ Command | Description$ Prevent all damage that would be dealt by nonwhite creatures this turn.

A:SP$ Effect | Cost$ W | Name$ Censor Fog All | ReplacementEffects$ RPreventAll | PrecostDesc$ Censor "nonwhite"-- | CostDesc$ {W} | NonBasicSpell$ True | SpellDescription$ (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "nonwhite".) | StackDescription$ Prevent all damage that would be dealt by creatures this turn.

SVar:RPreventAll:Event$ DamageDone | Prevent$ True | ValidSource$ Creature | ActiveZones$ Command | Description$ Prevent all damage that would be dealt by creatures this turn.

Oracle:Prevent all damage that would be dealt by nonwhite creatures this turn.\nCensor "nonwhite"--{W} (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "nonwhite".)