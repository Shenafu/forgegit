Name:Card Dealer
ManaCost:R 2
Types:Creature Dwarf Spellshaper
PT:2/1

A:AB$ Draw | Cost$ R 1 T Discard<2/Card> | NumCards$ 2 | SpellDescription$ Draw two cards.

Oracle:{R}{1}, {T}, Discard two cards: Draw two cards.
