Name:Censor Exile
ManaCost:W 1
Types:Instant

A:SP$ ChangeZone | Cost$ W 1 | ValidTgts$ Creature.attacking | TgtPrompt$ Select target attacking creature. | Origin$ Battlefield | Destination$ Exile | SpellDescription$ Exile target attacking creature.

A:SP$ ChangeZone | Cost$ W 3 | ValidTgts$ Creature | Origin$ Battlefield | Destination$ Exile | PrecostDesc$ Censor "attacking"-- | CostDesc$ {W}{3} | NonBasicSpell$ True | SpellDescription$ (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "attacking".) | StackDescription$ Exile target creature.

Oracle:Exile target attacking creature.\nCensor "attacking"--{W}{3} (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "attacking".)