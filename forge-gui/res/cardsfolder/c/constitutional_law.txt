Name:Constitutional Law
ManaCost:W W W 4
Types:Legendary Enchantment

S:Mode$ Continuous | Affected$ Permanent.nonLand+nonCreature+Other | SetPower$ AffectedX | SetToughness$ AffectedX | References$ AffectedX | AddType$ Creature | Description$ Each other nonland, noncreature permanent is also a creature with base power and toughness each equal to its converted mana cost.

SVar:AffectedX:Count$CardManaCost

SVar:NonStackingEffect:True

S:Mode$ Continuous | Affected$ Land | SetPower$ 2 | SetToughness$ 2 | AddType$ Creature | Description$ Each land is also a 2/2 creature.

Oracle:Each other nonland, noncreature permanent is also a creature with base power and toughness each equal to its converted mana cost.\nEach land is also a 2/2 creature.
