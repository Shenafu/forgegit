Name:Cryocoffin
ManaCost:U U 1
Types:Enchantment Aura
K:Enchant creature

A:SP$ Attach | Cost$ U U 1 | ValidTgts$ Creature | AILogic$ KeepTapped

S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddKeyword$ EternallyFrozen | Description$ Enchanted creature has eternally frozen.

T:Mode$ ChangesZone | ValidCard$ Card.Self | Origin$ Any | Destination$ Battlefield | Execute$ TrigTap | TriggerDescription$ When CARDNAME enters, tap enchanted creature.

SVar:TrigTap:DB$Tap | Defined$ Enchanted

Oracle:Enchant creature\nWhen enters, tap enchanted creature.\nEnchanted creature has eternally frozen. (It doesn't untap.)
