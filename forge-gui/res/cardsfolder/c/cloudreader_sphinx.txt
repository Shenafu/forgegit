Name:Cloudreader Sphinx
ManaCost:4 U
Types:Creature Sphinx
PT:3/4
K:Flying
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigScry | TriggerDescription$ When CARDNAME enters, scry 2.
SVar:TrigScry:DB$ Scry | ScryNum$ 2
DeckHas:Ability$Scry
Oracle:Flying\nWhen Cloudreader Sphinx enters, scry 2. 
