Name:Crazed Goblin
ManaCost:R
Types:Creature Goblin Warrior
PT:1/1
K:Berserk
SVar:Picture:http://www.wizards.com/global/images/magic/general/crazed_goblin.jpg
Oracle:Berserk (Attacks each combat if able)
