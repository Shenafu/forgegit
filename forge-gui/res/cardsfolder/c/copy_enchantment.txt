Name:Copy Enchantment
ManaCost:2 U
Types:Enchantment
K:ETBReplacement:Copy:DBCopy:Optional
SVar:DBCopy:DB$ Clone | Choices$ Enchantment.Other | SpellDescription$ You may have CARDNAME enter as a copy of any enchantment on the field.
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/copy_enchantment.jpg
Oracle:You may have Copy Enchantment enter as a copy of any enchantment on the field.
