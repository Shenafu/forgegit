Name:Consumption Charm
ManaCost:B
Types:Instant Charm

A:SP$ Charm | Cost$ B | Choices$ DBReturn,DBDiscard,DBPutCounter | CharmNum$ 1

SVar:DBReturn:DB$ ChangeZone | Origin$ Graveyard | Destination$ Hand | ValidTgts$ Creature.YouOwn | TgtPrompt$ Select target creature | SpellDescription$ Return target creature card from your graveyard to your hand.

SVar:DBDiscard:DB$ Discard | ValidTgts$ Player | TgtPrompt$ Choose a player | NumCards$ 1 | Mode$ TgtChoose | SpellDescription$ Target player discards a card.

SVar:DBPutCounter:DB$ PutCounter | ValidTgts$ Creature | TgtPrompt$ Select target creature | CounterType$ M1M1 | CounterNum$ 1 | IsCurse$ True | SpellDescription$ Target creature gets a -1/-1 counter.

Oracle:Choose one --\n• Return target creature card from your graveyard to your hand.\n• Target player discards a card.\n• Target creature gets a -1/-1 counter.
