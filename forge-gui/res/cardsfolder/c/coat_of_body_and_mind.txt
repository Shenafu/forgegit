Name:Coat of Body and Mind
ManaCost:G U 2
Types:Enchantment Aura
K:Enchant permanent

A:SP$ Attach | Cost$ G U 2 | ValidTgts$ Permanent

S:Mode$ Continuous | Affected$ Permanent.EnchantedBy | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Protection:Card.Green:Protection from green:Card.CardUID_HostCardUID & Protection:Card.Blue:Protection from blue:Card.CardUID_HostCardUID | Description$ Enchanted creature gets +2/+2 and has protection from green and from blue except from this card.

T:Mode$ Untaps | ValidCard$ Permanent.EnchantedBy | TriggerZones$ Battlefield | Execute$ DBToken | TriggerDescription$ When enchanted permanent becomes untapped, you may pay {G}. If you do, create a 2/2 green Wolf creature token.

SVar:DBToken:AB$ Token | Cost$ G | TokenScript$ g_2_2_wolf

T:Mode$ Untaps | ValidCard$ Permanent.EnchantedBy | TriggerZones$ Battlefield | Execute$ DBMill | TriggerDescription$ When enchanted permanent becomes untapped, you may pay {U}. If you do, target player mills ten cards.

SVar:DBMill:AB$ Mill | Cost$ U | ValidTgts$ Player | NumCards$ 10

DeckHas:Ability$Mill|Token

Oracle:Enchant permanent\nEnchanted permanent has +2/+2 and has protection from green and from blue except from this card.\nWhen enchanted permanent becomes untapped, you may pay {G}. If you do, create a 2/2 green Wolf creature token.\nWhen enchanted permanent becomes untapped, you may pay {U}. If you do, target player mills ten cards.
