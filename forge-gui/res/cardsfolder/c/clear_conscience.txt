Name:Clear Conscience
ManaCost:W W
Types:Instant

A:SP$ Debuff | Cost$ W W | ValidTgts$ Creature | RemoveAllKeywords$ True | SubAbility$ DBDepower | TgtPrompt$ Select target creature to lose all keywords | SpellDescription$ Target creature loses all keyword abilities until end of turn.

SVar:DBDepower:DB$ Animate | ValidTgts$ Creature | Power$ 0 | TgtPrompt$ Select target creature whose power becomes 0 | SpellDescription$ Target creature's base power becomes 0 until end of turn.

Oracle:Target creature loses all keyword abilities until end of turn.\nTarget creature's base power becomes 0 until end of turn.