Name:Censor Drain
ManaCost:W B
Types:Instant

A:SP$ DealDamage | Cost$ W B | ValidTgts$ Creature.nonArtifact | TgtPrompt$ Select target nonartifact creature | NumDmg$ 2 | SubAbility$ DBGainLife | SpellDescription$ CARDNAME deals 2 damage to target nonartifact creature. You gain 2 life.

A:SP$ DealDamage | Cost$ W B 2 | ValidTgts$ Creature | NumDmg$ 2 | SubAbility$ DBGainLife | PrecostDesc$ Censor "nonartifact"-- | CostDesc$ {W}{B}{2} | NonBasicSpell$ True | SpellDescription$ (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "nonartifact".) | StackDescription$ CARDNAME deals 2 damage to target creature. You gain 2 life.

SVar:DBGainLife:DB$ GainLife | Defined$ You | LifeAmount$ 2

Oracle:CARDNAME deals 2 damage to target nonartifact creature. You gain 2 life.\nCensor "nonartifact"--{W}{B}{2} (You may cast this spell for its censor cost. If you do, change its text by deleting all instances of "nonartifact".)