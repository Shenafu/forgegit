Name:Cudgel Troll
ManaCost:2 G G
Types:Creature Troll
PT:4/3
A:AB$ Regenerate | Cost$ G | SpellDescription$ Regenerate CARDNAME.
SVar:Picture:http://www.wizards.com/global/images/magic/general/cudgel_troll.jpg
Oracle:{G}: Regenerate Cudgel Troll.
