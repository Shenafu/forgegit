Name:Chilling Mystery
ManaCost:2 U
Types:Sorcery
K:Lore
A:SP$ ChangeZone | Cost$ 2 U | Origin$ Battlefield | Destination$ Hand | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | SpellDescription$ Return target permanent its owner's hand.
Oracle:Return target permanent to its owner's hand.\nLore (When you cast this, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)