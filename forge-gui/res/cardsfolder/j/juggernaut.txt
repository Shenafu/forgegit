Name:Juggernaut
ManaCost:4
Types:Artifact Creature Juggernaut
PT:5/3
K:Berserk
K:Elude:Creature.Wall:Wall
SVar:Picture:http://www.wizards.com/global/images/magic/general/juggernaut.jpg
Oracle:Berserk (Attacks each combat if able)\nEludes Walls
