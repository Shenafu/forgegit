Name:Jobbofel, Ginormous Blower
ManaCost:10 G G U U
Types:Legendary Creature Leviathan
PT:11/11
K:Hexproof
K:Trample
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigShuffle | TriggerDescription$ When CARDNAME enters, shuffle all permanents your opponents control into their owner's libraries. Then each player draws three cards.
SVar:TrigShuffle:DB$ ChangeZoneAll | ChangeType$ Permanent.OppCtrl | Origin$ Battlefield | Destination$ Library | Shuffle$ True | SubAbility$ TrigDraw
SVar:TrigDraw:DB$ Draw | Defined$ Player | NumCards$ 3
Oracle:Hexproof, trample\nWhen Jobbofel, Ginormous Blower enters, shuffle all permanents your opponents control into their owners' libraries. Then each player draws three cards.