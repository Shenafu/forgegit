Name:Jimsam Thaumaturgist
ManaCost:UR 1
Types:Creature Dwarf Spellshaper
PT:1/3

A:AB$ Pump | Cost$ UR Discard<1/Card> | Defined$ Self | KW$ HIDDEN CARDNAME's power and toughness are switched | SubAbility$ DBElude | SpellDescription$ Switch CARDNAME's power and toughness until end of turn. If the discarded card was an Elemental card, it gains elude until end of turn.

SVar:DBElude:DB$ Pump | Defined$ Self | KW$ Elude | ConditionCheckSVar$ X | ConditionSVarCompare$ GE1 | References$ X

SVar:X:Discarded$Valid Elemental

Oracle:{UR}, Discard a card: Switch Jimsam Thaumaturgist's power and toughness until end of turn. If the discarded card was an Elemental card, it gains elude until end of turn.