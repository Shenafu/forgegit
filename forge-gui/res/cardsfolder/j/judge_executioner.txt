Name:Judge
ManaCost:W 2
Types:Sorcery
K:Fuse

A:SP$ TapAll | Cost$ W 2 | ValidTgts$ Player | TgtPrompt$ Select target player | ValidCards$ Creature | SpellDescription$ Tap all creatures target player controls

Oracle:Tap all creatures target player controls.\nFuse (You may cast one or both halves of this card from your hand.)


AlternateMode: Split

ALTERNATE

Name:Executioner
ManaCost:B B 3
Types:Sorcery

A:SP$ DestroyAll | Cost$ B B 3 | ValidCards$ Creature.tapped | SpellDescription$ Destroy all tapped creatures.

Oracle:Destroy all tapped creatures.\nFuse (You may cast one or both halves of this card from your hand.)