Name:Juntu Stakes
ManaCost:2
Types:Artifact
S:Mode$ Continuous | Affected$ Creature.powerLE1 | AddKeyword$ EternallyFrozen | Description$ Creatures with power 1 or less have eternally frozen.
SVar:NonStackingEffect:True
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/juntu_stakes.jpg
Oracle:Creatures with power 1 or less have eternally frozen.
