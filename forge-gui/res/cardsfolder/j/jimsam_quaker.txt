Name:Jimsam Quaker
ManaCost:R R 4
Types:Creature Giant
PT:5/4
K:Haste

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigPump | TriggerDescription$ When CARDNAME enters, target creature gains defenseless until end of turn.

SVar:TrigPump:DB$ Pump | ValidTgts$ Creature | IsCurse$ True | KW$ Defenseless | TgtPrompt$ Select target creature.

SVar:PlayMain1:TRUE

Oracle:Haste\nWhen Jimsam Quaker enters, target creature gains defenseless until end of turn.
