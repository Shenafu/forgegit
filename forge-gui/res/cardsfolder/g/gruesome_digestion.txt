Name:Gruesome Digestion
ManaCost:B P2 X
Types:Tribal Sorcery Spirit

A:SP$ DealDamage | Cost$ B P2 X | XColor$ WB | ValidTgts$ Damageable | NumDmg$ X | References$ X | AILogic$ XLifeDrain | SubAbility$ DBGainLife | SpellDescription$ Spend only white and/or black mana on X. CARDNAME deals X damage to any target. You gain X life.

SVar:DBGainLife:DB$ GainLife | Defined$ You | LifeAmount$ X | References$ X

SVar:X:Count$xPaid

Oracle:Spend only white and/or black mana on X.\nGruesome Digestion deals X damage to any target. You gain X life.