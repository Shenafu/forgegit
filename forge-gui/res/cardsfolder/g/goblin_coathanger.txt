Name:Goblin Coathanger
ManaCost:R W
Types:Creature Goblin Soldier
PT:1/1
K:First strike
S:Mode$ ReduceCost | ValidTarget$ Card.Self | ValidCard$ Card | ValidSpell$ Activated.Equip | Activator$ You | Amount$ 1 | Description$ Equip abilities that target CARDNAME cost {1} less to activate.
Oracle:First strike\nEquip abilities that target Goblin Coathanger cost {1} less to activate.
