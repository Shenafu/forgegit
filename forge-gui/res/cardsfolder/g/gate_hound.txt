Name:Gate Hound
ManaCost:2 W
Types:Creature Hound
PT:1/1
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddKeyword$ Vigilance | CheckSVar$ X | SVarCompare$ EQ1 | Description$ Creatures you control have vigilance while CARDNAME is enchanted.
SVar:X:Count$Valid Card.Self+enchanted
SVar:EnchantMe:Once
SVar:Picture:http://www.wizards.com/global/images/magic/general/gate_hound.jpg
Oracle:Creatures you control have vigilance while Gate Hound is enchanted.
