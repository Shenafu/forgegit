Name:Gnoumi Pathmage
ManaCost:U 1
Types:Creature Gnome Rogue
PT:2/1

A:AB$ Pump | Cost$ U 4 | ValidTgts$ Creature | KW$ Elude | TgtPrompt$ Select target creature. | SpellDescription$ Target creature gains elude until end of turn.

Oracle:{U}{4}: Target creature gains elude until end of turn.
