Name:Goblin Balloon Brigade
ManaCost:R
Types:Creature Goblin Warrior
PT:1/1
A:AB$ Pump | Cost$ R | Defined$ Self | KW$ Flying | SpellDescription$ CARDNAME gains flying until end of turn.
SVar:Picture:http://resources.wizards.com/magic/cards/9ed/en-us/card84540.jpg
Oracle:{R}: Goblin Balloon Brigade gains flying until end of turn.
