Name:Grixis Grimblade
ManaCost:UR B
Types:Creature Zombie Warrior
PT:2/1
S:Mode$ Continuous | Affected$ Card.Self | AddPower$ 1 | AddToughness$ 1 | AddKeyword$ Deathtouch | CheckSVar$ X | SVarCompare$ GE1 | Description$ While you control another multicolored permanent, CARDNAME gets +1/+1 and has deathtouch.
SVar:X:Count$Valid Permanent.YouCtrl+MultiColor+Other
SVar:BuffedBy:Permanent.MultiColor
SVar:Picture:http://www.wizards.com/global/images/magic/general/grixis_grimblade.jpg
Oracle:While you control another multicolored permanent, Grixis Grimblade gets +1/+1 and has deathtouch.
