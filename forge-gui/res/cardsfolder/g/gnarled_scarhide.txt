Name:Gnarled Scarhide
ManaCost:B
Types:Enchantment Creature Minotaur
PT:2/1
K:Bestow:3 B
K:Defenseless
S:Mode$ Continuous | Affected$ Card.EnchantedBy | AddPower$ 2 | AddToughness$ 1 | AddKeyword$ Defenseless | Description$ Enchanted creature gets +2/+1 and has defenseless.
SVar:Picture:http://www.wizards.com/global/images/magic/general/gnarled_scarhide.jpg
Oracle:Bestow {3}{B} (If you cast this card for its bestow cost, it's an Aura spell with enchant creature. It becomes a creature again if it's not attached to a creature.)\nDefenseless\nEnchanted creature gets +2/+1 and has defenseless.
