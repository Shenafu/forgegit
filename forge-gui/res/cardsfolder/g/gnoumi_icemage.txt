Name:Gnoumi Icemage
ManaCost:G 1
Types:Creature Gnome Shaman
PT:1/3
K:Frostbite

A:AB$ Pump | Cost$ G | Defined$ Self | KW$ Absorb | SpellDescription$ CARDNAME gains absorb until end of turn.

Oracle:Frostbite\n{G}: CARDNAME gains absorb until end of turn.
