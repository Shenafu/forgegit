Name:Ghastly Deal
ManaCost:B B P2
Types:Tribal Sorcery Spirit

A:SP$ Draw | Cost$ B B P2 | NumCards$ 2 | SpellDescription$ Draw two cards.

Oracle:Draw two cards.