Name:Grip of the Roil
ManaCost:2 U
Types:Instant
K:Surge:1 U
A:SP$ Freeze | Cost$ 2 U | ValidTgts$ Creature | SubAbility$ DBDraw | SpellDescription$ Freeze target creature. Draw a card.
SVar:DBDraw:DB$ Draw | NumCards$ 1
SVar:Picture:http://www.wizards.com/global/images/magic/general/grip_of_the_roil.jpg
Oracle:Surge {1}{U} (You may cast this spell for its surge cost if you or a teammate has cast another spell this turn.)\nFreeze target creature. Draw a card.
