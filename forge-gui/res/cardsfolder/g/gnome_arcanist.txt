Name:Gnome Arcanist
ManaCost:1 U
Types:Creature Gnome Wizard
PT:2/2
K:Arcane teachings
DeckNeeds:Type$Arcane
Oracle:Arcane teachings (When this creature enters, you may exile target Arcane card in your graveyard taught to this creature. {Q}: Copy the taught card and cast the copy.)
