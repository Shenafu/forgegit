Name:Gudul Lurker
ManaCost:U
Types:Creature Salamander
PT:1/1
K:Elude
K:Megamorph:U
SVar:Picture:http://www.wizards.com/global/images/magic/general/gudul_lurker.jpg
Oracle:Elude\nMegamorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)
