Name:Goblin Shortcutter
ManaCost:1 R
Types:Creature Goblin Scout
PT:2/1
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigCanNotBlock | TriggerDescription$ When CARDNAME enters, target creature gains defenseless until end of turn.
SVar:TrigCanNotBlock:DB$Pump | ValidTgts$ Creature | KW$ Defenseless | TgtPrompt$ Select target creature. | IsCurse$ True | SpellDescription$ Target creature gains defenseless until end of turn.
SVar:PlayMain1:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/goblin_shortcutter.jpg
Oracle:When Goblin Shortcutter enters, target creature gains defenseless until end of turn.
