Name:Quantum Phantom
ManaCost:U2 R2
Types:Creature Illusion
PT:3/1

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigDraw | TriggerDescription$ When CARDNAME enters, draw a card, then discard a card.

SVar:TrigDraw:DB$ Draw | Defined$ You | NumCards$ 1 | SubAbility$ DBDiscard

SVar:DBDiscard:DB$Discard | Defined$ You | NumCards$ 1 | Mode$ TgtChoose

T:Mode$ BecomesTarget | ValidTarget$ Card.Self | TriggerZones$ Battlefield | Execute$ TrigChange | TriggerDescription$ When CARDNAME becomes the target of a spell or ability, return CARDNAME to its owner's hand.

SVar:TrigChange:DB$ChangeZone | Origin$ Battlefield | Destination$ Hand

Oracle:When Quantum Phantom enters, draw a card, then discard a card.\nWhen Quantum Phantom becomes the target of a spell or ability, return Quantum Phantom to its owner's hand.