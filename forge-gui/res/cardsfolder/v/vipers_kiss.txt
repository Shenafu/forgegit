Name:Viper's Kiss
ManaCost:B
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ B | ValidTgts$ Creature | AILogic$ Curse
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ -1 | AddToughness$ -1 | AddKeyword$ Disability | Description$ Enchanted creature gets -1/-1 and has disability.
SVar:Picture:http://www.wizards.com/global/images/magic/general/vipers_kiss.jpg
Oracle:Enchant creature\nEnchanted creature gets -1/-1 and has disability.
