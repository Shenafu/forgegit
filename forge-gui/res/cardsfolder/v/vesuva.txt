Name:Vesuva
ManaCost:no cost
Types:Land
K:ETBReplacement:Copy:DBCopy:Optional
SVar:DBCopy:DB$ Clone | Choices$ Land.Other | IntoPlayTapped$ True | SpellDescription$ You may have CARDNAME enter tapped as a copy of any land on the field.
SVar:NeedsToPlay:Land.YouDontCtrl+notnamedVesuva,Land.YouCtrl+nonLegendary+notnamedVesuva
SVar:Picture:http://www.wizards.com/global/images/magic/general/vesuva.jpg
Oracle:You may have Vesuva enter tapped as a copy of any land on the field.
