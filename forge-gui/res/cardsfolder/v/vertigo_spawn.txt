Name:Vertigo Spawn
ManaCost:1 U
Types:Creature Illusion
PT:0/3
K:Defender
T:Mode$ AttackerBlocked | ValidCard$ Creature | ValidBlocker$ Card.Self | Execute$ TrigFreeze | TriggerDescription$ Whenever CARDNAME blocks a creature, freeze that creature.
SVar:TrigFreeze:DB$ Freeze | Defined$ TriggeredAttacker
SVar:HasBlockEffect:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/vertigo_spawn.jpg
Oracle:Defender\nWhenever Vertigo Spawn blocks a creature, freeze that creature.
