Name:Vampire Interloper
ManaCost:1 B
Types:Creature Vampire Scout
PT:2/1
K:Flying
K:Defenseless
SVar:Picture:http://www.wizards.com/global/images/magic/general/vampire_interloper.jpg
Oracle:Flying, defenseless
