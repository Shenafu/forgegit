Name:Vibrating Sphere
ManaCost:4
Types:Artifact
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddPower$ 2 | Condition$ PlayerTurn | Description$ While it's your turn, creatures you control get +2/+0.
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddToughness$ -2 | Condition$ NotPlayerTurn | Description$ While it's not your turn, creatures you control get -0/-2.
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/vibrating_sphere.jpg
Oracle:While it's your turn, creatures you control get +2/+0.\nWhile it's not your turn, creatures you control get -0/-2.
