Name:Vampiric Embrace
ManaCost:2 B B
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 2 B B | ValidTgts$ Creature | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Flying & Predation:1 | Description$ Enchanted creature gets +2/+2 and has flying and predation 1.
SVar:Picture:http://www.wizards.com/global/images/magic/general/vampiric_embrace.jpg
Oracle:Enchant creature\nEnchanted creature gets +2/+2 and has flying and predation 1.