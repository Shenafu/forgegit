Name:Vapor Elemental
ManaCost:U U 1
Types:Creature Elemental
PT:0/1
K:Defender
K:Flying

R:Event$ DamageDone | Prevent$ True | ValidSource$ Creature | ValidTarget$ Card.Self | IsCombat$ True | Description$ Prevent all combat damage that would be dealt to CARDNAME.

Oracle:Defender, flying\nPrevent all combat damage that would be dealt to CARDNAME.