Name:Entangling Trap
ManaCost:1 W
Types:Enchantment
T:Mode$ Clashed | ValidPlayer$ You | Won$ True | TriggerZones$ Battlefield | Execute$ TrigTapW | TriggerDescription$ Whenever you clash, tap target creature an opponent controls. If you won, freeze that creature.
SVar:TrigTapW:DB$ Freeze | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Select target creature an opponent controls
T:Mode$ Clashed | ValidPlayer$ You | Won$ False | TriggerZones$ Battlefield | Execute$ TrigTapL | Secondary$ True | TriggerDescription$ Whenever you clash, tap target creature an opponent controls. If you won, freeze that creature.
SVar:TrigTapL:DB$ Tap | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Select target creature an opponent controls
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/entangling_trap.jpg
Oracle:Whenever you clash, tap target creature an opponent controls. If you won, freeze that creature.
