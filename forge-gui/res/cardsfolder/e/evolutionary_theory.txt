Name:Evolutionary Theory
ManaCost:G G 2 X X
Types:Sorcery
K:Multikicker:G 1

A:SP$ ChangeZone | Cost$ G G 2 X X | Origin$ Library | Destination$ Hand | ChangeType$ Creature | ChangeNum$ X | References$ X | SubAbility$ DBDeploy | SpellDescription$ Search your library for up to X creature cards, reveal them, put them into your hand, then shuffle your library. For each time Evolutionary Theory was kicked, you may put a creature card from your hand onto the field.

SVar:DBDeploy:DB$ ChangeZone | Origin$ Hand | Destination$ Battlefield | ChangeType$ Creature | ChangeNum$ XKicked | References$ XKicked | Optional$ You

SVar:X:Count$xPaid
SVar:XKicked:Count$TimesKicked

Oracle:Multikicker {G}{1}\nSearch your library for up to X creature cards, reveal them, put them into your hand, then shuffle your library. For each time Evolutionary Theory was kicked, you may put a creature card from your hand onto the field.