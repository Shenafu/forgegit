Name:Empower Essence
ManaCost:RG G2
Types:Sorcery

A:SP$ PutCounter | Cost$ RG G2 | ValidTgts$ Creature | CounterType$ P1P1 | CounterNum$ 2 | SpellDescription$ Target creature gets two +1/+1 counters.

Oracle:Target creature gets two +1/+1 counters.