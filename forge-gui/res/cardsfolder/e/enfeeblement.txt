Name:Enfeeblement
ManaCost:B B
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ B B | ValidTgts$ Creature | AILogic$ Curse
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ -2 | AddToughness$ -2 | Description$ Enchanted creature gets -2/-2.
SVar:Picture:http://www.wizards.com/global/images/magic/general/enfeeblement.jpg
Oracle:Enchant creature\nEnchanted creature gets -2/-2.
