Name:Ertai
ManaCost:no cost
Types:Vanguard
HandLifeModifier:-1/+4
S:Mode$ Continuous | EffectZone$ Command | Affected$ Creature.YouCtrl | AddKeyword$ Hexproof | Description$ Creatures you control have hexproof.
SVar:Picture:https://downloads.cardforge.org/images/cards/VAN/Ertai.full.jpg
Oracle:Hand -1, life +4\nCreatures you control have hexproof.
