Name:Ember Gale
ManaCost:3 R
Types:Sorcery

A:SP$ PumpAll | Cost$ 3 R | ValidCards$ Creature | ValidTgts$ Player | KW$ Defenseless | TgtPrompt$ Select target player | AILogic$ Evasion | IsCurse$ True | SubAbility$ DBDamage | SpellDescription$ Creatures target player controls gain defenseless until end of turn. CARDNAME deals 1 damage to each white and/or blue creature that player controls.

SVar:DBDamage:DB$ DamageAll | NumDmg$ 1 | ValidCards$ Creature.White+TargetedPlayerCtrl,Creature.Blue+TargetedPlayerCtrl | Defined$ ParentTarget | ValidDescription$ each creature that's white or blue that player controls.

AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/ember_gale.jpg
Oracle:Creatures target player controls gains defenseless until end of turn. Ember Gale deals 1 damage to each white and/or blue creature that player controls.
