Name:Epic Adventure
ManaCost:3 G
Types:Sorcery
K:Lore
A:SP$ ChangeZone | Cost$ 3 G | Origin$ Library | Destination$ Battlefield | ChangeType$ Land | ChangeNum$ 1 | SpellDescription$ Search your library for a land card, put it onto the field, then shuffle your library.
Oracle:Search your library for a land card, put it onto the field, then shuffle your library.\nLore (When you cast this, copy it for each card in all graveyards with the same name. You may choose new targets for the copies.)