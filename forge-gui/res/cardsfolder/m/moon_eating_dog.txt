Name:Moon-Eating Dog
ManaCost:3 U
Types:Creature Hound
PT:3/3
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Flying | IsPresent$ Planeswalker.Yanling+YouCtrl | Description$ While you control a Yanling planeswalker, CARDNAME has flying.
SVar:BuffedBy:Nissa
Oracle:While you control a Yanling planeswalker, Moon-Eating Dog has flying.
