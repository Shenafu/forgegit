Name:Marble Titan
ManaCost:3 W
Types:Creature Giant
PT:3/3
S:Mode$ Continuous | Affected$ Creature.powerGE3 | AddKeyword$ EternallyFrozen | Description$ Creatures with power 3 or greater have eternally frozen.
AI:RemoveDeck:Random
SVar:Picture:http://resources.wizards.com/magic/cards/9ed/en-us/card84533.jpg
Oracle:Creatures with power 3 or greater have eternally frozen.
