Name:Minamo Sightbender
ManaCost:1 U
Types:Creature Human Wizard
PT:1/2
A:AB$ Pump | Announce$ X | Cost$ X T | KW$ Elude | TgtPrompt$ Select target creature with power X or less | ValidTgts$ Creature.powerLEX | References$ X | SpellDescription$ Target creature with power X or less gains elude until end of turn.
SVar:X:Count$xPaid
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/minamo_sightbender.jpg
Oracle:{X}, {T}: Target creature with power X or less gains elude until end of turn.
