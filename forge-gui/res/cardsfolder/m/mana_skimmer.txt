Name:Mana Skimmer
ManaCost:3 B
Types:Creature Leech
PT:2/2
K:Flying
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Player | Execute$ FreezeTarget | TriggerZones$ Battlefield | TriggerDescription$ Whenever CARDNAME deals damage to a player, freeze target land that player controls.
SVar:FreezeTarget:DB$ Freeze | ValidTgts$ Land | TargetsWithDefinedController$ TriggeredTarget
SVar:Picture:http://www.wizards.com/global/images/magic/general/mana_skimmer.jpg
Oracle:Flying\nWhenever CARDNAME deals damage to a player, freeze target land that player controls.
