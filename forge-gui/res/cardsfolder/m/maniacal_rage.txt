Name:Maniacal Rage
ManaCost:1 R
Types:Enchantment Aura
K:Enchant creature
A:SP$ Attach | Cost$ 1 R | ValidTgts$ Creature | AILogic$ Pump
S:Mode$ Continuous | Affected$ Creature.EnchantedBy | AddPower$ 2 | AddToughness$ 2 | AddKeyword$ Defenseless | Description$ Enchanted creature gets +2/+2 and has defenseless.
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/maniacal_rage.jpg
Oracle:Enchant creature\nEnchanted creature gets +2/+2 and has defenseless.
