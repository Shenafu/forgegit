Name:Marang River Prowler
ManaCost:2 U
Types:Creature Human Rogue
PT:2/1
K:Elude
K:Defenseless
S:Mode$ Continuous | Affected$ Card.Self | EffectZone$ Graveyard | MayPlay$ True | IsPresent$ Permanent.Black+YouCtrl,Permanent.Green+YouCtrl | Description$ You may cast CARDNAME from you graveyard while you control a black or green permanent.
SVar:SacMe:3
SVar:DiscardMe:3
DeckHints:Color$Black|Green
SVar:Picture:http://www.wizards.com/global/images/magic/general/marang_river_prowler.jpg
Oracle:Elude, defenseless\nYou may cast Marang River Prowler from your graveyard while you control a black or green permanent.
