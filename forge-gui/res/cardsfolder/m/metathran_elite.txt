Name:Metathran Elite
ManaCost:1 U U
Types:Creature Metathran Soldier
PT:2/3
S:Mode$ Continuous | Affected$ Card.Self+enchanted | AddKeyword$ Elude | Description$ CARDNAME has elude while it's enchanted.
SVar:EnchantMe:Once
SVar:Picture:http://www.wizards.com/global/images/magic/general/metathran_elite.jpg
Oracle:Metathran Elite has elude while it's enchanted.
