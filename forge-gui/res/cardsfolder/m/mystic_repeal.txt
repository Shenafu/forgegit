Name:Mystic Repeal
ManaCost:G
Types:Instant
A:SP$ ChangeZone | Cost$ G | ValidTgts$ Enchantment | IsCurse$ True | TgtPrompt$ Select target enchantment | Origin$ Battlefield | Destination$ Library | LibraryPosition$ -1 | SpellDescription$ Tuck target enchantment.
Oracle:Tuck target enchantment.