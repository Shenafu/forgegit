Name:Molecular Theory
ManaCost:U 2
Types:Tribal Enchantment Elemental

A:AB$ Mill |  Cost$ Sac<1/Elemental> | ValidTgts$ Player | NumCards$ 3 | SpellDescription$ Target player mills three cards.

DeckHas:Ability$Mill

Oracle:Sacrifice an Elemental: Target player mills three cards.