Name:Mark for Death
ManaCost:3 R
Types:Sorcery
A:SP$ Pump | Cost$ 3 R | ValidTgts$ Creature.OppCtrl | TgtPrompt$ Select target creature an opponent controls | KW$ Intercept | IsCurse$ True | SubAbility$ DBUntap | SpellDescription$ Target creature an opponent controls gains intercept until end of turn. Untap that creature. Other creatures that player controls gain defenseless until end of turn.
SVar:DBUntap:DB$ Untap | Defined$ Targeted | SubAbility$ DBEffect
SVar:DBEffect:DB$ Effect | Name$ Mark for Death Effect | StaticAbilities$ KWPump | AILogic$ Evasion | RememberObjects$ TargetedController | ImprintCards$ Targeted
SVar:KWPump:Mode$ Continuous | EffectZone$ Command | AffectedZone$ Battlefield | Affected$ Creature.IsNotImprinted+RememberedPlayerCtrl | AddKeyword$ Defenseless | Description$ Other creatures that player controls gain defenseless until end of turn.
AI:RemoveDeck:All
SVar:Picture:http://www.wizards.com/global/images/magic/general/mark_for_death.jpg
Oracle:Target creature an opponent controls gains intercept until end of turn. Untap that creature. Other creatures that player controls gain defenseless until end of turn.
