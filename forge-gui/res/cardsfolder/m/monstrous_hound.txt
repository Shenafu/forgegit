Name:Monstrous Hound
ManaCost:3 R
Types:Creature Hound
PT:4/4
S:Mode$ Continuous | Affected$ Card.Self | AddKeyword$ Defender & Defenseless | CheckSVar$ X | SVarCompare$ LEY | References$ X,Y | Description$ CARDNAME has defender unless you control more lands than defending player. CARDNAME has defenseless unless you control more lands than attacking player.
SVar:X:Count$Valid Land.YouCtrl
SVar:Y:Count$Valid Land.YouDontCtrl
SVar:Picture:http://www.wizards.com/global/images/magic/general/monstrous_hound.jpg
Oracle:Monstrous Hound has defender unless you control more lands than defending player.\nMonstrous Hound has defenseless unless you control more lands than attacking player.
