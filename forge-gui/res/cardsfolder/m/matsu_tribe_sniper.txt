Name:Matsu-Tribe Sniper
ManaCost:1 G
Types:Creature Snake Warrior Archer
PT:1/1
A:AB$ DealDamage | Cost$ T | ValidTgts$ Creature.withFlying | TgtPrompt$ Select target creature with flying | NumDmg$ 1 | SpellDescription$ CARDNAME deals 1 damage to target creature with flying.
T:Mode$ DamageDone | ValidSource$ Card.Self | ValidTarget$ Creature | TriggerZones$ Battlefield | Execute$ TrigFreeze | TriggerDescription$ Whenever CARDNAME deals damage to a creature, freeze that creature.
SVar:TrigFreeze:DB$ Freeze | Defined$ TriggeredTarget
SVar:HasCombatEffect:TRUE
SVar:Picture:http://www.wizards.com/global/images/magic/general/matsu_tribe_sniper.jpg
Oracle:{T}: CARDNAME deals 1 damage to target creature with flying.\nWhenever CARDNAME deals damage to a creature, freeze that creature.
