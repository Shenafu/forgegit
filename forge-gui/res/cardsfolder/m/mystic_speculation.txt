Name:Mystic Speculation
ManaCost:U
Types:Sorcery
A:SP$ Scry | Cost$ U | ScryNum$ 3 | SpellDescription$ Scry 3.
K:Buyback:2
DeckHas:Ability$Scry
SVar:Picture:http://www.wizards.com/global/images/magic/general/mystic_speculation.jpg
Oracle:Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nScry 3.
