Name:Murmuring Bosk
ManaCost:no cost
Types:Land Forest
A:AB$ Mana | Cost$ T | Produced$ W | SubAbility$ DBPain | SpellDescription$ Add {W}. CARDNAME deals 1 damage to you.
A:AB$ Mana | Cost$ T | Produced$ B | SubAbility$ DBPain | SpellDescription$ Add {B}. CARDNAME deals 1 damage to you.
K:ETBReplacement:Other:DBTap
SVar:DBTap:DB$ Tap | ETB$ True | Defined$ Self | UnlessCost$ Reveal<1/Treefolk> | UnlessPayer$ You | StackDescription$ enters tapped. | SpellDescription$ As CARDNAME enters, you may reveal a Treefolk card from your hand. If you don't, CARDNAME enters tapped.
SVar:DBPain:DB$DealDamage | NumDmg$ 1 | Defined$ You
SVar:Picture:http://www.wizards.com/global/images/magic/general/murmuring_bosk.jpg
Oracle:({T}: Add {G}.)\nAs Murmuring Bosk enters, you may reveal a Treefolk card from your hand. If you don't, Murmuring Bosk enters tapped.\n{T}: Add {W} or {B}. Murmuring Bosk deals 1 damage to you.
