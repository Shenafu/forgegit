Name:Midnight Raid
ManaCost:3 B B
Types:Enchantment
S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddKeyword$ Retreat | Description$ Creatures you control have retreat.
T:Mode$ DamageDone | ValidSource$ Creature.YouCtrl | ValidTarget$ Player | CombatDamage$ True | TriggerZones$ Battlefield | Execute$ TrigDiscard | TriggerDescription$ Whenever a creature you control deals combat damage to a player, that player discards a card.
SVar:TrigDiscard:DB$ Discard | Defined$ TriggeredTarget | NumCards$ 1 | Mode$ TgtChoose
SVar:PlayMain1:TRUE
Oracle:Creatures you control have retreat.\nWhenever a creature you control deals combat damage to a player, that player discards a card.