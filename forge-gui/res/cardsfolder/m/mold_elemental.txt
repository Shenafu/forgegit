Name:Mold Elemental
ManaCost:G 1
Types:Creature Elemental
PT:1/1

A:AB$ Destroy | Cost$ G 2 T | ValidTgts$ Artifact,Enchantment | SpellDescription$ Destroy target artifact or enchantment.

A:AB$ Destroy | PrecostDesc$ Channel -- | Cost$ G G 4 Discard<1/CARDNAME> | ActivationZone$ Hand | ValidTgts$ Permanent.nonCreature | SpellDescription$ Destroy target noncreature permanent.

Oracle:{G}{2}, {T}: Destroy target artifact or enchantment.\nChannel -- {G}{G}{4}, Discard CARDNAME: Destroy target noncreature permanent.