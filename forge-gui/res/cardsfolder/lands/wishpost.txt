Name:Wishpost
ManaCost:no cost
Types:Land Locus
A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Produce {C}.
A:AB$ Scry | Cost$ 2 T | ScryNum$ X | References$ X | SpellDescription$ Scry X, where X equals the number of Locus you control.
SVar:X:Count$Valid Locus.YouCtrl
AI:RemoveDeck:Random
DeckHas:Ability$Scry
Oracle:{T}: Produce {C}.\n{2}, {T}: Scry X, where X equals the number of Locus you control.
