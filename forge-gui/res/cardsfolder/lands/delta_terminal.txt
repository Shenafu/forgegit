Name:Delta Terminal
ManaCost:no cost
Types:Land Terminus
A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Produce {C}.
A:AB$ Mana | Cost$ C C T | Produced$ C C C C C | SpellDescription$ Produce {C}{C}{C}{C}{C}.
AI:RemoveDeck:Random
Oracle:{T}: Produce {C}.\n{C}{C}, {T}: Produce {C}{C}{C}{C}{C}.
