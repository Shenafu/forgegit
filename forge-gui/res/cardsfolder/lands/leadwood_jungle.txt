Name:Leadwood Jungle
ManaCost:no cost
Types:Delayed Land Forest Situs
A:AB$ Mana | Cost$ F | Produced$ G U | IsPresent$ Land.YouCtrl+Island+Situs | PresentCompare$ GE1 | SpellDescription$ Produce {G}{U} if you control a Island Situs.
AI:RemoveDeck:Random
Oracle:{T}: Produce {G}.\n{F}: Produce {G}{U} if you control a Island Situs.
