Name:Notwen University
ManaCost:no cost
Types:Legendary Land

A:AB$ Mana | Cost$ T | Produced$ C | SpellDescription$ Add {C}.

A:AB$ ChangeZone | Cost$ G U R 2 T | Origin$ Sideboard | Destination$ Hand | ChangeType$ Card.YouOwn | ChangeNum$ 1 | SpellDescription$ You may choose a card you own from outside the game and put it into your hand.

Oracle:{T}: Add {C}.\n{G}{U}{R}{2}, {T}: You may choose a card you own from outside the game and put it into your hand.