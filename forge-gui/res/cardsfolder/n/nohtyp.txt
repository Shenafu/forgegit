Name:Nohtyp
ManaCost:G U 1
Types:Creature Snake Druid
PT:2/1
K:Flash

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigDraw | TriggerDescription$ When CARDNAME enters, draw a card.
SVar:TrigDraw:DB$ Draw | Defined$ You | NumCards$ 1

Oracle:Flash\nWhen Nohtyp enters, draw a card.
