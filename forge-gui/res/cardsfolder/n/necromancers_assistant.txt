Name:Necromancer's Assistant
ManaCost:2 B
Types:Creature Zombie
PT:3/1
T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Card.Self | Execute$ TrigMill | TriggerDescription$ When CARDNAME enters, you mill three cards.
SVar:TrigMill:DB$ Mill | NumCards$ 3 | Defined$ You
DeckHas:Ability$Mill
SVar:Picture:http://www.wizards.com/global/images/magic/general/necromancers_assistant.jpg
Oracle:When Necromancer's Assistant enters, you mill three cards.
