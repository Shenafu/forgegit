Name:Narnam Cobra
ManaCost:2
Types:Artifact Creature Snake
PT:2/1
A:AB$ Pump | Cost$ G | KW$ Deathtouch | Defined$ Self | SpellDescription$ CARDNAME gains deathtouch until end of turn.
AI:RemoveDeck:Random
SVar:Picture:http://www.wizards.com/global/images/magic/general/narnam_cobra.jpg
Oracle:{G}: Narnam Cobra gains deathtouch until end of turn.
