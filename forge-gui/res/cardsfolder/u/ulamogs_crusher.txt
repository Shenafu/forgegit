Name:Ulamog's Crusher
ManaCost:8
Types:Creature Eldrazi
PT:8/8
K:Berserk
K:Annihilator:2
SVar:Picture:http://www.wizards.com/global/images/magic/general/ulamogs_crusher.jpg
Oracle:Berserk\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)
