Name:Undercover Cop
ManaCost:W 2
Types:Creature Human Soldier
PT:2/3

A:AB$ Pump | Cost$ B 1 | NumAtt$ 1 | NumDef$ -1 | KW$ Intimidate | SubAbility$ DBColor | SpellDescription$ CARDNAME becomes black, gets +1/-1, and gains intimidate until end of turn.

SVar:DBColor:DB$ Animate | Colors$ Black | OverwriteColors$ True | Defined$ Self

Oracle:{B}{1}: CARDNAME becomes black, gets +1/-1, and gains intimidate until end of turn.