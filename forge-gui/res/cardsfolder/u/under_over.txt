Name:Under
ManaCost:B 2
Types:Sorcery
K:Fuse

A:SP$ ChooseCard | Cost$ B 2 | RememberChosen$ True | ValidTgts$ Player | IsCurse$ True | ChoiceZone$ Hand | Choices$ Card | TargetControls$ True | Amount$ 2 | SubAbility$ DBTuck | SelectPrompt$ Select two cards from your hand to tuck. | SpellDescription$ Target player tucks two cards from er hand.

SVar:DBTuck:DB$ ChangeZone | DefinedPlayer$ ParentTarget | Defined$ Remembered | Origin$ Hand | Destination$ Library | LibraryPosition$ -1 | Hidden$ True | IsCurse$ True | SubAbility$ DBCleanup

SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True

Oracle:Target player tucks two cards from er hand.\nFuse (You may cast one or both halves of this card from your hand.)


AlternateMode: Split

ALTERNATE

Name:Over
ManaCost:W 2
Types:Sorcery

A:SP$ Token | Cost$ W 2 | TokenScript$ w_1_1_spirit_flying | TokenAmount$ Y | References$ X,Y | SpellDescription$ For each card in your hand more than the player with the fewest cards, create a 1/1 white Spirit creature token with flying.

SVar:X:Count$InYourHand
SVar:Y:PlayerCountPlayers$LowestCardsInHand/NMinus.X

Oracle:For each card in your hand more than target player, create a 1/1 white Spirit creature token with flying.\nFuse (You may cast one or both halves of this card from your hand.)