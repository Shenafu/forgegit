[metadata]
Name=Zin-Scien
Code=ZSN
Code2=ZSN
Date=2020-10-29
Type=Expansion
BoosterCovers=3
Booster=1 RareMythic, 3 Uncommon, 8 Common, 1 Any LANDS
ChanceReplaceCommonWith=.125F Mythic

[cards]
M Barag, the Battle-Forged
M Gideon, General of Hope
M Gorkhhesh, Dragon of Wrath
M Kiora, Commander of the Deep
M Mind Elemental
M Repeating History
M Susyon of Summer Storms
M Tibalt, the Pain Dealer
R Angel Pathslicer
R Brick Battleland
R Copyspell
R Creation Myth
R Dragon Warlord
R Explorer's Outpost
R Frisky Crocodile
R Grey Battleland
R Groundbreaking News
R Hellhole
R Kor Engineer
R Liege's Crusade
R Mimicking Beast
R Mind Yeti
R Mobile Wall
R Our Life Will Never End
R Pink Battleland
R Planar Formation
R Pleasures from Treasures
R Set the Night on Fire
R Siege Dragon
R Slippery Snake
R Status Quo
R Story Elements
R Tossed by a Dwarf
R Turquoise Battleland
R Vainglorious Demon
R Walled Garden
U Amorphous Shape
U Asrok Octopus
U Blue Isle
U Chilling Mystery
U Crimson Knight
U Disturbing Rumors
U Dwarf Tosser
U Engulfing Cell
U Epic Adventure
U Eternal Witness
U Field Repair
U Food for Thought
U Goblin Commander
U Green Algae
U Green Grove
U Icefist Gnome
U Kor Weaponmaster
U Leap Frog
U Lifechanging Event
U Merciless Mercenary
U Metaphysicality
U Mimicking Chameleon
U Mimicry
U Mind Bogle
U Mind Island
U Nohtyp
U Ogre Conqueror
U Page Turner
U Pile of Dead Body
U Pinching Spider
U Preening Vampire
U Recruit Depot
U Reintegrate
U Resolve Conflict
U Siege Catapult
U Soaking Brainiac
U Spinning Dwarf
U Stampede Shout
U Transformative Dance
U Walking Fortress
U Wall of Absorption
U Wall of Explosives
U Wall of Ideas
U Weeping Willow
U Wisdom
C Adaptive Nature
C Amphibious Hippo
C Art of War
C Basic Plot
C Battle Rampart
C Blast Zone
C Bloodshed Vampire
C Boring Story
C Bullish Rage
C Buried Artifact
C Call to Service
C Dark Night
C Debrief
C Defame Rogue
C Defame Shaman
C Demon of Betrayal
C Deus Ex
C Dragon Kid
C Dwarven Ingenuity
C Elf Adventurer
C Fact Check
C Famous Dwarf
C Famous Vampire
C Fire Ceremony
C First Sergeant
C Flight of Fancy
C Flightless Bird
C Glorious Kithkin
C Goblin Coathanger
C Gorkhhesh's Threat
C Hall of Fame
C Hall of Shame
C Heart-Pumping Song
C Horror Story
C Human Student
C Inside Job
C Lame Ape
C Learning Curve
C Leather Saddle
C Legless Snake
C Marked by Dishonor
C Marked by Honor
C Memetic Elf
C Memetic Faerie
C Memetic Merfolk
C Memetic Treefolk
C Mimicking Parrot
C Mind Folk
C Nightwatch Bodyguard
C Nonflammable Wall
C Notorious Goblin
C Oceanic Serpent
C Ogre Student
C Oil-Vat Parapet
C Orcish Intruder
C Pass the Torch
C Pet Guppy
C Plot Armor
C Prideful Knight
C Primal Hydra
C Rat Student
C Re-educate
C Recycled Story
C Reinforced Shielding
C Royal Treatment
C Sad Story
C Shamanic Dance
C Silverstrike
C Skyhunter Cub
C Spiny Skin
C Spreading Seas
C Steelsilk Spider
C Supply Depot
C Sword and Board
C Treacherous Snake
C Valorous Angel
C Wall of Spears
C Warrior's Day
L Plains
L Island
L Swamp
L Mountain
L Forest
