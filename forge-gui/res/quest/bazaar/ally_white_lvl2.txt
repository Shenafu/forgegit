Name:Hung Daibo, Protective Knight
ManaCost:no cost
Colors:white
Types:Legendary Planeswalker Daibo
Loyalty:5

T:Mode$ Phase | Phase$ Upkeep | ValidPlayer$ You | TriggerZones$ Battlefield | IsPresent$ Knight.token+YouCtrl | PresentCompare$ EQ0 | Execute$ TrigToken | TriggerDescription$ At the beginning of your upkeep, if you control no Knight tokens, create a 2/2 white Knight creature token.

SVar:TrigToken:DB$Token | TokenAmount$ 1 | TokenScript$ w_2_2_knight

A:AB$ GainLife | Cost$ SubCounter<2/LOYALTY> | Planeswalker$ True | LifeAmount$ X | References$ X | SpellDescription$ You gain life equal to the greatest power among creatures you control.
SVar:X:Count$GreatestPower_Creature.YouCtrl

Oracle:At the beginning of your upkeep, if you control no Knight tokens, create a 2/2 white Knight creature token.\n[-2]: Target creature you control gets +2/+0 and gains lifelink until end of turn.
