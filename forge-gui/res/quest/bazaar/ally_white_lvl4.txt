Name:Hung Daibo, Concerted Effort
ManaCost:no cost
Colors:white
Types:Legendary Planeswalker Daibo
Loyalty:3

T:Mode$ AttackersDeclared | AttackedTarget$ You,Permanent.YouCtrl | TriggerZones$ Battlefield | Execute$ TrigToken | TriggerDescription$ Whenever a creature attacks you or a permanent you control, create a 0/1 white Citizen creature token.

SVar:TrigToken:DB$ Token | TokenAmount$ 1 | TokenScript$ w_0_1_citizen

A:AB$ Pump | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | NumDef$ +2 |  KW$ Stalwart & Lifelink | TargetMin$ 0 | TargetMax$ 1 | ValidTgts$ Creature | SpellDescription$ Up to one target creature gets +0/+2 and gains lifelink and stalwart until end of turn.

A:AB$ ChangeZone | Cost$ SubCounter<X/LOYALTY> | Planeswalker$ True | ValidTgts$ Permanent.nonLand | Origin$ Battlefield | Destination$ Exile | IsCurse$ True | TgtPrompt$ Choose target nonland permanent with converted mana cost X | References$ X | SpellDescription$ Exile target nonland permanent with converted mana cost X.

SVar:X:Targeted$CardManaCost

A:AB$ Effect | Cost$ SubCounter<13/LOYALTY> | Planeswalker$ True | Ultimate$ True | Name$ Emblem - Hung Daibo, Cleansing Light | StaticAbilities$ STDaibo | Stackable$ False | Duration$ Permanent | AILogic$ Always | SpellDescription$ You get an emblem with "Creatures you control have flying, first strike, guardian, indestructible, lifelink, retaliate, and vigilance."

SVar:STDaibo:Mode$ Continuous | EffectZone$ Command | Affected$ Creature.YouCtrl | AffectedZone$ Battlefield | AddKeyword$ Flying & First Strike & Guardian:1 & Indestructible & Lifelink & Retaliate & Vigilance

Oracle:Whenever one or more creatures attack you or a permanent you control, create a 0/1 white Citizen creature token.\n[+1]: Up to one target creature gets +0/+2 and gains lifelink and stalwart until end of turn.\n[-X]: Exile target nonland permanent with converted mana cost X.\n[-13]: ULTIMATE -- You get an emblem with "Creatures you control have flying, first strike, guardian, indestructible, lifelink, retaliate, and vigilance."
