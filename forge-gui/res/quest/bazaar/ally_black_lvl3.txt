Name:Torenei, Nightmare Distiller
ManaCost:no cost
Colors:black
Types:Legendary Planeswalker Torenei
Loyalty:3

T:Mode$ DamageDoneOnce | CombatDamage$ True | ValidSource$ Creature.YouCtrl | TriggerZones$ Battlefield | ValidTarget$ Player | Execute$ TrigDiscard | TriggerDescription$ Whenever one or more creatures you control deals combat damage to a player, that player discards a card.

SVar:TrigDiscard:DB$ Discard | Defined$ TriggeredTarget | NumCards$ 1 | Mode$ TgtChoose

A:AB$ ChangeZone | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | Origin$ Graveyard | Destination$ Exile | ValidTgts$ Card | TargetMin$ 0 | TargetMax$ 1 | TgtPrompt$ Select target card from a graveyard | RememberChanged$ True | SubAbility$ DBToken | SpellDescription$ Exile up to one target card from a graveyard. If a creature card was exiled this way, create a 2/2 black Zombie creature token.

SVar:DBToken:DB$ Token | TokenAmount$ 1 | TokenScript$ b_2_2_zombie | ConditionDefined$ Remembered | ConditionPresent$ Creature | ConditionCompare$ GE1 | SubAbility$ DBCleanup

SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True

A:AB$ Sacrifice | Cost$ SubCounter<7/LOYALTY> | Planeswalker$ True | Ultimate$ True | Amount$ 3 | Defined$ Player | SacValid$ Creature | RememberSacrificed$ True | SubAbility$ DBLifeGain | SpellDescription$ Each player sacrifices three creatures. You gain 1 life for each creature sacrificed this way.

SVar:DBLifeGain:DB$ GainLife | Defined$ You | LifeAmount$ X | References$ X | SubAbility$ DBCleanup

SVar:X:Remembered$Amount

Oracle:Whenever one or more creatures you control deals combat damage to a player, that player discards a card.\n[+1]: Exile up to one target card from a graveyard. If a creature card was exiled this way, create a 2/2 black Zombie creature token.\n[-7]: ULTIMATE -- Each player sacrifices three creatures. You gain 1 life for each creature sacrificed this way.