Name:YongNgaam, Diamondskin Terror
ManaCost:no cost
Colors:red
Types:Legendary Planeswalker YongNgaam
Loyalty:3

S:Mode$ Continuous | Affected$ Creature.YouCtrl | AddKeyword$ Haste | AddPower$ 1 | Description$ Creatures you control get +1/+0 and have haste.

A:AB$ Mill | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | Defined$ You | NumCards$ 1 | Destination$ Exile | RememberMilled$ True | SubAbility$ DBEffect | SpellDescription$ Imagine 1.

SVar:DBEffect:DB$ Effect | StaticAbilities$ STPlay | RememberObjects$ Remembered | ForgetOnMoved$ Exile | SubAbility$ DBCleanup

SVar:STPlay:Mode$ Continuous | EffectZone$ Command | AffectedZone$ Exile | Affected$ Card.IsRemembered | MayPlay$ True | Description$ You may play the card(s) this turn.

SVar:DBCleanup:DB$ Cleanup | ClearRemembered$ True

A:AB$ GainControl | Cost$ SubCounter<3/LOYALTY> | Planeswalker$ True | ValidTgts$ Creature | LoseControl$ EOT | Untap$ True | Unlag$ True | SubAbility$ DBPump | SpellDescription$ Gain control of target creature until end of turn. Untap and unlag it. It gains berserk.

SVar:DBPump:DB$ Pump | Defined$ Targeted | KW$ Berserk | Permanent$ True

A:AB$ DamageAll | Cost$ SubCounter<10/LOYALTY> | Planeswalker$ True | Ultimate$ True | ValidPlayers$ Player.Opponent | ValidCards$ Damageable.OppCtrl | NumDmg$ 9 | ValidDescription$ each opponent and each damageable they control. | SpellDescription$ CARDNAME deals 9 damage to each opponent and each damageable they control.

Oracle:Creatures you control get +1/+0 and have haste.\n[+1]: Imagine 1. (Exile the top card of your library. You may play it this turn.)\n[-3]: Gain control of target creature until end of turn. Untap and unlag it. It gains berserk.\n[-10]: ULTIMATE -- CARDNAME deals 9 damage to each opponent and each damageable they control.
