Name:Lurarhi
ManaCost:no cost
Colors:blue
Types:Legendary Planeswalker Lurarhi
Loyalty:2

A:AB$ Draw | Cost$ SubCounter<1/LOYALTY> | Planeswalker$ True | NumCards$ 1 | SpellDescription$ Draw a card.

Oracle:[-1]: Draw a card.
