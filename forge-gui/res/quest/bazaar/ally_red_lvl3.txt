Name:YongNgaam, Lavatastic Heat
ManaCost:no cost
Colors:red
Types:Legendary Planeswalker YongNgaam
Loyalty:3

S:Mode$ Continuous | AddKeyword$ Virulent & Overflow | Affected$ Instant.YouCtrl,Sorcery.YouCtrl | AffectedZone$ Stack | Description$ Instant and sorcery spells you cast have overflow and virulent (their damage overflows to the target's controller and can't be prevented).

A:AB$ Pump | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | NumAtt$ +1 | KW$ First Strike | TargetMin$ 0 | TargetMax$ 1 | ValidTgts$ Creature | SpellDescription$ Up to one target creature gets +1/+0 and gains first strike until end of turn.

A:AB$ GainControl | Cost$ SubCounter<9/LOYALTY> | Planeswalker$ True | Ultimate$ True | ValidTgts$ Creature | TargetMin$ 0 | TargetMax$ 2 | LoseControl$ EOT | Untap$ True | Unlag$ True | SpellDescription$ Gain control of up to two target creatures until end of turn. Untap and unlag them.

Oracle:Instant and sorcery spells you cast have overflow and virulent (their damage overflows to the target's controller and can't be prevented).\n[+1]: Up to one target creature gets +1/+0 and gains first strike until end of turn.\n[-9]: ULTIMATE -- Gain control of up to two target creatures until end of turn. Untap and unlag them.
