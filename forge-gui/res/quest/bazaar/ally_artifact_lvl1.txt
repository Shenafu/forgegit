Name:Myriam
ManaCost:no cost
Colors:colorless
Types:Legendary Planeswalker Myriam
Loyalty:2

A:AB$ Token | Cost$ SubCounter<1/LOYALTY> | Planeswalker$ True | TokenAmount$ 1 | TokenScript$ c_1_1_a_thopter_flying | SpellDescription$ Create a colorless 1/1 Thopter artifact creature token with flying.

Oracle:[-1]: Create a colorless 1/1 Thopter artifact creature token with flying.
