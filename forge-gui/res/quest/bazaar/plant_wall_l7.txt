Name:Plant Wall
ManaCost:no cost
Colors:green
Types:Creature Plant Wall
PT:1/4
K:Defender
K:Wither
A:AB$ GainLife | Cost$ T | LifeAmount$ 1 | SpellDescription$ You gain 1 life.
A:AB$ Mana | Cost$ AddCounter<1/M0M1> | Produced$ Any | ActivationLimit$ 1 | SpellDescription$ Add one mana of any color. Activate this ability only once each turn.
Oracle: Defender, Wither\n{T}: You gain 1 life.\nPlant Wall gets a -0/-1 counter, {T}: Add one mana of any color. Activate this ability only once each turn.