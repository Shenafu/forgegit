Name:Myriam, Thopteraviary
ManaCost:no cost
Colors:colorless
Types:Legendary Planeswalker Myriam
Loyalty:5

T:Mode$ ChangesZone | Origin$ Any | Destination$ Battlefield | ValidCard$ Artifact.nonToken | Execute$ TrigToken | TriggerDescription$ Whenever an artifact enters, you create a colorless 1/1 Thopter artifact creature token with flying.

SVar:TrigToken:DB$ Token | TokenAmount$ 1 | TokenScript$ c_1_1_a_thopter_flying

A:AB$ Mana | Cost$ SubCounter<2/LOYALTY> | Planeswalker$ True | Produced$ C | Amount$ 2 | RestrictValid$ Artifact | SpellDescription$ Add {C}{C}. Spend this mana only to cast artifact spells.

Oracle:Whenever a nontoken artifact enters, you create a colorless 1/1 Thopter artifact creature token with flying.\n[-2]: Add {C}{C}. Spend this mana only to cast artifact spells.
