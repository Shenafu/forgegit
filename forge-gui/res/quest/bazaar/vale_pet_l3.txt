Name:Lotus Vale
ManaCost:no cost
Types:Delayed Land
A:AB$ Mana | Cost$ T | Produced$ Any | Amount$ 3 | SpellDescription$ Add three mana of any one color.
SVar:Picture:http://www.wizards.com/global/images/magic/general/lotus_vale.jpg
Oracle:{T}: Add three mana of any one color.
