Name:Hung Daibo, the Greatest Love of Wall
ManaCost:no cost
Colors:white
Types:Legendary Planeswalker Daibo
Loyalty:3

T:Mode$ ChangesZone | TriggerZones$ Battlefield | ValidCard$ Creature | Origin$ Any | Destination$ Battlefield | Execute$ TrigGainLife | TriggerDescription$ Whenever a creature enters, you gain 1 life.

SVar:TrigGainLife:DB$ GainLife | Defined$ You | LifeAmount$ 1

A:AB$ Token | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | TokenAmount$ 1 | TokenScript$ w_0_3_wall_defender | SpellDescription$ Create a 0/3 white Wall creature token with defender.

A:AB$ PumpAll | Cost$ SubCounter<10/LOYALTY> | Planeswalker$ True | Ultimate$ True | ValidCards$ Creature.YouCtrl | NumAtt$ X | References$ X | KW$ HIDDEN CARDNAME can attack as though it didn't have defender. | SpellDescription$ Until end of turn, creatures you control get +X/+0 and can attack as though they didn't have defender, where X is your life total.

SVar:X:Count$YourLifeTotal

Oracle:Whenever a creature enters, you gain 1 life.\n[+1]: Create a 0/3 white Wall creature token with defender.\n[-10]: ULTIMATE -- Until end of turn, creatures you control get +X/+0 and can attack as though they didn't have defender, where X is your life total.
