Name:Myriam, Hi-Tech Foundry
ManaCost:no cost
Colors:colorless
Types:Legendary Planeswalker Myriam
Loyalty:3

T:Mode$ Phase | Phase$ Upkeep | ValidPlayer$ You | Execute$ TrigChangeZone | OptionalDecider$ You | TriggerZones$ Battlefield | TriggerDescription$ At the beginning of your upkeep, you may return target artifact card from your graveyard to your hand.

SVar:TrigChangeZone:DB$ChangeZone | Origin$ Graveyard | Destination$ Hand | TgtPrompt$ Choose target artifact card in your graveyard | ValidTgts$ Artifact.YouCtrl

A:AB$ Animate | Cost$ AddCounter<1/LOYALTY> | Planeswalker$ True | TargetMin$ 0 | TargetMax$ 1 | ValidTgts$ Artifact.nonCreature | TgtPrompt$ Select target noncreature artifact | Power$ X | Toughness$ X | Types$ Artifact,Creature | References$ X | UntilYourNextTurn$ True | AILogic$ PTByCMC | SpellDescription$ Until your next turn, up to one target noncreature artifact becomes an artifact creature with power and toughness equal to its converted mana cost.

SVar:X:Targeted$CardManaCost

A:AB$ Mana | Cost$ SubCounter<2/LOYALTY> | Planeswalker$ True | Produced$ C | Amount$ 3 | RestrictValid$ Card.Artifact,Activated.Artifact | SpellDescription$ Add {C}{C}{C}. Spend this mana only to cast artifact spells or activate abilities of artifacts.

A:AB$ Effect | Cost$ SubCounter<11/LOYALTY> | Planeswalker$ True | Ultimate$ True | Name$ Emblem - Myriam, Hi-Tech Foundry | StaticAbilities$ STPump | Duration$ Permanent | AILogic$ Always | SubAbility$ DBToken | SpellDescription$ You get emblem with "Artifact creatures you control get +2/+2". Create seven colorless 1/1 Thopter artifact creature tokens with flying.

SVar:STPump:Mode$ Continuous | EffectZone$ Command | Affected$ Creature.Artifact+YouCtrl | AffectedZone$ Battlefield | AddPower$ 2 | AddToughness$ 2 | Description$ Artifact creatures you control get +2/+2.

SVar:DBToken:DB$ Token | TokenAmount$ 7 | TokenScript$ c_1_1_a_thopter_flying

Oracle:At the beginning of your upkeep, you may return target artifact card from your graveyard to your hand.\n[+1]: Until your next turn, up to one target noncreature artifact becomes also a creature with power and toughness equal to its converted mana cost.\n[-2]: Add {C}{C}{C}. Spend this mana only to cast artifact spells or activate abilities of artifacts.\n[-11]: ULTIMATE -- You get emblem with "Artifact creatures you control get +2/+2." Create seven colorless 1/1 Thopter artifact creature token with flying.
