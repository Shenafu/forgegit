Name:Spark Elemental
ManaCost:no cost
Types:Creature Elemental
Colors:red
PT:3/1
K:Trample
K:Haste
K:Demise

SVar:EndOfTurnLeavePlay:True

Oracle:Trample, haste\nDemise (Sacrifice it at the beginning of the end step.)
