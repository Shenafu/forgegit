Name:Elemental
ManaCost:no cost
Types:Creature Elemental
Colors:white,blue,black,red,green
PT:1/1
A:AB$ Mana | Cost$ Sac<1/CARDNAME> | Produced$ Any | Amount$ 1 | SpellDescription$ Add one mana of any color.
Oracle:Sacrifice this creature: Add one mana of any color.
