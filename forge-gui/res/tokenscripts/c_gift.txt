Name:Gift
ManaCost:no cost
Types:Artifact Gift

A:AB$ Charm | Cost$ 2 Tap Sac<1/CARDNAME> | SorcerySpeed$ True | Choices$ DBP1P1,DBCharge,DBLoyalty,DBFate

SVar:DBP1P1:DB$ PutCounter | CounterType$ P1P1 | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | SpellDescription$ Target permanent gets a +1/+1 counter.

SVar:DBCharge:DB$ PutCounter | CounterType$ CHARGE | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | SpellDescription$ Target permanent gets a charge counter.

SVar:DBLoyalty:DB$ PutCounter | CounterType$ LOYALTY | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | SpellDescription$ Target permanent gets a loyalty counter.

SVar:DBFate:DB$ PutCounter | CounterType$ FATE | ValidTgts$ Permanent | TgtPrompt$ Select target permanent | SpellDescription$ Target permanent gets a fate counter.

Oracle:Sorcerous, {2}, {T}, Sacrifice this artifact: Target permanent gets a +1/+1, charge, loyalty, or fate counter.
