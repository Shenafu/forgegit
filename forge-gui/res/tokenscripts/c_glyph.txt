Name:Glyph
ManaCost:no cost
Types:Artifact Glyph

A:AB$ Draw | Cost$ T Sac<1/CARDNAME> | NumCards$ 1 | SubAbility$ DBDiscard | SpellDescription$ Draw a card, then discard a card.

SVar:DBDiscard:DB$Discard | Defined$ You | NumCards$ 1 | Mode$ TgtChoose

Oracle:{T}, Sacrifice CARDNAME: Draw a card, then discard a card.
