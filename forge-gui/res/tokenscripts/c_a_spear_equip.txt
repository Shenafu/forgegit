Name:Spear
ManaCost:no cost
Types:Artifact Equipment Spear
K:Equip:1

S:Mode$ Continuous | Affected$ Creature.EquippedBy | AddKeyword$ Haste & First Strike | Description$ Equipped creature has haste and first strike.

Oracle:Equipped creature has haste and first strike.\nEquip {1}
