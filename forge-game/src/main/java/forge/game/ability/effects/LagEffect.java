package forge.game.ability.effects;

import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.TargetRestrictions;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class LagEffect extends SpellAbilityEffect {

    /* (non-Javadoc)
     * @see forge.card.abilityfactory.SpellEffect#resolve(java.util.Map, forge.card.spellability.SpellAbility)
     */
    @Override
    public void resolve(SpellAbility sa) {
        final Card card = sa.getHostCard();
        final boolean remLagged = sa.hasParam("RememberLagged");
        final boolean alwaysRem = sa.hasParam("AlwaysRemember");
        if (remLagged) {
            card.clearRemembered();
        }

        final TargetRestrictions tgt = sa.getTargetRestrictions();
        final List<Card> tgtCards = getTargetCards(sa);

        for (final Card tgtC : tgtCards) {
            if (tgt != null && !tgtC.canBeTargetedBy(sa)) {
                continue;
            }
            if (tgtC.isInPlay()) {
                if (tgtC.isUnlagged() && remLagged || alwaysRem) {
                    card.addRemembered(tgtC);
                }
                tgtC.lag();
            }
            if (sa.hasParam("ETB")) {
                // do not fire Lags triggers
                tgtC.setLagged(true);
            }
        }
    }

    @Override
    protected String getStackDescription(SpellAbility sa) {
        final StringBuilder sb = new StringBuilder();

        sb.append("Lag ");
        final List<Card> tgtCards = getTargetCards(sa);
        sb.append(StringUtils.join(tgtCards, ", "));
        sb.append(".");
        return sb.toString();
    }

}
