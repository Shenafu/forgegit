package forge.game.ability.effects;

import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;

import java.util.List;

public class HonorExchangeEffect extends SpellAbilityEffect {

    // *************************************************************************
    // ************************ EXCHANGE HONOR **********************************
    // *************************************************************************



    // *************************************************************************
    // ************************* LOSE HONOR *************************************
    // *************************************************************************

    /* (non-Javadoc)
     * @see forge.card.abilityfactory.AbilityFactoryAlterHonor.SpellEffect#getStackDescription(java.util.Map, forge.card.spellability.SpellAbility)
     */
    @Override
    protected String getStackDescription(SpellAbility sa) {
        final StringBuilder sb = new StringBuilder();
        final Player activatingPlayer = sa.getActivatingPlayer();
        final List<Player> tgtPlayers = getTargetPlayers(sa);


        if (tgtPlayers.size() == 1) {
            sb.append(activatingPlayer).append(" exchanges honor totals with ");
            sb.append(tgtPlayers.get(0));
        } else if (tgtPlayers.size() > 1) {
            sb.append(tgtPlayers.get(0)).append(" exchanges honor totals with ");
            sb.append(tgtPlayers.get(1));
        }
        sb.append(".");
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see forge.card.abilityfactory.AbilityFactoryAlterHonor.SpellEffect#resolve(java.util.Map, forge.card.spellability.SpellAbility)
     */
    @Override
    public void resolve(SpellAbility sa) {
        final Card source = sa.getHostCard();
        Player p1;
        Player p2;

        final List<Player> tgtPlayers = getTargetPlayers(sa);

        if (tgtPlayers.size() == 1) {
            p1 = sa.getActivatingPlayer();
            p2 = tgtPlayers.get(0);
        } else {
            p1 = tgtPlayers.get(0);
            p2 = tgtPlayers.get(1);
        }

        final int honor1 = p1.getHonor();
        final int honor2 = p2.getHonor();

        if ((honor1 > honor2) && p1.canLoseHonor() && p2.canGainHonor()) {
            final int diff = honor1 - honor2;
            p1.loseHonor(diff);
            p2.gainHonor(diff, source, sa);
        } else if ((honor2 > honor1) && p2.canLoseHonor() && p1.canGainHonor()) {
            final int diff = honor2 - honor1;
            p2.loseHonor(diff);
            p1.gainHonor(diff, source, sa);
        } else {
            // they are equal, so nothing to do
        }

    }

}
