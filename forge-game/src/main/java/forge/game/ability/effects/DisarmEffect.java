package forge.game.ability.effects;

import java.util.List;

import forge.game.Game;
import forge.game.GameLogEntryType;
import forge.game.GameObject;
import forge.game.ability.AbilityUtils;
import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.card.CardCollection;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.TargetRestrictions;

public class DisarmEffect extends SpellAbilityEffect {

	/* (non-Javadoc)
	 * @see forge.card.abilityfactory.SpellEffect#getStackDescription(java.util.Map, forge.card.spellability.SpellAbility)
	 */
	//*
	@Override
	protected String getStackDescription(SpellAbility sa) {
		final Card host = sa.getHostCard();
		final Player player = host.getController();
		final List<GameObject> tgts = getTargets(sa);

		final StringBuilder sb = new StringBuilder();

		sb.append(player).append(" disarms.");
		if (tgts != null && !tgts.isEmpty()) {
			sb.append(" Targeting: " + tgts + ".");
		}
	
		return sb.toString();
	}
	//*/

	/* (non-Javadoc)
	 * @see forge.card.ability.SpellAbilityEffect#resolve(forge.card.spellability.SpellAbility)
	 */
	@Override
	public void resolve(SpellAbility sa) {
		final Card host = sa.getHostCard();
		final Player player = host.getController();
		final Game game = player.getGame();

		CardCollection tgtCards = getTargetCards(sa);
		final TargetRestrictions tgt = sa.getTargetRestrictions();
		//boolean success = false;
		String tgtName = "";

		final int hostPower = host.getCurrentPower();
		String codeLog = String.format("%s Power = %d", host, hostPower);
		game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);

		for (final Card tgtC : tgtCards) {
			if (tgtC.isInPlay() && ((tgt == null) || tgtC.canBeTargetedBy(sa)) && tgtC.isFaceDown()) {
				//Card downCard = tgtC.getAlternateState().getCard();

				tgtC.setState(tgtC.getAlternateStateName(), true);
				game.getAction().revealTo(tgtC, player);
				final int tgtCmc = tgtC.getCMC();
				tgtName = tgtC.getName();

				codeLog = String.format("%s CMC = %d", tgtName, tgtCmc);
				game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);

				if (hostPower >= tgtCmc) {
					codeLog = String.format("%s disarms %s succeeds", host, tgtName);
					game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);

					if (sa.hasAdditionalAbility("WinSubAbility")) {
						AbilityUtils.resolve(sa.getAdditionalAbility("WinSubAbility"));
					}
				} else {
					codeLog = String.format("%s disarms %s fails", host, tgtName);
					game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);

					if (sa.hasAdditionalAbility("LoseSubAbility")) {
						AbilityUtils.resolve(sa.getAdditionalAbility("LoseSubAbility"));
					}
				}
				tgtC.turnFaceDown(true);
			}      	
		}
	}
}
