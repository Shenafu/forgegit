package forge.game.ability.effects;

import forge.game.ability.AbilityUtils;
import forge.game.ability.SpellAbilityEffect;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.TargetRestrictions;

import java.util.ArrayList;
import java.util.List;

public class HonorSetEffect extends SpellAbilityEffect {

    /* (non-Javadoc)
     * @see forge.card.abilityfactory.AbilityFactoryAlterHonor.SpellEffect#resolve(java.util.Map, forge.card.spellability.SpellAbility)
     */
    @Override
    public void resolve(SpellAbility sa) {
        final boolean redistribute = sa.hasParam("Redistribute");
        final int honorAmount = redistribute ? 20 : AbilityUtils.calculateAmount(sa.getHostCard(), sa.getParam("HonorAmount"), sa);
        final TargetRestrictions tgt = sa.getTargetRestrictions();
        final List<Integer> honortotals = new ArrayList<Integer>();
        
        if (redistribute) {
            for (final Player p : getTargetPlayers(sa)) {
                if ((tgt == null) || p.canBeTargetedBy(sa)) {
                    honortotals.add(p.getHonor());
                }
            }
        }

        for (final Player p : getTargetPlayers(sa)) {
            if ((tgt == null) || p.canBeTargetedBy(sa)) {
                if (!redistribute) {
                    p.setHonor(honorAmount, sa.getHostCard());
                } else {
                    int honor = sa.getActivatingPlayer().getController().chooseNumber(sa, "Honor Total: " + p, honortotals, p);
                    p.setHonor(honor, sa.getHostCard());
                    honortotals.remove((Integer) honor);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see forge.card.abilityfactory.AbilityFactoryAlterHonor.SpellEffect#getStackDescription(java.util.Map, forge.card.spellability.SpellAbility)
     */
    @Override
    protected String getStackDescription(SpellAbility sa) {
        final StringBuilder sb = new StringBuilder();
        final int amount = AbilityUtils.calculateAmount(sa.getHostCard(), sa.getParam("HonorAmount"), sa);
        final boolean redistribute = sa.hasParam("Redistribute");

        List<Player> tgtPlayers = getTargetPlayers(sa);
        if (!redistribute) {
            for (final Player player : tgtPlayers) {
                sb.append(player).append(" ");
            }
            sb.append("honor total becomes ").append(amount).append(".");
        } else {
            sb.append("Redistribute ");
            for (final Player player : tgtPlayers) {
                sb.append(player).append(" ");
            }
            sb.append("honor totals.");
        }
        return sb.toString();
    }

}
