package forge.game.ability.effects;


import forge.game.ability.AbilityUtils;
import forge.game.ability.SpellAbilityEffect;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;
import forge.util.Lang;

import java.util.List;

public class HonorGainEffect extends SpellAbilityEffect {

    /* (non-Javadoc)
     * @see forge.game.ability.SpellAbilityEffect#getStackDescription(forge.game.spellability.SpellAbility)
     */
    @Override
    protected String getStackDescription(SpellAbility sa) {
        final StringBuilder sb = new StringBuilder();
        final String amountStr = sa.getParam("HonorAmount");

        sb.append(Lang.joinHomogenous(getDefinedPlayersOrTargeted(sa)));

        if (!amountStr.equals("AFHonorLost") || sa.hasSVar(amountStr)) {
            final int amount = AbilityUtils.calculateAmount(sa.getHostCard(), amountStr, sa);

            sb.append(" gains ").append(amount).append(" honor.");
        } else {
            sb.append(" gains honor equal to the honor lost this way.");
        }

        return sb.toString();
    }

    /* (non-Javadoc)
     * @see forge.game.ability.SpellAbilityEffect#resolve(forge.game.spellability.SpellAbility)
     */
    @Override
    public void resolve(SpellAbility sa) {
        final int honorAmount = AbilityUtils.calculateAmount(sa.getHostCard(), sa.getParam("HonorAmount"), sa);

        List<Player> tgtPlayers = getDefinedPlayersOrTargeted(sa);
        if( tgtPlayers.isEmpty() ) {
            tgtPlayers.add(sa.getActivatingPlayer());
        }

        for (final Player p : tgtPlayers) {
            if (!sa.usesTargeting() || p.canBeTargetedBy(sa)) {
                p.gainHonor(honorAmount, sa.getHostCard(), sa);
            }
        }
    }

}
