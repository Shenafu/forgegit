package forge.game.ability.effects;

import forge.game.ability.AbilityFactory;
import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.replacement.ReplacementEffect;
import forge.game.replacement.ReplacementHandler;
import forge.game.replacement.ReplacementLayer;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.TargetRestrictions;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class FreezeEffect extends SpellAbilityEffect {

   /* (non-Javadoc)
    * @see forge.card.abilityfactory.SpellEffect#resolve(java.util.Map, forge.card.spellability.SpellAbility)
    */
   @Override
   public void resolve(SpellAbility sa) {
       final Card card = sa.getHostCard();
       final boolean remTapped = sa.hasParam("RememberTapped");
       final boolean alwaysRem = sa.hasParam("AlwaysRemember");
       if (remTapped) {
           card.clearRemembered();
       }

       final TargetRestrictions tgt = sa.getTargetRestrictions();
       final List<Card> tgtCards = getTargetCards(sa);

       for (final Card tgtC : tgtCards) {
           if (tgt != null && !tgtC.canBeTargetedBy(sa)) {
               continue;
           }
           if (tgtC.isInPlay()) {
               if (tgtC.isUntapped() && remTapped || alwaysRem) {
                   card.addRemembered(tgtC);
               }
               tgtC.tap();
           }
           
           // enters frozen?
           if (sa.hasParam("ETB")) {
               // do not fire Taps triggers
               tgtC.setTapped(true);
           }

				// cease resolution if permanent won't tap
				// frozen only matters while it's tapped
           if (!tgtC.isTapped()) {
	           	continue;
           }
           
           // tgt gains Frozen keyword/state and replacement effect
           //tgtC.addFrozen();
           tgtC.addIntrinsicKeyword("Frozen");
           
           /*        
           
           // TODO: hardcoded untap replacement effect @ Card.java.untap()
           		until can create repeffstr that works
           		
           String repeffstr = "Event$ Untap | ActiveZones$ Battlefield | ValidCard$ Card.self"
           + " Affected$ Card.self | Description$ If this would untap while frozen, instead remove frozen.";
           
           String dbRemFrozen = "DB$ Debuff | Keywords$ Frozen | Defined$ Card.self";
           
           SpellAbility saRemFrozen = AbilityFactory.getAbility(dbRemFrozen, tgtC);
           
           ReplacementEffect re = ReplacementHandler.parseReplacement(repeffstr, tgtC, true);
           re.setLayer(ReplacementLayer.Other);

           re.setOverridingAbility(saRemFrozen);
           tgtC.addReplacementEffect(re);
           //*/
       }
   }

   @Override
   protected String getStackDescription(SpellAbility sa) {
       final StringBuilder sb = new StringBuilder();

       sb.append("Freeze ");
       final List<Card> tgtCards = getTargetCards(sa);
       sb.append(StringUtils.join(tgtCards, ", "));
       sb.append(".");
       return sb.toString();
   }

}
