package forge.game.ability.effects;

import forge.game.Game;
import forge.game.GameLogEntryType;
import forge.game.ability.AbilityFactory;
import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.card.CardCollectionView;
import forge.game.card.CardLists;
import forge.game.card.CardPredicates;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;
import forge.game.zone.ZoneType;

public class TeachEffect extends SpellAbilityEffect {
    @Override
    protected String getStackDescription(SpellAbility sa) { 
        final String validType = sa.getHostCard().getSVar("teachType");
        
        String out = String.format("%s chooses a %s card to exile for teaching.", sa.getActivatingPlayer(), validType);

        return out;
    }

    @Override
    public void resolve(SpellAbility sa) {
        final Card host = sa.getHostCard();
        final Player player = sa.getActivatingPlayer();
        final Game game = player.getGame();
        final String validType = sa.getHostCard().getSVar("teachType");
        
        // make list of cards in exile with valid type
        CardCollectionView choices = CardLists.filter(host.getController().getCardsIn(ZoneType.Graveyard), CardPredicates.isType(validType));

        // if no valid cards, exit
        if (choices.isEmpty()) {
            return;
        }
        
        // choose a card to exile
        Card choice = player.getController().chooseSingleEntityForEffect(choices, sa, "Choose a card you want to teach.", true);

        if (choice == null) {
          return;
        }

        // move host card to exile
        Card movedCard = game.getAction().moveTo(ZoneType.Exile, choice, sa);
        
        // store movedCard in taught array
        host.addTaughtCard(movedCard);

        // create new ability to cast taught card        
        final String cost = movedCard.getManaCost().getShortString();

        final String strPlay =  "AB$ Play | Cost$ " + cost + " Q | ValidCard$ Card.IsTaught+ExiledWithSource | ValidZone$ Exile"
        			+ " | Amount$ All | CopyCard$ True | Defined$ Taught | WithoutManaCost$ True"
     				+ " | SpellDescription$ Cast a copy of " + movedCard.getName() + ".";

        final SpellAbility saPlay = AbilityFactory.getAbility(strPlay, host);
        host.addSpellAbility(saPlay);
        
        String codeLog = String.format("Teaching %s to %s", movedCard, host);
        game.getGameLog().add(GameLogEntryType.STACK_RESOLVE, codeLog);        
        
        return;

    }

}
