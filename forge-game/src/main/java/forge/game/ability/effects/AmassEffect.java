/*
 * Forge: Play Magic: the Gathering.
 * Copyright (C) 2011  Forge Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package forge.game.ability.effects;

import forge.game.card.token.TokenInfo;
import forge.game.Game;
import forge.game.ability.AbilityUtils;
import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.card.CardCollection;
import forge.game.card.CardLists;
import forge.game.card.CardPredicates;
import forge.game.card.CounterType;
import forge.game.event.GameEventTokenCreated;
import forge.game.player.Player;
import forge.game.player.PlayerController;
import forge.game.spellability.SpellAbility;
import forge.game.zone.ZoneType;

public class AmassEffect extends SpellAbilityEffect {

	final String armyType = "Army";
	final String tokenScript = "b_0_0_zombie_army";
	final CounterType counterType = CounterType.P1P1;

	@Override
	protected String getStackDescription(SpellAbility sa) {
		final StringBuilder sb = new StringBuilder();

		sb.append("Amassing");

		return sb.toString();
	}

	@Override
	public void resolve(SpellAbility sa) {
		final Card host = sa.getHostCard();
		final Game game = host.getGame();
		final String valid = sa.getParamOrDefault("ValidPlayer", "You");
		final int nCounter = AbilityUtils.calculateAmount(host, sa.getParamOrDefault("Num", "1"), sa);
		Card prototype = TokenInfo.getProtoType(tokenScript, sa);
		final boolean remember = sa.hasParam("RememberAmass");

		for (final Player player : AbilityUtils.getDefinedPlayers(host, valid, sa)) {
			final PlayerController pc = player.getController();
			int armyCount = getArmies(player).size();

			// create Army token if player doesn't have one
			if (armyCount < 1) {
				Card token = TokenInfo.makeToken(prototype, player, false, 1).get(0);
				Card c = game.getAction().moveToPlay(token, sa);
				c.updateStateForView();
				game.fireEvent(new GameEventTokenCreated());
			}

			// put counters on an Army creature
			Card tgt = pc.chooseCardsForEffect(getArmies(player), sa,
				"Choose an Army to get +1/+1 counters", 1, 1, false).get(0);
			tgt.addCounter(counterType, nCounter, player, true);
			game.updateLastStateForCard(tgt);

			if (remember) {
				game.getCardState(sa.getHostCard()).addRemembered(tgt);
			}
		}
	}

	private CardCollection getArmies(Player player) {
		return CardLists.filter(player.getCardsIn(ZoneType.Battlefield), CardPredicates.isType(armyType));
	}
}
