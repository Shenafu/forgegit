package forge.game.ability.effects;

import forge.card.CardStateName;
import forge.game.Game;
import forge.game.GameActionUtil;
import forge.game.ability.AbilityUtils;
import forge.game.ability.SpellAbilityEffect;
import forge.game.card.Card;
import forge.game.card.CardCollection;
import forge.game.card.CardCollectionView;
import forge.game.card.CardLists;
import forge.game.card.CounterType;
import forge.game.player.DelayedReveal;
import forge.game.player.Player;
import forge.game.player.PlayerView;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.TargetRestrictions;
import forge.game.zone.PlayerZone;
import forge.game.zone.ZoneType;
import forge.util.Lang;
import forge.util.TextUtil;

import java.util.*;

public class DigMultipleEffect extends SpellAbilityEffect {

    @Override
    protected String getStackDescription(SpellAbility sa) {
        final Card host = sa.getHostCard();
        final StringBuilder sb = new StringBuilder();
        final int numToDig = AbilityUtils.calculateAmount(host, sa.getParam("DigNum"), sa);
        final List<Player> tgtPlayers = getTargetPlayers(sa);
        String changeValid = sa.getParam("ChangeValid");

        sb.append(host.getController()).append(" looks at the top ");
        sb.append(Lang.nounWithAmount(numToDig, "card")).append(" of ");

        if (tgtPlayers.contains(host.getController())) {
            sb.append("their ");
        }
        else {
            for (final Player p : tgtPlayers) {
                sb.append(Lang.getPossesive(p.getName())).append(" ");
            }
        }
        sb.append("library for a ");
        sb.append(changeValid);
        
        return sb.toString();
    }

    @Override
    public void resolve(SpellAbility sa) {
        final Card host = sa.getHostCard();
        final Player player = sa.getActivatingPlayer();
        final Game game = player.getGame();
        Player chooser = player;
        int numToDig = AbilityUtils.calculateAmount(host, sa.getParam("DigNum"), sa);

        final ZoneType srcZone = sa.hasParam("SourceZone") ? ZoneType.smartValueOf(sa.getParam("SourceZone")) : ZoneType.Library;

        final ZoneType destZone1 = sa.hasParam("DestinationZone") ? ZoneType.smartValueOf(sa.getParam("DestinationZone")) : ZoneType.Hand;
        final ZoneType destZone2 = sa.hasParam("DestinationZone2") ? ZoneType.smartValueOf(sa.getParam("DestinationZone2")) : ZoneType.Library;

        int libraryPosition = sa.hasParam("LibraryPosition") ? Integer.parseInt(sa.getParam("LibraryPosition")) : -1;
        String changeValid = sa.hasParam("ChangeValid") ? sa.getParam("ChangeValid") : "";

        final int libraryPosition2 = sa.hasParam("LibraryPosition2") ? Integer.parseInt(sa.getParam("LibraryPosition2")) : -1;
        final boolean noMove = sa.hasParam("NoMove");
        final boolean skipReorder = sa.hasParam("SkipReorder");

        // A hack for cards like Explorer's Scope that need to ensure that a card is revealed to the player activating the ability
        final boolean forceRevealToController = sa.hasParam("ForceRevealToController");

        final List<String> keywords = new ArrayList<String>();
        if (sa.hasParam("Keywords")) {
            keywords.addAll(Arrays.asList(sa.getParam("Keywords").split(" & ")));
        }

        final TargetRestrictions tgt = sa.getTargetRestrictions();
        final List<Player> tgtPlayers = getTargetPlayers(sa);

        if (sa.hasParam("Choser")) {
            final List<Player> choosers = AbilityUtils.getDefinedPlayers(sa.getHostCard(), sa.getParam("Choser"), sa);
            if (!choosers.isEmpty()) {
                chooser = choosers.get(0);
            }
        }

        for (final Player p : tgtPlayers) {
            if (tgt != null && !p.canBeTargetedBy(sa)) {
                continue;
            }
            final CardCollection top = new CardCollection();
            final CardCollection rest = new CardCollection();
            final PlayerZone sourceZone = p.getZone(srcZone);

            numToDig = Math.min(numToDig, sourceZone.size());
            for (int i = 0; i < numToDig; i++) {
                top.add(sourceZone.get(i));
            }

            if (!top.isEmpty()) {
                DelayedReveal delayedReveal = null;
                boolean hasRevealed = true;
                if (sa.hasParam("Reveal")) {
                    game.getAction().reveal(top, p, false);
                }
                else if (sa.hasParam("RevealOptional")) {
                    String question = TextUtil.concatWithSpace("Reveal:", TextUtil.addSuffix(Lang.joinHomogenous(top),"?"));

                    hasRevealed = p.getController().confirmAction(sa, null, question);
                    if (hasRevealed) {
                        game.getAction().reveal(top, p);
                    }
                }
                else if (sa.hasParam("RevealValid")) {
                    final String revealValid = sa.getParam("RevealValid");
                    final CardCollection toReveal = CardLists.getValidCards(top, revealValid, host.getController(), host);
                    if (!toReveal.isEmpty()) {
                        game.getAction().reveal(toReveal, host.getController());
                        if (sa.hasParam("RememberRevealed")) {
                            for (final Card one : toReveal) {
                                host.addRemembered(one);
                            }
                        }
                    }
                    // Singletons.getModel().getGameAction().revealToCopmuter(top.toArray());
                    // - for when it exists
                }
                else if (!sa.hasParam("NoLooking")) {
                    // show the user the revealed cards
                    delayedReveal = new DelayedReveal(top, srcZone, PlayerView.get(p), host.getName() + " - Looking at cards in ");

                    if (noMove) {
                        // Let the activating player see the cards even if they're not moved
                        game.getAction().revealTo(top, player);
                    }
                }

                if (sa.hasParam("RememberRevealed") && !sa.hasParam("RevealValid") && hasRevealed) {
                    for (final Card one : top) {
                        host.addRemembered(one);
                    }
                }

                if (!noMove) {
                    CardCollection movedCards;
                    for (final Card c : top) {
                        rest.add(c);
                    }
                    List<CardCollection> valids = new ArrayList<CardCollection>();
                    if (!changeValid.isEmpty()) {
                  	  // split changeValid and store into list
                  	  // this way, a card that matches multiple valid criteria can be selected for either
                  	  String[] changeValids = changeValid.split(",");
                  	  for (int i=0; i<changeValids.length; i++) {
                  		  valids.add(CardLists.getValidCards(top, changeValids[i], host.getController(), host, sa));
                  	  }
                    }

                    if (forceRevealToController) {
                        // Force revealing the card to the player activating the ability (e.g. Explorer's Scope)
                        game.getAction().revealTo(top, player);
                        delayedReveal = null; // top is already seen by the player, do not reveal twice
                    }

                     String prompt;

                     if (sa.hasParam("PrimaryPrompt")) {
                         prompt = sa.getParam("PrimaryPrompt");
                     } else {
                         prompt = "Choose a card to put into " + destZone1.name();
                         if (destZone1.equals(ZoneType.Library)) {
                             if (libraryPosition == -1) {
                                 prompt = "Choose a card to put on the bottom of {player's} library";
                             }
                             else if (libraryPosition == 0) {
                                 prompt = "Choose a card to put on top of {player's} library";
                             }
                         }
                     }

                     movedCards = new CardCollection();
                     for (int i = 0; i < valids.size(); i++) {
                         // let user get choice
                        // If we're choosing multiple cards, only need to show the reveal dialog the first time through.
                        boolean shouldReveal = (i == 0);
                         Card chosen = chooser.getController().chooseSingleEntityForEffect(valids.get(i), shouldReveal ? delayedReveal : null, sa, prompt, true, p);

                         if (chosen == null) {
                             continue;
                         }

                         movedCards.add(chosen);
                         for (int j = 0; j < valids.size(); j++) {
                        	 valids.get(j).remove(chosen);
                         }
                     }

                     if (!sa.hasParam("ExileFaceDown") && !sa.hasParam("NoReveal")) {
                         game.getAction().reveal(movedCards, chooser, true,
                                 chooser + " picked " + (movedCards.size() == 1 ? "this card" : "these cards") + " from ");
                     }
                    if (sa.hasParam("ForgetOtherRemembered")) {
                        host.clearRemembered();
                    }
                    Collections.reverse(movedCards);

                    Card effectHost = sa.getOriginalHost();
                    if (effectHost == null) {
                        effectHost = sa.getHostCard();
                    }
                    for (Card c : movedCards) {
                        final PlayerZone zone = c.getOwner().getZone(destZone1);

                        if (zone.is(ZoneType.Library) || zone.is(ZoneType.PlanarDeck) || zone.is(ZoneType.SchemeDeck)) {
                            if (libraryPosition == -1 || libraryPosition > zone.size()) {
                                libraryPosition = zone.size();
                            }
                            c = game.getAction().moveTo(zone, c, libraryPosition, sa);
                        }
                        else {
                            c = game.getAction().moveTo(zone, c, sa);
                            if (destZone1.equals(ZoneType.Battlefield)) {
                                for (final String kw : keywords) {
                                    c.addExtrinsicKeyword(kw);
                                }
                                if (sa.hasParam("Tapped")) {
                                    c.setTapped(true);
                                }
                            } else if (destZone1.equals(ZoneType.Exile)) {
                                c.setExiledWith(effectHost);
                            }
                        }

                        if (sa.hasParam("ExileFaceDown")) {
                            c.setState(CardStateName.FaceDown, true);
                        }
                        if (sa.hasParam("Imprint")) {
                           host.addImprintedCard(c);
                        }
                        if (sa.hasParam("Teach")) {
                           host.addTaughtCard(c);
                        }
                        if (sa.hasParam("ForgetOtherRemembered")) {
                            host.clearRemembered();
                        }
                        if (sa.hasParam("RememberChanged")) {
                            host.addRemembered(c);
                        }
                        rest.remove(c);
                    }

                    // now, move the rest to destZone2
                    if (destZone2 == ZoneType.Library || destZone2 == ZoneType.PlanarDeck || destZone2 == ZoneType.SchemeDeck
                            || destZone2 == ZoneType.Graveyard) {
                        CardCollection afterOrder = rest;
                        if (sa.hasParam("RestRandomOrder")) {
                            CardLists.shuffle(afterOrder);
                        }
                        else if (!skipReorder && rest.size() > 1) {
                            if (destZone2 == ZoneType.Graveyard) {
                                afterOrder = (CardCollection) GameActionUtil.orderCardsByTheirOwners(game, rest, destZone2);
                            } else {
                                afterOrder = (CardCollection) chooser.getController().orderMoveToZoneList(rest, destZone2);
                            }
                        }
                        if (libraryPosition2 != -1) {
                            // Closest to top
                            Collections.reverse(afterOrder);
                        }
                        for (final Card c : afterOrder) {
                            if (destZone2 == ZoneType.Library) {
                                game.getAction().moveToLibrary(c, libraryPosition2, sa);
                            }
                            else {
                                game.getAction().moveToVariantDeck(c, destZone2, libraryPosition2, sa);
                            }
                        }
                    }
                    else {
                        // just move them randomly
                        for (int i = 0; i < rest.size(); i++) {
                            Card c = rest.get(i);
                            final PlayerZone toZone = c.getOwner().getZone(destZone2);
                            c = game.getAction().moveTo(toZone, c, sa);
                            if (destZone2 == ZoneType.Battlefield && !keywords.isEmpty()) {
                                for (final String kw : keywords) {
                                    c.addExtrinsicKeyword(kw);
                                }
                            } else if (destZone2 == ZoneType.Exile) {
                                if (sa.hasParam("ExileWithCounter")) {
                                    c.addCounter(CounterType.getType(sa.getParam("ExileWithCounter")), 1, player, true);
                                }
                                c.setExiledWith(effectHost);
                            }
                        }
                    }
                }
            }
        }
    }
}
