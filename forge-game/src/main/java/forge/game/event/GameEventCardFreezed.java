package forge.game.event;

import forge.game.card.Card;

public class GameEventCardFreezed extends GameEvent {
    public final boolean freezed;
    public final Card card;

    public GameEventCardFreezed(final Card card, final boolean freezed) {
        this.freezed = freezed;
        this.card = card;
    }
    
    
    @Override
    public <T> T visit(IGameEventVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
