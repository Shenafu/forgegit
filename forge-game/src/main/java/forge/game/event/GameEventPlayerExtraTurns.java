package forge.game.event;

import forge.game.player.Player;
import forge.util.Lang;
import forge.util.TextUtil;

public class GameEventPlayerExtraTurns extends GameEvent {
    public final Player player;
    public final int oldExtraTurns;
    public final int newExtraTurns;
    
    public GameEventPlayerExtraTurns(Player who, int oldValue, int newValue) {
        player = who;
        oldExtraTurns = oldValue;
        newExtraTurns = newValue;
    }
    
    @Override
    public <T> T visit(IGameEventVisitor<T> visitor) {
        return visitor.visit(this);
    }
    
    @Override
    public String toString() {
        return TextUtil.concatWithSpace(Lang.getPossesive(player.getName()),"extra turns changed:",  String.valueOf(oldExtraTurns),"->", String.valueOf(newExtraTurns));
    }
}
