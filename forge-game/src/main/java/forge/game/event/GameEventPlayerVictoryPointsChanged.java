package forge.game.event;

import forge.game.player.Player;
import forge.util.Lang;
import forge.util.TextUtil;

public class GameEventPlayerVictoryPointsChanged extends GameEvent {
    public final Player player;
    public final int oldVictory;
    public final int newVictory;
    
    public GameEventPlayerVictoryPointsChanged(Player who, int oldValue, int newValue) {
        player = who;
        oldVictory = oldValue;
        newVictory = newValue;
    }
    
    @Override
    public <T> T visit(IGameEventVisitor<T> visitor) {
        return visitor.visit(this);
    }
    
    @Override
    public String toString() {
        return TextUtil.concatWithSpace(Lang.getPossesive(player.getName()),"victory points changed:",  String.valueOf(oldVictory),"->", String.valueOf(newVictory));
    }
}
