package forge.game.event;

import forge.game.card.Card;

public class GameEventCardLagged extends GameEvent {
    public final boolean lagged;
    public final Card card;

    public GameEventCardLagged(final Card card, final boolean lagged) {
        this.lagged = lagged;
        this.card = card;
    }
    
    
    @Override
    public <T> T visit(IGameEventVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
