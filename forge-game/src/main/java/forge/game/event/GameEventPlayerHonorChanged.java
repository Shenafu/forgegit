package forge.game.event;

import forge.game.player.Player;
import forge.util.Lang;
import forge.util.TextUtil;

public class GameEventPlayerHonorChanged extends GameEvent {
    public final Player player;
    public final int oldHonor;
    public final int newHonor;
    
    public GameEventPlayerHonorChanged(Player who, int oldValue, int newValue) {
        player = who;
        oldHonor = oldValue;
        newHonor = newValue;
    }
    
    @Override
    public <T> T visit(IGameEventVisitor<T> visitor) {
        return visitor.visit(this);
    }
    
    @Override
    public String toString() {
        return TextUtil.concatWithSpace(Lang.getPossesive(player.getName()),"honor changed:",  String.valueOf(oldHonor),"->", String.valueOf(newHonor));
    }
}
