/*
 * Forge: Play Magic: the Gathering.
 * Copyright (C) 2011  Forge Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package forge.game.cost;

import forge.game.card.Card;
import forge.game.card.CardCollection;
import forge.game.card.CardCollectionView;
import forge.game.card.CardLists;
import forge.game.player.Player;
import forge.game.spellability.SpellAbility;
import forge.game.zone.ZoneType;
import forge.util.TextUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * The Class CostUnattach.
 */
public class CostUnattach extends CostPartWithList {
    // Unattach<CARDNAME> if ability is on the Equipment
    // Unattach<OriginalHost/Heartseeker> if equipped creature has the ability
	 // Unattach<1/Equipment> if ability is not granted by equipment, e.g. instant/sorcery, other ability
    // OLD: Unattach<Card.Attached+namedHeartseeker/Equipped Heartseeker> if equipped creature has the ability

    /**
     * Serializables need a version ID.
     */
    private static final long serialVersionUID = 1L;


    /**
     * Instantiates a new cost unattach.
     */
    public CostUnattach(final String type, final String desc) {
        super("1", type, desc);
    }

    public CostUnattach(final String amt, final String type, final String desc) {
       super(amt, type, desc);
	}

	@Override
    public boolean isUndoable() { return false; }

    @Override
    public boolean isReusable() { return true; }

    /*
     * (non-Javadoc)
     * 
     * @see forge.card.cost.CostPart#toString()
     */
    @Override
    public final String toString() {
        return TextUtil.concatWithSpace("Unattach", this.getTypeDescription());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * forge.card.cost.CostPart#canPay(forge.card.spellability.SpellAbility,
     * forge.Card, forge.Player, forge.card.cost.Cost)
     */
    @Override
    public final boolean canPay(final SpellAbility ability, final Player payer) {
        final Card source = ability.getHostCard();

        final int amt = Integer.parseInt(this.getAmount());
        final String type = this.getType();
        final String desc = this.getTypeDescription();
        if (type.equals("CARDNAME")) {
            if (source.isEquipping() || source.isFortifying()) {
                return true;
            }
        } else if (type.equals("OriginalHost")) {
           Card originalEquipment = ability.getOriginalHost();
           if (originalEquipment.isEquipping() || originalEquipment.isFortifying()) {
               return true;
           }
        } else if (type.startsWith("Equipment") || type.startsWith("Aura") || type.startsWith("Fortification") ) {
      	  List<Card> mods = CardLists.getValidCards(source.getController().getCardsIn(ZoneType.Battlefield), desc, payer, source);
      	  List<Card> attached = new ArrayList<Card>();
      	  for (Card mod: mods) {
      		  if (mod.isEquipping() || mod.isEnchanting() || mod.isFortifying()) {
      			  attached.add(mod);
      		  }
      	  }
      	  
      	  // debug Xay
      	  System.out.println();
      	  System.out.println("canPay()");
      	  System.out.println("type: " + type);
      	  System.out.println("amount: " + amt);
      	  System.out.println("attached: " + attached);
      	  System.out.println("attached.size: " + attached.size());
      	  
           if (attached.size() >= amt) {

         	  System.out.println("can pay: True");
         	  return true;
           }
        } else {
           if (CardLists.getValidCards(source.getEquippedBy(), type, payer, source).size() > 0) {
              return true;
          }
           if (CardLists.getValidCards(source.getFortifiedBy(), type, payer, source).size() > 0) {
              return true;
          }
        }
        
   	  System.out.println("can pay: False");

        return false;
    }

    public List<Card> findCardsToUnattach(final Card source, Player activator, SpellAbility ability) {
   	 final int amt = Integer.parseInt(this.getAmount());
   	 final String type = this.getType();
       final String desc = this.getTypeDescription();
       List<Card> retCards = new ArrayList<Card>();

  	  // debug Xay
  	  System.out.println();
  	  System.out.println("findCardsToUnattach()");
  	  System.out.println("amount: " + amt);
  	  System.out.println("type: " + type);
  	  System.out.println("desc: " + desc);
  	  
  	  
        if (type.equals("CARDNAME")) {
            if (source.isEquipping() || source.isFortifying()) {
            	retCards.add(source);
            }
        } else if (type.equals("OriginalHost")) {
            Card originalMod = ability.getOriginalHost();
            if (originalMod.isEquipping() || originalMod.isFortifying()) {
            	retCards.add(originalMod);
            }
        } else if (type.startsWith("Equipment") || type.startsWith("Aura") || type.startsWith("Fortification") ) {
      	  List<Card> mods = CardLists.getValidCards(source.getController().getCardsIn(ZoneType.Battlefield), desc, activator, source);
      	  List<Card> attached = new ArrayList<Card>();
      	  for (Card mod: mods) {
      		  if (mod.isEquipping() || mod.isEnchanting()) {
      			  attached.add(mod);
      		  }
      	  }
      	  
      	  // debug Xay
      	  System.out.println("attached: " + attached);
      	  System.out.println("attached.size: " + attached.size());
      	  
           if (attached.size() >= amt) {
         	  String message = String.format("Choose %d %s to Unattach", amt, desc);
         	  CardCollectionView choice = ability.getActivatingPlayer().getController().chooseCardsForEffect(new CardCollection(attached), ability, message, amt, amt, false);
         	  for (Card c : choice) {
         		  retCards.add(c);
         	  }
           }
        } else {
            List<Card> attachees = CardLists.getValidCards(source.getEquippedBy(), type, activator, source);
            attachees.addAll(CardLists.getValidCards(source.getFortifiedBy(), type, activator, source));
            if (attachees.size() > 0) {
                // Just pick the first one, although maybe give a dialog
            	retCards.add(attachees.get(0));
            }
        }
        return retCards;
    }

    /* (non-Javadoc)
     * @see forge.card.cost.CostPartWithList#executePayment(forge.card.spellability.SpellAbility, forge.Card)
     */
    @Override
    protected Card doPayment(SpellAbility ability, Card targetCard) {
        targetCard.unattachFromEntity(targetCard.getEntityAttachedTo());
        return targetCard;
    }

    @Override
    public String getHashForLKIList() {
        return "Unattached";
    }
    @Override
    public String getHashForCardList() {
    	return "UnattachedCards";
    }

   
    public <T> T accept(ICostVisitor<T> visitor) {
        return visitor.visit(this);
    }

}
