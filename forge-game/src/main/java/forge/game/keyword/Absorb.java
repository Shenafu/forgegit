package forge.game.keyword;

import java.util.Collection;

import javax.xml.soap.Detail;

public class Absorb extends KeywordInstance<Absorb> {
	private String amount = "all";
	private String type = "";
	private String desc = "";

	@Override
	protected void parse(String details) {

		// Absorb{:Amount}{:Trait}{:Description}
		// ex. Absorb
		// ex. Absorb:1
		// ex. Absorb:1:Red
		// ex. Absorb:1:Red,Black:red or black

		if (!details.isEmpty()) {
			String[] det = details.split(":");

			if (det.length > 2) {
				this.desc = det[2];      		  
			}
			if (det.length > 1) {
				this.type = det[1];
			}
			if (det.length > 0) {

				try {
					this.amount = Integer.parseInt(det[0]) > 0 ? det[0] : "all";
				} catch (Exception e) {
				}
			}
		}
	}

	@Override
	protected String formatReminderText(String reminderText) {		
		if (type.isEmpty()) {
			String reminder = "Prevent %s damage dealt to this from any source.";
			return String.format(reminder, amount);
		}
		if (!desc.isEmpty()) {
			return String.format(reminderText, amount, desc);
		}
		return String.format(reminderText, amount, type);
	}

	/* (non-Javadoc)
	 * @see forge.game.keyword.KeywordInstance#redundant(java.util.Collection)
	 */
	@Override
	public boolean redundant(Collection<KeywordInterface> list) {
		for (KeywordInterface i : list) {
			if (i.getOriginal().equals(getOriginal())) {
				return true;
			}
		}
		return false;
	}


}
