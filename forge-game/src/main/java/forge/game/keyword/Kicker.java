package forge.game.keyword;

import java.util.List;

import com.google.common.collect.Lists;

import forge.game.cost.Cost;
import forge.util.TextUtil;

public class Kicker extends KeywordWithCost {
   private Cost cost2 = null;
   private Cost cost3 = null;

    public Kicker() {
    }
    
    @Override
    protected void parse(String details) {
        List<String> l = Lists.newArrayList(TextUtil.split(details, ':'));
        super.parse(l.get(0));
        if (l.size() > 1) {
           cost2 = new Cost(l.get(1), false);
        }
        if (l.size() > 2) {
           cost3 = new Cost(l.get(2), false);
        }
    }
    
    @Override
    protected String formatReminderText(String reminderText) {
       if (cost2 == null) {
          return super.formatReminderText(reminderText);
      }
       
   	 String extraCost = cost.toSimpleString();
       if (cost2 != null) {
          extraCost += " and/or " + cost2.toSimpleString();
      }
       if (cost3 != null) {
          extraCost += " and/or " + cost3.toSimpleString();
      }
        //handle special case of double kicker
        return TextUtil.concatWithSpace("You may pay an additional ", extraCost, " as you cast this spell.");
    }
}
