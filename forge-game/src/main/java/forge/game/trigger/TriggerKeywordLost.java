/*
 * Forge: Play Magic: the Gathering.
 * Copyright (C) 2011  Forge Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package forge.game.trigger;

import forge.game.card.Card;
import forge.game.keyword.Keyword;
import forge.game.spellability.SpellAbility;
import forge.game.spellability.SpellAbilityStackInstance;

/**
 * <p>
 * Trigger_KeywordLost class.
 * </p>
 * 
 * @author Forge
 * @version $Id$
 */
public class TriggerKeywordLost extends Trigger {

	/**
	 * <p>
	 * Constructor for Trigger_KeywordLost.
	 * </p>
	 * 
	 * @param params
	 *            a {@link java.util.HashMap} object.
	 * @param host
	 *            a {@link forge.game.card.Card} object.
	 * @param intrinsic
	 *            the intrinsic
	 */
	public TriggerKeywordLost(final java.util.Map<String, String> params, final Card host, final boolean intrinsic) {
		super(params, host, intrinsic);
	}

	/** {@inheritDoc} */
	@Override
	public final boolean performTest(final java.util.Map<String, Object> runParams2) {
		Keyword kw = (Keyword) runParams2.get("Keyword");

		if (hasParam("ValidCard")) {
			if (!matchesValid(runParams2.get("Card"), getParam("ValidCard").split(","),
			this.getHostCard())) {
				return false;
			}
		}

		if (hasParam("ValidKeyword")) {
			for (String k : getParam("ValidKeyword").split(",")) {
				if (!kw.toString().equals(k)) {
					return false;
				}
			}
		}

		if (hasParam("NonValidKeyword")) {
			for (String k : getParam("NonValidKeyword").split(",")) {
				if (kw.toString().equals(k)) {
					return false;
				}
			}
		}

		if (hasParam("OncePerEffect")) {
			// A "once per effect" trigger will only trigger once regardless of how many things the effect caused
			// to change zones.

			// The SpellAbilityStackInstance keeps track of which host cards with "OncePerEffect"
			// triggers already fired as a result of that effect.
			// TODO This isn't quite ideal, since it really should be keeping track of the SpellAbility of the host
			// card, rather than keeping track of the host card itself - but it's good enough for now - since there
			// are no cards with multiple different OncePerEffect triggers.
			SpellAbilityStackInstance si = (SpellAbilityStackInstance) runParams2.get("SpellAbilityStackInstance");

			// si == null means the stack is empty
         return si == null || !si.hasOncePerEffectTrigger(this);
		}

		return true;
	}


	/** {@inheritDoc} */
	@Override
	public final void setTriggeringObjects(final SpellAbility sa) {
		sa.setTriggeringObject("Card", getRunParams().get("Card"));
		sa.setTriggeringObject("Keyword", getRunParams().get("Keyword"));
	}

	@Override
	public String getImportantStackObjects(SpellAbility sa) {
		StringBuilder sb = new StringBuilder();
		sb.append("Card: ").append(sa.getTriggeringObject("Card")).append(", ");
		sb.append("Lost: ").append(sa.getTriggeringObject("Keyword"));
		return sb.toString();
	}
}
